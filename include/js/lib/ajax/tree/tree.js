//
// by ESV at HOME, 11.2011
//
// работа с деревом контента
//

function __TREE_init(
	root_id,		// id корня дерева
	root_name,	// наименование корня дерева
	width,		// ширина
	height,		// высота
	dataURL,		// url для получения дерева
					// НЕ должен заканчиваться на '/'
	getURL,		// url для получения контент-страницы элемента,  используется как getURL + id элемента, контент-страница
					// которого должна быть получена
					// должен заканчиваться на '/'
	addURL,		// url для добавления элемента, используется как addURL + id элемента, к которому добавляется
					// должен заканчиваться на '/'
	delURL,		// url для удаления элемента,  используется как delURL + id элемента, который удаляется
					// должен заканчиваться на '/'
	moveURL,		// url для перемещения элемента,  используется как url с передачей POST-запроса:
					// параметры:
					//	drag_id - id перемещаемого элемента
					// drop_id - id элемента, на который перемещается
					// point   - точка перемещения: 'above' или 'below'
					// НЕ должен заканчиваться на '/'
	idTree,		// id блока дерева
	renderTo		// id блока DIV, куда будет отрисовываться дерево

) {

	root_id = root_id.toString();
	root_name = root_name.toString();

	width = width*1; // преобразование в число
	height = height*1; // преобразование в число

	return new Ext.tree.TreePanel({

		id: idTree,
		width: width,
		height: height,
		renderTo: renderTo,
		animate: true,
		autoScroll: true,		// включается автоматическая прокрутка
		containerScroll: true,
		border: true,
		collapsible: false,	// панель дерева не сворачивается
		draggable: false,
		enableDD: true,
		useArrows: true,
		root: {
			id: root_id,
			text: root_name,
			draggable: false,
			editable: false,
			nodeType: 'async',
			expanded: true,
			cls: 'node'
		},
		rootVisible: true,

		dataUrl: dataURL,

		// панель кнопок
		tbar: [
			{	// добавить элемент в дерево
				xtype: 'button',
				text: 'добавить',

				handler: function() {

					var tree = this.ownerCt.ownerCt;

						if (!tree) {
							alert('Ошибка получения компонента - дерево');
							return false;
						}

					if (tree.selected_id>-1) {
						goAjaxNavigation(tree.addURL + tree.selected_id);
					} else {
						Ext.Msg.alert('Добавление невозможно', 'Элемент не выбран. Добавление невозможно.');
					}

					return true;
				}
			},
			{	// удалить элемент из дерева
				xtype: 'button',
				text: 'удалить',

				handler: function() {

					var tree = this.ownerCt.ownerCt;

						if (!tree) {
							alert('Ошибка получения компонента - дерево');
							return false;
						}

					if (tree.selected_id>0) {

						var node = tree.get_selected_node();

						if (!node) {
							Ext.Msg.alert('Ошибка', 'Невозможно получить текущий элемент дерева.');
							return false;
						} else if (!node.attributes.leaf) {
							Ext.Msg.alert('Удаление невозможно', 'Невозможно удалить элемент, содержащий подэлементы.');
							return false;
						} else if (node.attributes.allowDelete==false) {
							var reason = '';
							if (node.attributes.reason) {
								reason = ' ' + node.attributes.reason;
							}
							Ext.Msg.alert('Удаление невозможно', 'Удаление элемента запрещено.' + reason);
							return false;
						} else {
							Ext.Msg.confirm('Удаление','Вы действительно хотите удалить выбранный элемент?',

								function(button) {

									if (button=='yes') {

										var parent = node.parentNode;
										var id = tree.selected_id;

										goDirectAjaxNavigation(tree.delURL + id);
										node.remove(true);

										if (parent && parent.attributes.id>0) {
											tree.selected_id = parent.attributes.id;
											tree.path = parent.getPath('id');
											tree.reload_tree();
										} else {
											var root = tree.getRootNode();
											tree.selected_id = 0;
											tree.path = root.getPath('id');
											tree.selectPath(tree.path,'id');
										}
									}
								}
							);
						}

					} else {
						Ext.Msg.alert('Удаление невозможно', 'Элемент не выбран. Удаление невозможно.');
						return false;
					}

					return true;
				}
			}
		],

		// обработка событий дерева
		listeners: {

			click: // выбор элемента

				function(node,event) {

					// запоминаем id выделенного элемента
					this.selected_id = node.attributes.id;

					// запоминаем путь
					this.path = node.getPath('id');

					// переходим к контент-странице элемента, если это не корень
					goAjaxNavigation(this.getURL + this.selected_id);

					return true;
				},

			append: // добавление элемента

				function(tree,parent,node,index) {

					// установка класса для отображения элемента
					if (!node.attributes.cls) {
						node.attributes.cls = 'node';
					}

					node.draggable = true;
					node.allowDrag = true;
					node.allowDrop = true;
					node.editable = false;
				},

			// Drag & Drop
			nodedragover: // проверка при проносе над элементами, drap

					function(el) {

					// запрет drag на элементе для добавления
					if (el.point == 'append') {
						el.cancel = true;
						return false;
					}

					// запрет перемещения на самого себя
					if (el.target.attributes.id==el.dropNode.attributes.id) {
						el.cancel = true;
						return false;
					}

					// запрет перемещения в другую категорию
					if (el.target.attributes.parent_id!=el.dropNode.attributes.parent_id) {
						el.cancel = true;
						return false;
					}

					el.cancel = false;
					return true;
				},

			// запрет drop на элементе для добавления
			beforenodedrop:

				function(el) {

					// запрет добавления
					if (el.point == 'append') {
						el.cancel = true;
						return false;
					}

					// запрет перемещения на самого себя
					if (el.target.attributes.id==el.dropNode.attributes.id) {
						el.cancel = true;
						return false;
					}

					// запрет перемещения в другую категорию
					if (el.target.attributes.parent_id!=el.dropNode.attributes.parent_id) {
						el.cancel = true;
						return false;
					}

					var params = {
						'drag_id':	el.dropNode.attributes.id,
						'drop_id':	el.target.attributes.id,
						'point':		el.point
					};

					$.ajax({
						url: this.moveURL,
						type: 'post',
						async: false,			// !!! отключим асинхронную обработку - важно дождаться обработки на сервере
						cache: false,
						dataType: 'json',		// ответ сервера
						data: params,
						traditional: true,	// кодирование данных

						complete: function(jqXHR, textStatus) {

						},

						success: function (data, textStatus, jqXHR) {
							el.cancel = false;
						},

						error: function(jqXHR, textStatus, errorThrown) {
							el.cancel = true;
						}
					});

					return !el.cancel;
				}
		},

		// определение собственных методов и свойств

		path: null,			// выбранный путь
		selected_id: -1,	// выбранный элемент
		getURL:	getURL,	// url для получения контент-страницы элемента,  используется как getURL + id элемента, контент-страница
								// которого должна быть получена
								// должен заканчиваться на '/'
		addURL:	addURL,	// url для добавления элемента, используется как addURL + id элемента, к которому добавляется
								// должен заканчиваться на '/'
		delURL:	delURL,	// url для удаления элемента,  используется как delURL + id элемента, который удаляется
								// должен заканчиваться на '/'
		moveURL:	moveURL,	// url для перемещения элемента,  используется как url с передачей POST-запроса:
								// параметры:
								//	drag_id - id перемещаемого элемента
								// drop_id - id элемента, на который перемещается
								// point   - точка перемещения: 'above' или 'below'
								// должен заканчиваться на '/'

		// перезагрузка дерева
		reload_tree: function() {

			var root = this.getRootNode();
			var path = this.path;

			if (path) {
            root.reload(function(){
               root.getOwnerTree().expandPath(path);
					root.getOwnerTree().selectPath(path);
            });
			} else {
				root.reload();
				root.select();
			}
		},

		// получить текущий выбранный элемент
		get_selected_node: function() {
			var selection_model = this.getSelectionModel();
			if (!selection_model) {
				alert('Не могу получить SelectionModel');
				return null;
			}
			return selection_model.getSelectedNode();
		},

		// добавление элемента в дерево, чтобы после перезагрузки он стал
		// текущим выбранным. Вызывается из шаблона редактирования контент-страницы
		add_node: function(id) {
			if (id) {
				this.selected_id = id;
				this.path = this.path + '/' +id;
			}
		}
	});


	// обработчики событий дерева

	//// добавление элемента
	//__TREE.on('append', function(tree,parent,node,index) {
	//
	//	// установка класса для отображения элемента
	//	if (!node.attributes.cls) {
	//		node.attributes.cls = 'node';
	//	}
	//
	//	node.draggable = true;
	//	node.allowDrag = true;
	//	node.allowDrop = true;
	//	node.editable = false;
	//});


	//// выбор элемента
	//__TREE.on('click', function(node,event) {
	//
	//	// запоминаем id выделенного элемента
	//	__TREE.selected_id = node.attributes.id;
	//
	//	// запоминаем путь
	//	__TREE.path = node.getPath('id');
	//
	//	// переходим к контент-странице элемента, если это не корень
	//	goAjaxNavigation(__TREE.getURL + __TREE.selected_id);
	//
	//	return true;
	//});


	//// Drag & Drop
	//// проверка при проносе над элементами, drap
	//__TREE.on('nodedragover', function(el) {
	//
	//	// запрет drag на элементе для добавления
	//	if (el.point == 'append') {
	//		el.cancel = true;
	//		return false;
	//	}
	//
	//	// запрет перемещения на самого себя
	//	if (el.target.attributes.id==el.dropNode.attributes.id) {
	//		el.cancel = true;
	//		return false;
	//	}
	//
	//	// запрет перемещения в другую категорию
	//	if (el.target.attributes.parent_id!=el.dropNode.attributes.parent_id) {
	//		el.cancel = true;
	//		return false;
	//	}
	//
	//	el.cancel = false;
	//	return true;
	//});


	//// запрет drop на элементе для добавления
	//__TREE.on('beforenodedrop', function(el) {
	//
	//	// запрет добавления
	//	if (el.point == 'append') {
	//		el.cancel = true;
	//		return false;
	//	}
	//
	//	// запрет перемещения на самого себя
	//	if (el.target.attributes.id==el.dropNode.attributes.id) {
	//		el.cancel = true;
	//		return false;
	//	}
	//
	//	// запрет перемещения в другую категорию
	//	if (el.target.attributes.parent_id!=el.dropNode.attributes.parent_id) {
	//		el.cancel = true;
	//		return false;
	//	}
	//
	//	var params = {
	//		'drag_id':	el.dropNode.attributes.id,
	//		'drop_id':	el.target.attributes.id,
	//		'point':		el.point
	//	};
	//
	//	$.ajax({
	//		url: __TREE.moveURL,
	//		type: 'post',
	//		async: false,			// !!! отключим асинхронную обработку - важно дождаться обработки на сервере
	//		cache: false,
	//		dataType: 'json',		// ответ сервера
	//		data: params,
	//		traditional: true,	// кодирование данных
	//
	//		complete: function(jqXHR, textStatus) {
	//
	//		},
	//
	//		success: function (data, textStatus, jqXHR) {
	//			el.cancel = false;
	//		},
	//
	//		error: function(jqXHR, textStatus, errorThrown) {
	//			el.cancel = true;
	//		}
	//	});
	//
	//	return !el.cancel;
	//});
}
