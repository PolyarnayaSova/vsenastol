//
// скрипт управления разделами параметров и параметрами
// Copyright (C) ESV Corp. 2011
//
$(document).ready(function(){

	// названия id блоков
	var main_groups = 'groups_sortable',
		 main_params = 'parameters_sortable', // блок перемещаемых параметров
		 main_block_groups = 'UL#' + main_groups, // блок групп и параметров
		 main_block_params = 'UL#groups_sortable UL#' + main_params;

	// блок источника групп и параметров
	var source_group = 'groups_sortable_source',
		 source_param = 'parameters_sortable_source',
		 source_group_copy = 'group_sortable_source_copy',
		 source_param_copy = 'parameters_sortable_source_copy';

	// блок полей ввода
	var group_input = '<label>название раздела: <input type="text" size="40" value="" name="" /></label>',
		 param_input = '<label>название параметра: <input type="text" size="40" value="" name="" /></label>';

	var id_prefix = 'id_';

	// создаем скрытый дубликат блока для создания новых групп и параметров
	var make_source_copy = function() {

		var group_block = $('ul#' + source_group),
			 param_block = null,
			 copy = null;

			if (!group_block || !group_block.length) {
				alert('Ошибка: Невозможно получение блока-источника: ' + source_group);
				return false;
			}

			copy = group_block.clone();

			if (!copy || !copy.length) {
				alert('Ошибка: Невозможно создание блока-копии: ' + source_group);
				return false;
			}

			copy.hide();

			copy.attr('id',source_group_copy);

			param_block = copy.find('ul#' + source_param);

			if (!param_block || !param_block.length) {
				alert('Ошибка: Невозможно получение блока праметров блока-источника: ' + source_param);
				return false;
			}

			param_block.attr('id',source_param_copy);

			copy.insertAfter(group_block);

		return true;
	};

	// создаем дубликат скрытого блока для создания новых групп и параметров
	// заменяем "отработанный" блок
	var clone_source_copy = function() {

		var block =  $('ul#' + source_group),
			 group_block_copy = null,
			 param_block_copy = null,
			 copy = null;

			if (!block || !block.length) {
				alert('Ошибка: Невозможно получение блока-источника: ' + source_group);
				return false;
			}

			block.remove();

			group_block_copy = $('ul#' + source_group_copy);

			if (!group_block_copy || !group_block_copy.length) {
				alert('Ошибка: Невозможно получение копии блока-источника: ' + source_group_copy);
				return false;
			}

			param_block_copy = group_block_copy.find('ul#' + source_param_copy)

			if (!param_block_copy || !param_block_copy.length) {
				alert('Ошибка: Невозможно получение копии блока-источника: ' + source_group_copy);
				return false;
			}

			// переустановка атрибутов
			group_block_copy.attr('id',source_group);
			param_block_copy.attr('id',source_param);

			group_block_copy.show();

			make_source_copy(); // создаем скрытый дубликат

		return true;
	};


	// для новой группы замена заголовка на поле ввода, установка id
	var group_init = function(group) {

		var input = null,
			 param = null;

			if (gcount==0 || group_multipler==0) {
				alert('Ошибка: Переменные не инициализированы');
				return false;
			}

			if (!group || !group.length) {
				alert('Ошибка: Раздел не определен');
				return false;
			}

			// устанавливаем id группы и параметр 'group'=номер
			group.attr('id',id_prefix + (gcount*group_multipler));
			group.attr('group',gcount*group_multipler);

			// вставляем input
			group.find('span').attr('title','').html(group_input);

			// устанавливаем id поля ввода
			group.find('input').attr('name',id_prefix + (gcount*group_multipler));

			// блок поля ввода
			param = group.find('ul#' + main_params + ' li');

			param_init(param);

			gcount++;

		return true;
	};


	// для нового параметра замена заголовка на поле ввода, установка id
	var param_init = function(param) {

			if (pcount==0 || group_multipler==0) {
				alert('Ошибка: Переменные не инициализированы');
				return false;
			}

			if (!param || !param.length) {
				alert('Ошибка: Параметр не определен');
				return false;
			}

		var group_num = param.parents('li.group').attr('group');


			if (!group_num) {
				alert('Ошибка: Не могу получить номер раздела');
				return false;
			}

			// устанавливаем id параметра
			param.attr('id',id_prefix + (group_num*1+pcount));

			// вставляем input
			param.find('span').attr('title','').html(param_input);

			// устанавливаем id поля ввода
			param.find('input').attr('name',id_prefix + (group_num*1+pcount));

			pcount++;

		return true;
	};


	// перемещение элементов, создание новых параметров и групп

	// получение, создание новой группы
	var receive_group = function (event,ui) {

		var group_block = $(ui.item),
			 param_block = $(ui.item).find('ul#' + source_param);

			if (!group_block || !param_block.length) {
				alert('Ошибка: Невозможно получение блока раздела');
				return false;
			}

			if (!param_block || !param_block.length) {
				alert('Ошибка: Невозможно получение блока параметров в новом разделе: ' + source_param);
				return false;
			}

			param_block.attr('id',main_params); // меняем id блока параметров таким образом, чтобы
															// определить принадлежность к блоку группы
			group_init(group_block);

		clone_source_copy(); // новая копия
		init();

		return true;
	};


	// получение, создание нового параметра
	var receive_param = function (event,ui) {

		param_init($(ui.item));

		clone_source_copy(); // новая копия
		init();

		return true;
	};


	// удаление элемента - параметра или группы
	$(main_block_groups).on('click','DIV.delete',function(event){

		var block = $(this).parent('div').parent('li');
		var messge = null;

		if (block.length)
			if (block.attr('class')=='group') {
				message = "Вы действительно хотите удалить данный раздел?\nВНИМАНИЕ! Все параметры в данном разделе будут также удалены.\nЗначения удаленных параметров у товаров данной категории будут также удалены.";
			} else {
				message = "Вы действительно хотите удалить данный параметр?\nВНИМАНИЕ! Значения параметра товаров данной категории будут также удалены.";
			}

		if (confirm(message)) {
			block.remove();
		}

		return false;
	});


	// инициализация списков
	var init = function() {

		// блок-источник
		// создание новой группы
		$('ul#' + source_group).sortable({
			cursor: 'n-resize',
			axis: 'y',
			opacity: 0.7,
			scroll: true,
			connectWith: main_block_groups
		}); //.disableSelection();

		// создание нового параметра
		$('ul#' + source_group + ' ul#' + source_param).sortable({
			cursor: 'n-resize',
			axis: 'y',
			opacity: 0.7,
			scroll: true,
			connectWith: main_block_params
		}); //.disableSelection();

		// основной блок
		// перемещение существующих групп
		$(main_block_groups).sortable({
			cursor: 'n-resize',
			axis: 'y',
			opacity: 0.7,
			containment: 'parent',
			scroll: false,
			receive: receive_group
		}); //.disableSelection();

		// перемещение существующих параметров
		$(main_block_params).sortable({
			cursor: 'n-resize',
			axis: 'y',
			opacity: 0.7,
			containment: 'parent',
			scroll: false,
			receive: receive_param
		}); //.disableSelection();
	};

	init_variables(); // инициализируем счетчики

	make_source_copy(); // создаем скрытый дубликат

	init();

});
