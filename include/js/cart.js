/* cart operations */
$(document).ready(function(){

	function calc_delivery(){
		if(!$('input[name=self]').prop('checked')){
			city=$('select[name=city] option:selected').val();
			if(city!=0){
				$('#delivery_string').show();
				
				price=parseInt($('.sum-price span').attr('data-price'));
				delivery_price=0;
				if(city==1&&price<3000){
					delivery_price=200;
				}
				else if((city==3||city==4||city==5)&&price<5000){
					delivery_price=300;
				}
				else if(city==2||city==10000){
					delivery_price=-1;
				}
				
				delivery_price_text="Договорная";
				if(delivery_price>=0){
					delivery_price_text=delivery_price+",00 руб";
				}
								
				$('.sum-delivery').text(delivery_price_text);
				$('.sum-price span').text(price+delivery_price);
				return;
			}
		}
		
		$('.sum-price span').text($('.sum-price span').attr('data-price'));
		$('#delivery_string').hide();
	}
	
	$('select[name=city]').change(function(){
		calc_delivery();
	});
	$('input[name=self]').change(function(){
		calc_delivery();
	});

	// добавить товар в корзину
	$('DIV.content').on('click','A.cart-add',function(){

		var href = '/cart/';
		var id = $(this).attr('id');
		var action = $(this).attr('action');
		var el = $(this);

		// для ослов - переход по ссылке
		if ($.browser.msie) {
			return true;
		}

		if (!id) {
			alert('Id не определен');
			return false;
		}

		if (!action || !('add'==action || 'del'==action)) {
			alert('Action не определен');
			return false;
		}

		$.ajax({
			url:href+action+'/'+id,
			async:false,
			cache:false,
			dataType:'html',
			type:'POST',
			data:null,
			success: function() {

				// перезагружаем блок корзины
				$('DIV.header DIV.cart').load(href+'cartblock');

				// меняем значение ссылки на противоположное
				if ('add'==action) {
					el.attr('action','del');
					el.attr('href',href+'del/'+id);
					el.text('из корзины');
				} else {
					el.attr('action','add');
					el.attr('href',href+'add/'+id);
					el.text('в корзину');
				}
			}
		});

		return false;
	});

	// установить подсказку на элемент
	var hint_on = function(el) {
		if (el.val()=='' && el.attr('title').length>0) {
			el.addClass('hint');
			el.val(el.attr('title'));
		}
	}

	// снять подсказку с элемента
	var hint_off = function(el) {
		if (el.val()==el.attr('title') && el.hasClass('hint')) {
			el.removeClass('hint');
			el.val('');
		}
	}

	// инициализация полей подсказок
	var hint_init = function(el) {
		if (el.attr('title') && (el.val()==el.attr('title') || el.val()=='')) {
			el.val('');
			hint_on(el);
		}
	}

	$('DIV.order-form INPUT').on('focus',function(){ hint_off($(this)); });
	$('DIV.order-form INPUT').on('focusout',function(){ hint_on($(this)); });
	$('DIV.order-form TEXTAREA').on('focus',function(){ hint_off($(this)); });
	$('DIV.order-form TEXTAREA').on('focusout',function(){ hint_on($(this)); });

	// устанавливаем подсказки на поля ввода
	$('DIV.order-form INPUT[type!="submit"]').each(function(){ hint_init($(this)); });
	$('DIV.order-form TEXTAREA').each(function(){ hint_init($(this)); });


	// перед отправкой проверять значения полей
	$('FORM#send_order').on('submit',function(){

		var acquainted = $('INPUT[name="acquainted"]').attr('checked');

		if (acquainted!='checked') {
			alert('Подтвердите свое согласие с Условиями доставки');
			return false;
		}

		city=$('select[name=city] option:selected').val();
		price=parseInt($('.sum-price span').attr('data-price'));
		if(city==1&&price<1500){
			alert('Необходимо увеличить заказ по товару до 1500');
			return false;
		}
		else if((city==3||city==4||city==5)&&price<2500){
			alert('Необходимо увеличить заказ по товару до 2500');
			return false;
		}
		else if((city==2||city==10000)&&price<2500){
			alert('Необходимо увеличить заказ по товару до 2500');
			return false;
		}
		
		$('INPUT.hint').each(function(){ $(this).val(''); });
		$('TEXTAREA.hint').each(function(){ $(this).val(''); });

		return true;
	});


	// измененное поле количества
	var changedInput = null;
	var updaterTimer = null;

	// изменение количества товаров в корзине
	// данная функция запускается по таймеру
	/*var updateCart = function() {

		if (updaterTimer) {
			clearTimeout(updaterTimer);
		}

		updaterTimer = null;

		if (changedInput == null) {
			  return false;
		}

		var id = parseInt(changedInput.attr('id'));
		var count = parseInt(changedInput.val());

		changedInput = null;

		$.ajax({
			url: '/cart/update/' + id + '/' + count,
			async:false,
			cache:false,
			type: 'POST',
			dataType: 'html',
			data: null
		})
		.success(function(response){
			$('DIV.order').html(response);
			$('DIV.header DIV.cart').load('/cart/cartblock');
		})
		.complete(function(){ })
		.error(function(jqXHR, textStatus){
			alert("Error: " + textStatus);
		});

		return true;
	}


	// установка таймера для запуска обновления корзины
	function bindUpdaterTimer() {

		if (updaterTimer) {
			clearTimeout(updaterTimer);
		}
		updaterTimer = setTimeout(updateCart, 1000);
	}*/


	// обработка полей ввода количества товаров
   // привязка к полям ввода для проверки ввода только числовых значений
   $('DIV.order').on('keypress','INPUT',function(e) {

		var code = e.which;

		if (code != 0 && code != 8 && code != 13 && (code < 47 || e.which > 58)) {
			 e.preventDefault();
			 return false;
		}

		changedInput = $(this);

		// при изменении значения повесим на пересчет функцию
		//bindUpdaterTimer();

		return true;

	}).on('change','INPUT',function(e) {

		changedInput = $(this);
		var id = parseInt(changedInput.attr('id'));
		var count = parseInt(changedInput.val());
		$.ajax({
			url: '/cart/update/' + id + '/' + count,
			async:false,
			cache:false,
			type: 'POST',
			dataType: 'html',
			data: null
		})
		.success(function(response){
			console.log(response);
			obj = JSON.parse(response);
			$('.sum-count').text(obj.count);
			$('.sum-price span').text(obj.summa);
			$('.sum-price span').attr('data-price',obj.summa);
			calc_delivery();
			//$('DIV.order').html(response);
			$('DIV.header DIV.cart').load('/cart/cartblock');
		})
		.complete(function(){ })
		.error(function(jqXHR, textStatus){
			console.log(jqXHR);
			//alert("Error: " + textStatus);
		});

		return true;
	});

	// удаление товаров из корзины
	$('DIV.order').on('click','A.del',function(){

		var answer = confirm('Вы действительно хотите удалить данный товар из корзины?');

		if ($.browser.msie) {
			return answer;
		}
		
		$tr=$(this).parent().parent();

		if (answer) {
			$.ajax({
				url: $(this).attr('href'),
				async:false,
				cache:false,
				type: 'POST',
				dataType: 'html',
				data: null,
				success: function(response) {
					obj = JSON.parse(response);
					$('.sum-price span').text(obj.summa);
					$('.sum-price span').attr('data-price',obj.summa);
					$('.sum-count').text(obj.count);
					$tr.remove();
					calc_delivery();
					//$('DIV.order').html(response);
					$('DIV.header DIV.cart').load('/cart/cartblock');
				}
			});
		}

		return false;
	});
});
