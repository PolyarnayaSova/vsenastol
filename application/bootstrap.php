<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
///date_default_timezone_set('America/Chicago');
date_default_timezone_set('Asia/Yekaterinburg');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
///setlocale(LC_ALL, 'en_US.utf-8');
setlocale(LC_ALL, 'ru_RU.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
///I18n::lang('en-us');
I18n::lang('ru-ru');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
$_SERVER['KOHANA_ENV'] = 'development';

if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
	'index_file' => FALSE,
	'errors'		 => TRUE
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(

	'basepage'				=>	MODPATH .'admin/modules/basepage',	// Basepage template
	'structure'				=> MODPATH .'admin/modules/structure',	// Structures: Tree, Tree_ORM, Controller_Tree_Interface
																					// List_Sorted_ORM, List_Sorted_Item_ORM,
																					// List_Sorted_Image_ORM, List_Sorted_File_ORM
	'admin'					=> MODPATH .'admin',							// Administration tools

	'admin/content'		=> MODPATH .'admin/modules/content',		// Administrator 'Content' part of module
	'admin/brand'			=> MODPATH .'admin/modules/brand',			// Administrator 'Brands' part of module
	'admin/news'			=> MODPATH .'admin/modules/news',			// Administrator 'News' part of module
	'admin/advice'			=> MODPATH .'admin/modules/advice',			// Administrator 'Advice' part of module
	'admin/product'		=> MODPATH .'admin/modules/product',		// Administrator 'Product' part of module
	'admin/order'			=> MODPATH .'admin/modules/order',			// Administrator 'Order' part of module
	'admin/loader'			=> MODPATH .'admin/modules/loader',			// Administrator 'Loader of Price & Images' part of module
	'admin/access'			=> MODPATH .'admin/modules/access',			// Administrator 'Access' part of module

	'auth'       => MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	'database'   => MODPATH.'database',   // Database access
	'image'      => MODPATH.'image',      // Image manipulation
	'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	// 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	'pagination'	=> MODPATH.'pagination',	// Постраничная навигация
	'pigeon'			=> MODPATH.'pigeon',			// Mailer
	));


/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */

Route::set('category', 'category/<category0>/<c0_id>(/<category1>/<c1_id>)(/<all>)',array('c0_id'=>'\d+','c1_id'=>'\d+','all'=>'all'))
	->defaults(array(
		'controller' => 'category',
		'action'     => 'index',
	));

Route::set('product', 'product/<product>/<p_id>(/<category1>/<c1_id>(/<category0>/<c0_id>))',array('p_id'=>'\d+','c0_id'=>'\d+','c1_id'=>'\d+'))
	->defaults(array(
		'controller' => 'category',
		'action'     => 'product',
	));

Route::set('search', 'search(/<advanced>)(/<search>(/<all>))',array('all'=>'all','advanced'=>'advanced'))
	->defaults(array(
		'controller' => 'category',
		'action'     => 'search',
	));

Route::set('advancedsearch', 'advancedsearch')
	->defaults(array(
		'controller' => 'category',
		'action'     => 'advancedsearch',
	));

Route::set('actions', 'actions')
	->defaults(array(
		'controller' => 'category',
		'action'     => 'actions',
	));

Route::set('news', 'news(/<action>(/<id>))')
	->defaults(array(
		'controller' => 'news',
		'action'     => 'index',
	));

Route::set('advices', 'advices(/<action>(/<id>))')
	->defaults(array(
		'controller' => 'advices',
		'action'     => 'index',
	));

Route::set('brands', 'brands(/<action>(/<brand>/<id>)(/<all>))',array('id'=>'\d+','all'=>'all'))
	->defaults(array(
		'controller' => 'brands',
		'action'     => 'index',
	));

Route::set('cart', 'cart(/<action>(/<id>(/<count>)))',array('id'=>'\d+','count'=>'\d+'))
	->defaults(array(
		'controller' => 'cart',
		'action'     => 'order',
	));

Route::set('error', 'error(/<action>)')
	->defaults(array(
		'controller' => 'error',
		'action'     => '404',
	));

Route::set('page', '<page>')
	->defaults(array(
		'controller' => 'pages',
		'action'     => 'index',
	));

Route::set('default', '(<action>(/<id>))')
	->defaults(array(
		'controller' => 'application',
		'action'     => 'index',
	));
