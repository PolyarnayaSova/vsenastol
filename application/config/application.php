<?php defined('SYSPATH') or die('No direct script access.');

// настройки приложения
return array (
	'css' => array (
		'prefix' => 'include/css/',
		'files' => array (
			'application.css?v=5',
			'product.css?v=3',
			'pager.css?v=3'
		)
	),
	'js' => array (
		'prefix' => 'include/js/',
		'files' => array (
		)
	),
	'cookie_salt' => 'PosudaCookieSalt',
	'mail' => array (
//		'from' => 'inet-shop@UHET.ru',
//		'to'	=> 'Serge@UHET.ru',
		'from' => array('inet-shop-vsenastol@vsenastol.ru'=>'Интернет-магазин "Все на стол"'),
		'to'	=> array('info@vsenastol.ru'),
		'subject' => 'Заказ в Интернет-магазине "Все на стол"',
	),
	// название интернет-магазина
	'shop' => 'Интернет-магазин "Все на стол"',
);
?>
