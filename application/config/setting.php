<?php defined('SYSPATH') or die('No direct script access.');

// установки, настройки приложения

return array (
		'icon' => 'include/icon/application.png', // иконка приложения
		'cookie_message' => 'application_message_cookie', // название закладки для сообщений
		'cookie_region' => 'application_region', // название закладки региона
		'cookie_back' => 'application_back_href' // название закладки для возврата к определенной странице
	);
?>
