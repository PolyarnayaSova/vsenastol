<?php defined('SYSPATH') or die('No direct script access.');

	// параметры постраничной навигации
	return array (
		// для списка товаров
		'products' => array (
			'total_items'		=> 0, // заполняется при вычислении
			'items_per_page'	=> 12,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			'view'		=> 'pager/product_floating'
		),
		// для списка новостей
		'news' => array (
			'total_items'		=> 0, // заполняется при вычислении
			'items_per_page'	=> 10,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			'view'		=> 'pager/news_floating'
		),
		// для списка полезных советов
		'advices' => array (
			'total_items'		=> 0, // заполняется при вычислении
			'items_per_page'	=> 10,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			'view'		=> 'pager/advices_floating'
		),
		// для списка новостей
		'brands' => array (
			'total_items'		=> 0, // заполняется при вычислении
			'items_per_page'	=> 10,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			'view'		=> 'pager/brands_floating'
		)
	);
?>
