<?php defined('SYSPATH') or die('No direct script access.');
//
// @author ESV Corp. (C) 12.2011
//
// Работа с корзиной
//
class Cart {

	private function __construct() { }
	private static $cart = null;


	private static function init() {

		if (self::$cart==null) {
			self::$cart = array('count'=>0,'price'=>'0','products'=>array());
		}

		return self::$cart;
	}


	private static function save($cart=array()) {

		if (empty($cart)) {
			$cart = self::init();
		}

		self::$cart = $cart;

		$cart = serialize($cart);

		Cookie::set('cart',$cart);

		return self::$cart;
	}


	public static function delete() {
		Cookie::delete('cart');
		self::$cart = null;
	}


	public static function get() {

		if (self::$cart==null) {
			$cart = Cookie::get('cart',array());
			if (empty($cart)) {
				$cart = self::init();
			} else {
				$cart = unserialize($cart);
				self::$cart = $cart;
			}
		} else {
			$cart = self::$cart;
		}

		return $cart;
	}


	// добавить товар в корзину
	public static function add($id) {

		$cart = self::get();

		if (!in_array($id,array_keys($cart['products']))) {

			$product = new Model_Product($id);

			if (!$product->loaded()) {
				throw new HTTP_Exception_404('Корзина: товар не найден');
			}

			$cart['products'][$id] = 1;
			$cart['count']++;
			$cart['price']+= $product->get_price();
		}

		return self::save($cart);
	}


	// изменить количество товара в корзине
	public static function update($id,$count) {

		$cart = self::get();

		if (in_array($id,array_keys($cart['products']))) {
			if ($count<=0) {
				unset($cart['products'][$id]);
			} else {
				$cart['products'][$id] = $count;
			}
		}
		$count = 0;
		$price = 0;
		foreach ($cart['products'] as $p) {
			$count+=$p;
		}

		if ($count>0) {
			$products = new Model_Product();
			$products = $products->where('id','IN',array_keys($cart['products']))->find_all();

			foreach ($products as $p) {
				$price += $p->get_price()*$cart['products'][$p->id];
			}
		}

		$cart['count'] = $count;
		$cart['price'] = $price;

		return self::save($cart);
	}


	// удаление товара из корзины
	public static function del($id) {

		$cart = self::get();

		if (in_array($id,array_keys($cart['products']))) {

			$product = new Model_Product($id);

			if (!$product->loaded()) {
				throw new HTTP_Exception_404('Корзина: товар не найден');
			}

			$cart['price'] -= $product->get_price()*$cart['products'][$id];
			$cart['count'] -= $cart['products'][$id];

			unset($cart['products'][$id]);
		}

		return self::save($cart);
	}


	public static function in_cart($id) {

		$cart = self::get();

		return in_array($id,array_keys($cart['products']));
	}
}
