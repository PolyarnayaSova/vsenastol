<?php defined('SYSPATH') or die('No direct script access.');
//
// @author ESV Corp. (C) 12.2011
//
// Exception handler
//
class Error extends Kohana_Exception {

   public function __construct($message = 'Unknown error', array $variables = NULL, $code = 0) {
      parent::__construct($message, $variables, $code);
   }


	public static function handler(Exception $e) {

      switch (get_class($e)) {
			case 'HTTP_Exception_404':
			case 'Kohana_Request_Exception':
				$request = Request::factory(Route::url('error',array('action'=>'404')))->execute();
				print $request->send_headers()->body();
				return TRUE;
				break;

			default:
				return parent::handler($e);
      }
	}
}

?>
