<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 11.2011
//
// Контроллер Категории
//

class Controller_Category extends Controller_Application {


	// формирование пути для категории
	private function path($path,$all_href=false) {

		$path_str = HTML::anchor('/','Главная');

		$c0_id = 0;
		$c0_name = '';


		$count = count($path);

		if ($count>2) {
			throw new Kohana_Exception('Превышение допустимого уровня категорий');
		}

		foreach ($path as $p) {
			if ($p->parent_id==0) {
				$c0_id = $p->id;
				$c0_name = Text::symbols($p->name);
				$url = Route::url('category',array('category0'=>$c0_name,'c0_id'=>$c0_id));
			} else {
				$url = Route::url('category',array('category0'=>$c0_name,'c0_id'=>$c0_id,
															  'category1'=>Text::symbols($p->name),'c1_id'=>$p->id));
			}

			if (--$count==0 && !$all_href) {
				$path_str .= ' / ' . $p->name;
			} else {
				$path_str .= ' / ' . HTML::anchor($url,$p->name);
			}
		}

		return $path_str;
	}


	public function before() {
		parent::before();

		parent::scripts('cart.js');
	}


	public function action_index() {

		$c0_id = $this->request->param('c0_id',0);
		$c1_id = $this->request->param('c1_id',0);

		if ($c1_id) {

			$level = 2;

			$category = new Model_Product_Category($c1_id);
			if (!$category->loaded()) {
				throw new HTTP_Exception_404('Подкатегория не найдена');
			}
			if ($category->parent_id!=$c0_id) {
				throw new Kohana_Exception('Несоответствие id категорий');
			}

			if ($category->title) {
				$this->title = $category->title;
			} else {
				$parent_category = new Model_Product_Category($c0_id);

				if ($parent_category->loaded()) {
					$parent_name = ' / ' . $parent_category->name;
				} else {
					$parent_name = '';
				}

				$this->title = ($category->title ? $category->title : $category->name) . $parent_name;

				if (!empty($category->text)) {
					Content::$footer_content = $category->text;
				}
			}

		} elseif ($c0_id) {

			$level = 1;

			$category = new Model_Product_Category($c0_id);
			if (!$category->loaded()) {
				throw new HTTP_Exception_404('Категория не найдена');
			}

			$this->title = $category->title ? $category->title : $category->name;

			if (!empty($category->text)) {
				Content::$footer_content = $category->text;
			}

		} else {
			throw new HTTP_Exception_404('Категория не указана');
		}

		$this->category_menu = $category->path_tree();

		$this->keywords = $category->keywords;
		$this->description = $category->description;

		$this->path = self::path($category->path());

		if ($level==1) {
			$this->content = Content::category_list($category->id);
		} else {

			// отображение списка товаров в категории
			$products = $category->products->where('rest','>',0);

			// параметр отображения всех товаров без постраничной навигации
			$all_param = $this->request->param('all');

			if ($all_param=='all') {

				$products = $products->find_all();

				$pager = null;

				$url_all = null;

			} else {

				// постраничная навигация
				$pager = Kohana::$config->load('pager.products');

				$page = max(1, arr::get($_GET, 'page', 1));

				$limit = $pager['items_per_page'];
				$offset = $limit * ($page - 1);

				$pager['total_items'] = count($products->reset(false)->find_all());

				$products = $products->offset($offset)->limit($limit)->find_all();

				$url_all = $this->request->url() . '/all';

			}

			$this->content = Content::product_list($category,$products,$pager,$url_all);
		}
	}


	// список товаров, учавствующих в акции
	public function action_actions() {

		$products = new Model_Product();
		$products = $products->where('action','!=',0)->order_by('price');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.products');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($products->reset(false)->find_all());

		$products = $products->offset($offset)->limit($limit)->find_all();

		if (count($products) > 0) {
			$this->content = '<h1 class="title">Товары, участвующие в акциях</h1>' . Content::product_list(new Model_Product_Category(), $products, $pager);
		} else {
			$this->content = '<h1 class="title">Не найдены товары, участвующие в акциях</h1>';
		}
	}


	// информация о товаре
	public function action_product() {

		$p_id = $this->request->param('p_id');

		$product = new Model_Product($p_id);

		if (!$product->loaded()) {
			throw new HTTP_Exception_404('Товар не найден');
		}

		$category = $product->category;

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория товара не найдена');
		}

		// формируем title
		$c0_id = $this->request->param('c0_id');
		$c1_id = $this->request->param('c1_id');

		$category0 = new Model_Product_Category($c0_id);
		$category1 = new Model_Product_Category($c1_id);

		if ($category0->loaded()) {
			$c0_name = ' / ' . $category0->name;
		} else {
			$c0_name = '';
		}

		if ($category1->loaded()) {
			$c1_name = ' / ' . $category1->name;
		} else {
			$c1_name = '';
		}

		$this->title = $product->name . $c1_name . $c0_name;
		$this->description = $product->brief;

		//$this->title = $product->title;
		//$this->keywords = $product->keywords;
		//$this->description = $product->description;

		$this->category_menu = $category->path_tree();

		$this->path = self::path($category->path(),true);

		//$brand = $product->brand;
		//if ($brand->loaded()) {
		//	$brand = $brand->name;
		//} else {
		//	$brand = null;
		//}
		//
		//$material = $product->material;
		//if ($material->loaded()) {
		//	$material = $material->name;
		//} else {
		//	$material = null;
		//}

		$this->content =
			View::factory('product/product')
				->set('product',$product)
				->set('category',$category);
				//->set('brand',$brand)
				//->set('material',$material);
	}


	// поиск
	public function action_search() {

		if ($this->request->method()=='POST') {

			// инициация поиска
			// построение строки запроса

			$search_string = Text::symbols(trim(Arr::get($_POST,'search','')));

			if (!empty($search_string)) {

				$redirect = Route::url('search',array('search'=>$search_string));

				$this->request->redirect($redirect);

			} else {
				$this->request->redirect('/');
			}

			return;
		}

		$search_string = Text::symbols(trim($this->request->param('search','')));
		$advanced = ($this->request->param('advanced','')=='advanced');

		// разбивка на фразы поиска
		$words = preg_split('/[^\pL\pN]+/ui',$search_string);

		// списки окончаний
		$postfix = array (
			'ая', 'ое', 'ые', 'ый', 'ий', 'ов'
		);

		$result = array();
		foreach ($words as $k => $w) {
			$res_string = $w;
			foreach ($postfix as $p) {
				$res_string = rtrim($res_string,$p);
			}

			if (!empty($res_string)) {
				$result[] = $res_string;
			}
		}
		$words = $result;

		$products = new Model_Product();

		$brands_names = null;
		$categories_names = null;

		// при расширенном поиске учитываем дополнительные параметры
		if ($advanced) {

			// бренды
			//$brands_checked = Cookie::get('brands_advanced_search',null);
			// поиск помеченных брендов
			$brands_checked = array();
			foreach ($_GET as $key => $val) {
				if (preg_match('/brand_(\d+)/',$key,$match)) {
					$brands_checked[] = $match[1];
				}
			}


			// поиск помеченных категорий
			$categories_checked = array();
			foreach ($_GET as $key => $val) {
				if (preg_match('/category_(\d+)/',$key,$match)) {
					$categories_checked[] = $match[1];
				}
			}


			if (!empty($brands_checked)) {
			//	$brands_checked = unserialize($brands_checked);
				if (!is_array($brands_checked)) {
					$brands_checked = array();
				}

			} else {
				$brands_checked = array();
			}

			if (!empty($brands_checked)) {

				$brands = Model_Content::get_by_key('brands');

				if (!$brands->loaded()) {
					throw new HTTP_Exception_404('Блок брендов не найден');
				}

				$brands_names = $brands->contents;

				$brands_names = $brands_names->where('id','IN',$brands_checked)->order_by('name')->find_all();
				$brands_names_result = array();
				foreach ($brands_names as $bn) {
					$brands_names_result[] = $bn->name;
				}
				$brands_names = join(', ',$brands_names_result);

				$products->and_where('brand_id','IN',$brands_checked);

			} else {
				$brands_names = null;
			}

			// категории
			//$categories_checked = Cookie::get('categories_advanced_search',null);
			if (!empty($categories_checked)) {
			//	$categories_checked = unserialize($categories_checked);
				if (!is_array($categories_checked)) {
					$categories_checked = array();
				}
			} else {
				$categories_checked = array();
			}

			if (!empty($categories_checked)) {
				$categories_names = new Model_Product_Category();
				$categories_names = $categories_names->where('id','IN',$categories_checked)->order_by('name')->find_all();
				$categories_names_result = array();
				foreach ($categories_names as $cn) {
					$categories_names_result[] = $cn->name;
				}
				$categories_names = join(', ',$categories_names_result);

				$products->and_where('category_id','IN',$categories_checked);
			}

			$title = "Расширенный поиск";

		} else {

			$title = "Поиск";
		}

		// пустой список поисковых фраз и пустой расширенный запрос - нет смысла строить запрос - возвращаемся на базу
		if (empty($words) && empty($brands_checked) && empty($categories_checked)) {
			$this->request->redirect('/');
			return;
		}

		// сохраняем строку поиска для отображения в поле ввода
		$this->search_string = $search_string;

		$this->title = "$title: $search_string";

		// поля, в которых производится поиск
		$fields = array (
			'name',
			'brief',
			'text',
			'article'
		);

		// построение запроса
		foreach ($words as $w) {
			$products->and_where_open();
			foreach ($fields as $f) {
				$products->or_where($f,'LIKE',"%$w%");
			}
			$products->and_where_close();
		}

		$products->and_where('rest','>',0)->order_by('price');

		// параметр отображения всех товаров без постраничной навигации
		$all_param = $this->request->param('all');

		if ($all_param) {

			$products = $products->find_all();

			$pager = null;

			$url_all = null;

		} else {

			// постраничная навигация
			$pager = Kohana::$config->load('pager.products');

			$page = max(1, arr::get($_GET, 'page', 1));

			$limit = $pager['items_per_page'];
			$offset = $limit * ($page - 1);

			$pager['total_items'] =$products->reset(false)->count_all();

			$products = $products->offset($offset)->limit($limit)->find_all();

			$url_all = $this->request->url() . '/all';

		}

		$this->content =
			View::factory('product/list')
				->set('title',$title)
				->set('products',$products)
				->set('pager',$pager)
				->set('url_all',$url_all)
				->set('advanced',$advanced)
				->set('brands_names',$brands_names)
				->set('categories_names',$categories_names);
	}


	// расширенный поиск
	public function action_advancedsearch() {

		if (!empty($_GET)) {
			$search_string = Text::symbols(trim(Arr::get($_GET,'search','')));
			$redirect = Route::url('search',array('search'=>$search_string,'advanced'=>'advanced'));
			$uriParts = explode('?',$_SERVER['REQUEST_URI']);
			//echo $redirect.'?'.$uriParts[1];

			$this->request->redirect($redirect.'?'.$uriParts[1]);
		}
		/*if ($this->request->method()=='POST') {

			// поиск помеченных брендов
			$brands_checked = array();
			foreach ($_POST as $key => $val) {
				if (preg_match('/brand_(\d+)/',$key,$match)) {
					$brands_checked[] = $match[1];
				}
			}

			Cookie::set('brands_advanced_search',serialize($brands_checked));

			// поиск помеченных категорий
			$categories_checked = array();
			foreach ($_POST as $key => $val) {
				if (preg_match('/category_(\d+)/',$key,$match)) {
					$categories_checked[] = $match[1];
				}
			}

			Cookie::set('categories_advanced_search',serialize($categories_checked));

			$search_string = Text::symbols(trim(Arr::get($_POST,'search','')));
			$redirect = Route::url('search',array('search'=>$search_string,'advanced'=>'advanced'));
			$this->request->redirect($redirect);

			return;
		}*/

		$brands = Model_Content::get_by_key('brands');

		if (!$brands->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		$brands = $brands->contents->where('visible','!=',0)->order_by('name')->find_all();

		$categories = new Model_Product_Category();
		$categories = $categories->where('visible','!=',0)->where('parent_id','=',0)->order_by('position')->find_all();

		$brands_checked = Cookie::get('brands_advanced_search',null);
		if (!empty($brands_checked)) {
			$brands_checked = unserialize($brands_checked);
			if (!is_array($brands_checked)) {
				$brands_checked = array();
			}
		} else {
			$brands_checked = array();
		}

		$categories_checked = Cookie::get('categories_advanced_search',null);
		if (!empty($categories_checked)) {
			$categories_checked = unserialize($categories_checked);
			if (!is_array($categories_checked)) {
				$categories_checked = array();
			}
		} else {
			$categories_checked = array();
		}

		$this->title = 'Расширенный поиск';

		parent::css('advancedsearch.css?v=3');

		$this->content =
			View::factory('application/advancedsearch')
				->set('title','Расширенный поиск')
				->set('brands',$brands)
				->set('brands_checked',$brands_checked)
				->set('categories',$categories)
				->set('categories_checked',$categories_checked);
	}
}
