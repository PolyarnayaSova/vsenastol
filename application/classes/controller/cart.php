<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 12.2011
//
// Контроллер Корзина
//

class Controller_Cart extends Controller_Application {

	// блок корзины
	public function action_cartblock() {

		$this->auto_render = false;

		$this->response->body(Content::cart_block_header());
	}


	// добавить в корзину
	public function action_add() {

		$id = $this->request->param('id');

		$product = new Model_Product($id);

		if ($product->loaded()) {
			Cart::add($id);
		}

		if ($this->request->is_ajax()) {
			$this->auto_render = false;
		} else {
			$this->request->redirect($this->request->referrer());
		}
	}


	// изменить количество товара в корзине
	public function action_update() {

		$id = $this->request->param('id');
		$count = $this->request->param('count');

		if (Cart::in_cart($id)) {
			Cart::update($id,$count);
		}

		return self::action_order();
	}


	// удалить из корзины на странице товара или в списке товаров
	public function action_del() {

		$id = $this->request->param('id');

		$product = new Model_Product($id);

		if ($product->loaded()) {
			Cart::del($id);
		}

		if ($this->request->is_ajax()) {
			$this->auto_render = false;
		} else {
			$this->request->redirect($this->request->referrer());
		}
	}


	// удалить товар на странице заказа
	public function action_delfromorder() {

		$id = $this->request->param('id');

		Cart::del($id);

		return self::action_order();
	}


	// страница Корзина, Заказ
	public function action_order() {

		$cart = Cart::get();

		if (empty($cart['products'])) {
			$products = null;
		} else {
			$products = new Model_Product();
			$products = $products->where('id','IN',array_keys($cart['products']))->order_by('name')->find_all();
		}

		if ($this->request->is_ajax()) {

			$this->auto_render = false;
			$summa=0;
			$count=0;
			foreach ($products as $p){
				$summa+=$p->get_price()*$cart['products'][$p->id];
				$count += $cart['products'][$p->id];
			}
			
			echo json_encode((object)array(
				/*'cart'=>View::factory('cart/order')
					->set('cart',$cart)
					->set('products',$products)
					->set('step',1)
					->set('is_ajax',true)->render(),*/
				'summa'=>$summa,
				'count'=>$count,
			));
			
			/*$this->response->body(
				View::factory('cart/order')
					->set('cart',$cart)
					->set('products',$products)
					->set('is_ajax',true)
			);*/

		} else {

			parent::css('order.css?v=3');
			parent::scripts('cart.js');

			$this->content =
				View::factory('cart/order')
					->set('cart',$cart)
					->set('products',$products)
					->set('step',1)
					->set('cities',Model_City::get_select())
					->set('is_ajax',false);
		}
	}


	// страница условий доставки
	public function action_terms() {

		$terms = Model_Content::get_by_key('delivery_terms');

		if (!$terms->loaded()) {
			throw new HTTP_Exception_404('Страница условий доставки не найдена');
		}

		$this->content = $terms->text;
	}


	// оформление заказа
	public function action_send() {

		if ($this->request->method()!='POST') {
			throw new HTTP_Exception_400('Неверный запрос');
		}

		parent::css('order.css?v=1');
		parent::scripts('cart.js');

		$cart = Cart::get();

		if (empty($cart['products'])) {
			$products = null;
		} else {
			$products = new Model_Product();
			$products = $products->where('id','IN',array_keys($cart['products']))->order_by('name')->find_all();
		}

		$fio = htmlspecialchars(Arr::get($_POST,'fio'));
		$email = htmlspecialchars(Arr::get($_POST,'email'));
		$phone = htmlspecialchars(Arr::get($_POST,'phone'));
		$addr = htmlspecialchars(Arr::get($_POST,'addr'));
		$city = Arr::get($_POST,'city',0);
		$text = htmlspecialchars(Arr::get($_POST,'text'));
		$self = Arr::get($_POST,'self',0);

		$warning_message = false;
		if (empty($fio) || empty($phone) || empty($addr) || empty($city)) {
			$warning_message = 'Заполните обязательные поля';
		}
		
		$step = Arr::get($_POST,'step',1);

		if ($warning_message||$step==1) {
			if($warning_message){
				$step=1;
			}
			else{
				$step=2;
			}
			$this->content =
				View::factory('cart/order')
					->set('step',$step)
					->set('cart',$cart)
					->set('products',$products)
					->set('fio',$fio)
					->set('email',$email)
					->set('phone',$phone)
					->set('addr',$addr)
					->set('city',$city)
					->set('cities',Model_City::get_select())
					->set('text',$text)
					->set('self',$self)
					->set('warning_message',$warning_message)
					->set('is_ajax',false);
			return;
		}

		// обработка позиций
		$cart_all = Cart::get();
		if (empty($cart_all['products'])) {
			$products = null;
		} else {
			$products = new Model_Product();
			$products = $products->where('id','IN',array_keys($cart_all['products']))->order_by('name')->find_all();
		}
		$count_all = 0;
		
		$cart = array();
		foreach ($products as $p){
			$cart[$p->id] = $cart_all['products'][$p->id];
			$count_all += $cart_all['products'][$p->id];
		}

		if ($count_all>0) {
			$products = new Model_Product();
			$products = $products->where('id','IN',array_keys($cart))->order_by('name')->find_all();

			if (count($products)==0) {
				$this->content =
					View::factory('cart/order_done')
						->set('title','Заказ не отправлен')
						->set('comment','Не выбрано ни одного существующего товара');
			}

			// построение таблицы товаров
			$table_products =
				View::factory('cart/order_table_products')
					->set('city_id', $city)
					->set('cart',$cart)
					->set('products',$products)
					->set('with_photos',false)
					->render();

			$internal_table_products =
				View::factory('cart/order_table_products')
					->set('city_id', $city)
					->set('cart',$cart)
					->set('products',$products)
					->set('with_host',false)
					->render();

			$blank_table_products =
				View::factory('cart/order_table_products_blank')
					->set('city_id', $city)
					->set('cart', $cart)
					->set('products', $products)
					->set('delivery', Model_City::delivery($city))
					->set('discount', 0);

			$blank_list_table_products =
				View::factory('cart/order_table_products_blank_list')
					->set('city_id', $city)
					->set('cart', $cart)
					->set('products', $products);

			// запись информации о заказе
			$order = new Model_Order();
			$order->fio = $fio;
			$order->email = $email;
			$order->phone = $phone;
			$order->addr = $addr;
			$order->city = $city;
			$order->text = $text;
			$order->self = $self;
			$order->info = $internal_table_products;
			$order->blank = $blank_table_products;
			$order->blank_list = $blank_list_table_products;
			$order->save();

			$order_num = $order->id;

			unset($internal_table_products);
			unset($blank_table_products);
			unset($blank_list_table_products);

			// формирование сообщений для отправки
			// клиенту
			$mail_client =
				View::factory('cart/mail_order_client')
					->set('order_num',$order_num)
					->set('fio',$fio)
					->set('email',$email)
					->set('phone',$phone)
					->set('addr',$addr)
					->set('city',Model_City::name($city))
					->set('text',$text)
					->set('self',$self)
					->set('table_products',$table_products)
					->render();

			// в магазин
			$mail_shop =
				View::factory('cart/mail_order_shop')
					->set('order_num',$order_num)
					->set('fio',$fio)
					->set('email',$email)
					->set('phone',$phone)
					->set('addr',$addr)
					->set('city',Model_City::name($city))
					->set('text',$text)
					->set('self',$self)
					->set('table_products',$table_products)
					->render();

			// почтовик

			//if (false) {
			$mailer = new Mailer("fallback");

			$from = Kohana::$config->load('application.mail.from');
			$to = Kohana::$config->load('application.mail.to');
			$subject = "Заказ № $order_num. " . Kohana::$config->load('application.mail.subject');

			// подготовка письма к отправке клиенту
			if (!empty($email) && Valid::email($email)) {
				$mailer->setFrom($from);
				$mailer->setTo($email);
				$mailer->setSubject($subject);
				$mailer->setBody($mail_client,'text/html'); // заявка в магазин

				$sended_client = $mailer->send();
			} else {
				$sended_client = true;
			}

			// подготовка письма к отправке в магазин
			if ($sended_client) {

				$mailer->setFrom($from);
				$mailer->setTo($to);
				$mailer->setSubject($subject);
				$mailer->setBody($mail_shop,'text/html'); // заявка в магазин

				$sended_shop = $mailer->send();

			} else {
				$sended_shop = false;
			}
			//} else {
			//	$sended_client = $sended_shop = true;
			//}

			if ($sended_client && $sended_shop) {
				$this->content =
					View::factory('cart/order_done')
						->set('title',"Заказ №{$order_num} успешно отправлен")
						->set('comment','В скором времени наш менеджер свяжется с Вами по телефону');
				Cart::delete();
			} else {
				$this->content =
					View::factory('cart/order_done')
						->set('title','Заказ не отправлен')
						->set('comment','Ошибка отправки почтовых сообщений');
				$order->delete();
			}

		} else {
			$this->content =
				View::factory('cart/order_done')
					->set('title','Заказ не отправлен')
					->set('comment','Не найдено товаров для заказа');		}
	}
}
