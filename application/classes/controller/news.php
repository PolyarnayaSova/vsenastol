<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 12.2011
//
// Отображение Новостей
//

class Controller_News extends Controller_Application {

	public function before() {

		parent::before();

		parent::css('news.css');
	}

	public function action_index() {

		$news = Model_Content::get_by_key('news');

		if (!$news->loaded()) {
			throw new HTTP_Exception_404('Блок новостей не найден');
		}

		if ($news->title) {
			$this->title = $news->title;
		} else {
			$this->title = $news->name;
		}

		$this->description = $news->description;
		$this->keywords = $news->keywords;

		$news = new Model_News();

		$news->order_by('date','DESC');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.news');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($news->reset(false)->find_all());

		$news = $news->offset($offset)->limit($limit)->find_all();

		$this->content =
			View::factory('news/list')
				->set('news',$news)
				->set('pager',$pager);
	}

	public function action_show() {

		$id = $this->request->param('id',0);

		$news = new Model_News($id);

		if (!$news->loaded()) {
			throw new HTTP_Exception_404('Новость не найдена');
		}

		if ($news->title) {
			$this->title = $news->title;
		} else {
			$this->title = $news->name;
		}

		if ($news->description) {
			$this->description = $news->description;
		} else {
			$this->description = $news->brief;
		}

		$this->path = HTML::anchor('/','Главная') . ' / ' . HTML::anchor(Route::url('news'),'Новости');

		$this->content = View::factory('news/show')->set('news',$news);
	}
}
