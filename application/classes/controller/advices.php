<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 08.2012
//
// Отображение Полезных советов
//

class Controller_Advices extends Controller_Application {

	public function before() {

		parent::before();

		parent::css('advices.css');
	}

	public function action_index() {

		$advices = Model_Content::get_by_key('advices');

		if (!$advices->loaded()) {
			throw new HTTP_Exception_404('Блок советов не найден');
		}

		if ($advices->title) {
			$this->title = $advices->title;
		} else {
			$this->title = $advices->name;
		}

		$this->description = $advices->description;
		$this->keywords = $advices->keywords;

		$advices = new Model_Advice();

		$advices->order_by('id','DESC');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.advices');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($advices->reset(false)->find_all());

		$advices = $advices->offset($offset)->limit($limit)->find_all();

		$this->content =
			View::factory('advices/list')
				->set('advices',$advices)
				->set('pager',$pager);
	}

	public function action_show() {

		$id = $this->request->param('id',0);

		$advice = new Model_Advice($id);

		if (!$advice->loaded()) {
			throw new HTTP_Exception_404('Совет не найден');
		}

		if ($advice->title) {
			$this->title = $advice->title;
		} else {
			$this->title = $advice->name;
		}

		if ($advice->description) {
			$this->description = $advice->description;
		} else {
			$this->description = $advice->brief;
		}

		$this->path = HTML::anchor('/','Главная') . ' / ' . HTML::anchor(Route::url('advices'),'Полезные советы');

		$this->content = View::factory('advices/show')->set('advice',$advice);
	}
}
