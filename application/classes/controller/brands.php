<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 12.2011
//
// Бренды, производители
//

class Controller_Brands extends Controller_Application {

	public function before() {

		parent::before();

		parent::css('brands.css');
	}

	public function action_index() {

		$brands = Model_Content::get_by_key('brands');

		if (!$brands->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		if ($brands->title) {
			$this->title = $brands->title;
		} else {
			$this->title = $brands->name;
		}

		$this->description = $brands->description;
		$this->keywords = $brands->keywords;

		$brands = $brands->contents->order_by('name');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.brands');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($brands->reset(false)->find_all());

		$brands = $brands->offset($offset)->limit($limit)->find_all();

		$this->content =
			View::factory('brand/list')
				->set('brands',$brands)
				->set('pager',$pager);
	}


	public function action_show() {

		$brands = Model_Content::get_by_key('brands');

		if (!$brands->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		$id = $this->request->param('id',0);

		$brand = $brands->contents->where('id','=',$id)->find();

		if (!$brand->loaded()) {
			throw new HTTP_Exception_404('Страница бренда не найдена');
		}

		if ($brand->title) {
			$this->title = $brand->title;
		} else {
			$this->title = $brand->name;
		}

		if ($brand->description) {
			$this->description = $brand->description;
		} else {
			$this->description = $brand->brief;
		}

		$this->path = HTML::anchor('/','Главная') . ' / ' . HTML::anchor(Route::url('brands'),'Бренды');

		$this->content = View::factory('brand/show')->set('brand',$brand);
	}


	// товары бренда
	public function action_products() {

		$brands = Model_Content::get_by_key('brands');

		if (!$brands->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		$id = $this->request->param('id',0);

		$brand = $brands->contents->where('id','=',$id)->find();

		if (!$brand->loaded()) {
			throw new HTTP_Exception_404('Бренда не найден');
		}

		$products = new Model_Product();

		// отображение списка товаров в категории
		$products->where('rest','>',0)->where('brand_id','=',$brand->id);

		// параметр отображения всех товаров без постраничной навигации
		$all_param = $this->request->param('all');

		if ($all_param=='all') {

			$products = $products->find_all();

			$pager = null;

			$url_all = null;

		} else {

			// постраничная навигация
			$pager = Kohana::$config->load('pager.products');

			$page = max(1, arr::get($_GET, 'page', 1));

			$limit = $pager['items_per_page'];
			$offset = $limit * ($page - 1);

			$pager['total_items'] = count($products->reset(false)->find_all());

			$products = $products->offset($offset)->limit($limit)->find_all();

			$url_all = $this->request->url() . '/all';
		}

		$this->path = HTML::anchor('/','Главная') . ' / ' . HTML::anchor(Route::url('brands'),'Бренды') . ' / ' . $brand->name;

		$this->content =
			View::factory('product/list')
				->set('title','Товары бренда ' . $brand->name)
				->set('products',$products)
				->set('pager',$pager)
				->set('url_all',$url_all);
	}
}
