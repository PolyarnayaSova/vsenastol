<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 12.2011
//
// Отображение контент-страниц
//

class Controller_Pages extends Controller_Application {

	public function action_index() {

		$page_key = $this->request->param('page');

		$page = Model_Content::get_by_key($page_key);

		if (!$page->loaded()) {
			$page = new Model_Content(array('alias'=>$page_key));
		}

		if (!$page->loaded()) {
			throw new HTTP_Exception_404('Страница не найдена');
		}

		// карту сайта отрабатываем отдельно
		if ($page->key=='map') {

			parent::css('map.css');

			$this->title = 'Карта сайта';

			$this->content = Content::map();

		} else {

			$this->title = $page->title;
			$this->keywords = $page->keywords;
			$this->description = $page->description;

			$this->content = $page->text;
		}
	}
}
