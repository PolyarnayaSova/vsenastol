<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 12.2011
//
// Контроллер приложения
//

class Controller_Application extends Controller_Basepage {

	protected	$path = null,
					$category_menu = null,
					$search_string = null;

	// переопределение для использования общего пути
	protected function css($css,$prefix='') {

		if (empty($prefix)) {
			$prefix = Kohana::$config->load('application.css.prefix');
		}

		return parent::css($css,$prefix);
	}


	// переопределение для использования общего пути
	protected function scripts($css,$prefix='') {

		if (empty($prefix)) {
			$prefix = Kohana::$config->load('application.js.prefix');
		}

		return parent::scripts($css,$prefix);
	}


	public function before() {

		parent::before();

		Cookie::$salt = Kohana::$config->load('application.cookie_salt');

		// устанавливаем свой обработчик ошибок, исключений
		set_exception_handler(array('Error', 'handler'));

		// подключаем стили и скрипты
		self::css(Kohana::$config->load('application.css.files'));
		self::scripts(Kohana::$config->load('application.js.files'));

		// подключаем JQuery
		parent::JQuery();
	}


	public function after() {

		$this->author = "&copy; ESV Corp. 2011";

		if ($this->auto_render) {

			$this->content =
				View::factory('application')
					->set('content',$this->content)
					->set('path',$this->path)
					->set('category_menu',$this->category_menu)
					->set('search_string',$this->search_string);
		}

		parent::after();
	}

	// страница по-умолчанию
	// домашняя страница
	public function action_index() {

		$page = Model_Content::get_by_key('home');
		if (!$page->loaded()) {
			throw new HTTP_Exception_404('Главная страница не найдена');
		}

		$this->title = $page->title;
		$this->keywords = $page->keywords;
		$this->description = $page->description;

		$this->content = Content::category_list();
	}

}
?>
