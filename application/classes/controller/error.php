<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Караван
//
// @author ESV 12.2011
//
// Контроллер Отображение ошибок
//
class Controller_Error extends Controller_Application {

	public function action_404() {

		$error_404 = Model_Content::get_by_key('error_404');

		if ($error_404->loaded()) {
			$title = $error_404->title;
			$content = $error_404->text;
		} else {
			$title = 'Страница не найдена';
			$content = 'Страница не найдена<br /><a href="/">на главную</a>';
		}

		$this->title = $title;
		$this->response->status(404);
		$this->content = View::factory('error/404')->set('content',$content);
	}
}
