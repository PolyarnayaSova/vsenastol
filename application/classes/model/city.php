<?php defined('SYSPATH') or die('No direct script access.');
//
// @project Посуда
//
// @author ESV Corp. (C) 12.2011
//
// Модель: Города
//
class Model_City {

	const NO_CITY = 10000;

	private static $city = array (
		1 => 'Екатеринбург',
		2 => 'Челябинск',
		3 => 'Арамиль',
		4 => 'Березовский',
		5 => 'В.Пышма',
		self::NO_CITY => 'другой город'
	);

	// стоимости доставки
	private static $delivery = array (
		1 => 200, // Екатеринбург
		2 => 0, // Челябинск
		3 => 300, // Арамиль
		4 => 300, // Березовский
		5 => 300, // В.Пышма
		self::NO_CITY => 0
	);

	// суммы для бесплатной доставки
	private static $gift_delivery = array (
		1 => 3000, // Екатеринбург
		2 => 200000, // Челябинск
		3 => 5000, // Арамиль
		4 => 5000, // Березовский
		5 => 5000, // В.Пышма
		self::NO_CITY => 200000
	);

	// получение списка для select-box
	public static function get_select($first_empty=false) {

		if ($first_empty) {
			$result = array(''=>'');
		} else {
			$result = array(0 => 'город');
		}

		foreach (self::$city as $k => $c) {
			$result[$k] = $c;
		}

		return $result;
	}


	// получение названия города по его id
	public static function name($id) {

		return (isset(self::$city[$id]) ? self::$city[$id] : '');
	}

	// получение стоимости доставки для города по его id
	public static function delivery($id) {

		return (isset(self::$delivery[$id]) ? self::$delivery[$id] : 0);
	}

	// определение бесплатной доставки
	public static function gift_delivery($city_id, $sum) {

		if ( ! isset(self::$gift_delivery[$city_id])) { return false; }

		if ($sum >= self::$gift_delivery[$city_id]) { return true; }

		return false;
	}
}
