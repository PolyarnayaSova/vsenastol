<?php defined('SYSPATH') or die('No direct script access.');
//
// @author ESV 12.2011
//
// Генерация частей контента
//
class Content {

	public static $footer_content = null; // текст в футере

	// используются только статические методы
   private function __construct() { }

	// заголовок
	public static function header() {

		$header = Model_Content::get_by_key('header');
		if (!$header->loaded()) {
			throw new HTTP_Exception_404('Блок информации в заголовок не найден');
		}

		return
			View::factory('application/header')
				->set('header',$header->text)
				->render();
	}

	// меню в заголовке
	public static function top_menu() {

		// пункты меню
		$menu = array (
			'brands',
			'delivery',
			'actions',
			'advices',
			'contacts'
		);

		return
			View::factory('application/top_menu')
				->set('menu',$menu)
				->render();
	}

	// блок корзины
	public static function cart_block_header() {

		$cart = Cart::get();

		return View::factory('cart/block')->set('cart',$cart);
	}

	// меню категорий
	public static function category_menu($menu=null) {

		// меню по-умолчанию - первй уровень категорий
		if (!$menu) {
			$categories = new Model_Product_Category();
			$menu = $categories->children_tree();
		}

		return View::factory('category/menu')->set('menu',$menu)->render();
	}

	// список категорий
	public static function category_list($category_id=0) {

		$category = new Model_Product_Category($category_id);
		$categories = new Model_Product_Category();
		$categories = $categories->where('parent_id','=',$category_id)->find_all();

		return
			View::factory('category/list')
				->set('category',$category)
				->set('categories',$categories);
	}

	// список товаров
	public static function product_list(Model_Product_Category $category,$products,$pager,$url_all=null) {

		if ( ! empty($category) && $category->loaded()) {
			if ($category->parent_id==0) {
				$url_add = array('category0'=>Text::symbols($category->name),'c0_id'=>$category->id);
			} else {
				$parent = new Model_Product_Category($category->parent_id);
				if (!$parent->loaded()) {
					throw new HTTP_Exception_404('Невозможно определить родительскую категорию для вывода списка товаров');
				}
				$url_add = array('category0'=>Text::symbols($parent->name),'c0_id'=>$parent->id,
									  'category1'=>Text::symbols($category->name),'c1_id'=>$category->id);
			}
		} else {
			$url_add = null;
		}

		return
			View::factory('product/list')
				->set('products',$products)
				->set('url_add',$url_add)
				->set('pager',$pager)
				->set('url_all',$url_all);
	}

	// анонс новостей
	public static function news_annonce() {

		$news = new Model_News();
		$news = $news->order_by('date','DESC')->limit(4)->find_all();

		return View::factory('news/annonce')->set('news',$news)->render();
	}

	// меню в подвале
	public static function footer_menu() {

		$footer_menu = Model_Content::get_by_key('footer_menu');
		if (!$footer_menu->loaded()) {
			throw new HTTP_Exception_404('Блок меню в футере не найден');
		}
		$footer_menu = $footer_menu->contents->find_all();

		return View::factory('application/footer_menu')->set('menu',$footer_menu)->render();
	}

	// подвал
	public static function footer() {

		if (empty(self::$footer_content)) {
			$footer = Model_Content::get_by_key('footer');
			if (!$footer->loaded()) {
				throw new HTTP_Exception_404('Блок информации в футере не найден');
			}
			$footer_content = $footer->text;
		} else {
			$footer_content = self::$footer_content;
		}


		$contacts_footer = Model_Content::get_by_key('contacts_footer');
		if (!$contacts_footer->loaded()) {
			throw new HTTP_Exception_404('Блок контактов в футере не найден');
		}

		$copyright = Model_Content::get_by_key('copyright');
		if (!$copyright->loaded()) {
			throw new HTTP_Exception_404('Блок copyright в футере не найден');
		}

		$counters = Model_Content::get_by_key('counters');
		if (!$counters->loaded()) {
			throw new HTTP_Exception_404('Блок счетчиков в футере не найден');
		}

		return
			View::factory('application/footer')
				->set('footer',$footer_content)
				->set('contacts_footer',$contacts_footer->text)
				->set('copyright',$copyright->text)
				->set('counters',$counters->text)
				->render();
	}


	// карта сайта
	public static function map() {

		$map = array();

		$top_menu = Model_Content::get_by_key('top_menu');
		if (!$top_menu->loaded()) {
			throw new HTTP_Exception_404('Блок меню заголовка не найден');
		}

		$footer_menu = Model_Content::get_by_key('footer_menu');
		if (!$footer_menu->loaded()) {
			throw new HTTP_Exception_404('Блок меню нижнего колонтитула не найден');
		}

		$news = Model_Content::get_by_key('news');
		if (!$news->loaded()) {
			throw new HTTP_Exception_404('Блок новостей не найден');
		}

		$map = array_merge($map,$top_menu->children_tree());
		$map = array_merge($map,array(array('node'=>$news)));
		$map = array_merge($map,$footer_menu->children_tree());

		return View::factory('application/map')->set('map',$map)->render();
	}
}

?>
