<?php
defined('SYSPATH') or die('No direct script access.');
/**
 * Created by JetBrains PhpStorm.
 * User: gibson
 * Date: 21.03.11
 * Time: 16:04
 */

class Mailer {

    private $mailer;
    private $message;

    public $LE = "\n";
    
    public function __construct($config = null) {
        $this->mailer = Pigeon::mailer($config);
        $this->message = Swift_Message::newInstance();
    }

    /**
     * Set the body content of this entity as a string.
     * @param string $body
     * @param string $contentType optional
     */
    public function setBody($body, $contentType = null) {
        $this->message->setBody($body, $contentType);
    }

    /**
     * @path_to_file $file attached to msg
     * @return void
     */
    public function attach($file) {
        $this->message->attach(Swift_Attachment::fromPath($file));
    }

    /**
     * Set the From address of this message.
     *
     * It is permissible for multiple From addresses to be set using an array.
     *
     * If multiple From addresses are used, you SHOULD set the Sender address and
     * according to RFC 2822, MUST set the sender address.
     *
     * An array can be used if display names are to be provided: i.e.
     * array('email@address.com' => 'Real Name').
     *
     * If the second parameter is provided and the first is a string, then $name
     * is associated with the address.
     *
     * @param mixed $addresses
     * @param string $name optional
     */
    public function setFrom($addresses, $name = null) {
        $this->message->setFrom($addresses, $name = null);
    }

    public function setSubject($subject) {
        $this->message->setSubject($subject);
    }

    public function setTo($to) {
        $this->message->setTo($to);
    }

    public function send($batch = false){
        if($batch == false){
            return $this->mailer->send($this->message);
        }else{
            return $this->mailer->batchSend($this->message);
        }
    }
}