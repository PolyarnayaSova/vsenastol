<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Основной шаблон
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="wrap">
	<div class="bg">
   	<div class="body">

			<?php print Content::header(); ?>

         <div class="sidebar">

				<div class="search">
					<?php
						// форма поиска

						// определяем, существует ли активный запрос
						if (empty($search_string)) {
							$search_string='поиск';
							$search_class = null;
						} else {
							$search_class = 'active';
						}

						print Form::open(Route::url('search'),array('method'=>'POST','id'=>'search'));
						print Form::input('search',$search_string,array('id'=>'search','size'=>200,'class'=>$search_class));
						print HTML::anchor(Route::url('advancedsearch'),'расширенный поиск',array('class'=>'advanced'));
						print HTML::anchor('#','',array('class'=>'submit'));
						print Form::close();
					?>
				</div>

				<?php print Content::category_menu($category_menu); ?>

				<?php print Content::news_annonce(); ?>

         </div>

         <div class="wrap-content clearfix">
				<?php if ($path): ?>
				<div class="path">
					<?php print $path; ?>
				</div>
				<?php endif; ?>
				<div class="content">
					<?php print $content; ?>
				</div>
         </div>

			<?php print Content::footer(); ?>

      </div>
   </div>
</div>

<script type="text/javascript" language="javascript">
<!--
$(document).ready(function(){

	$('DIV.search INPUT#search')
		.on('focusin',function(){
			var val = $(this).val();

				if (val=='поиск') {
					$(this).val('');
				}

				$(this).addClass('active');
		})
		.on('focusout',function(){
			var val = $(this).val();

				if (val=='') {
					$(this).val('поиск');
					$(this).removeClass('active');
				}
		});

	$('DIV.search').on('click','A.submit',function(){

		var val = $('DIV.search INPUT#search').val();

		if (val=='поиск') {
			$('DIV.search INPUT#search').val('');
		}

		$('FORM#search').submit();

	});

});
-->
</script>
