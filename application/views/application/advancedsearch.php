<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Форма расширенного поиска
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<?php if (isset($title) && $title): ?>
<h1 class="title"><?php print $title; ?></h1>
<?php endif; ?>

<div class="advancedsearch">

	<?php
		print Form::open(Route::url('advancedsearch'),array('method'=>'GET'));
		print Form::input('search','',array('class'=>'search'));
		print Form::submit('asearch','поиск');
	?>
<table>
	<tr>
		<th>в брендах:</th>
		<th>в категориях товаров:</th>
	</tr>
	<tr>
		<td>
			<ul>
			<?php	foreach ($brands as $b): ?>
			<li><label>
				<?php
					print Form::checkbox("brand_{$b->id}",$b->id,in_array($b->id,$brands_checked));
					print $b->name;
				?>
			</label></li>
			<?php endforeach; ?>
			</ul>
		</td>

		<td>
			<ul>
			<?php
				foreach ($categories as $c0):
					$sub = $c0->categories->where('visible','!=',0)->order_by('position')->find_all();
					if (count($sub)>0):
			?>
					<li class="list-categories">
						<span class="title"><?php print $c0->name; ?></span>
						<ul>
							<?php
								foreach ($sub as $c1):
									$checked = in_array($c1->id,$categories_checked);
									$class = $checked ? ' checked' : '';
							?>
							<li class="check-box<?php print $class; ?>">
								<label>
									<?php

										print Form::checkbox("category_{$c1->id}",$c1->id,$checked);
										print $c1->name;
									?>
								</label>
							</li>
							<?php endforeach; ?>
						</ul>
					</li>
					<?php else: ?>
					<li>
						<label>
							<?php
								print Form::checkbox("category_{$c0->id}",$c0->id,in_array($c0->id,$categories_checked));
								print $c0->name;
							?>
						</label>
					</li>
					<?php endif; ?>

			<?php endforeach; ?>
			</ul>
		</td>
	</tr>
</table>
	<?php print Form::close(); ?>
</div>

<script type="text/javascript">
	<!--
		$('DIV.advancedsearch LI.list-categories UL LI.check-box').on('change','INPUT[type="checkbox"]',function(){
			$(this).parents('LI.check-box').toggleClass('checked',$(this).attr('checked'));
		});

		$('DIV.advancedsearch LI.list-categories').on('click','SPAN.title',function(){
			$(this).parent('LI.list-categories').toggleClass('opened');
		});
	//-->
</script>
