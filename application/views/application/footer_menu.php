<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Меню подвала
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<ul class="footer_menu">
	<li><?php print HTML::anchor('/','Главная'); ?></li>
	<?php
		foreach ($menu as $item):

		if (strpos($item->alias,'http://')===0) {
			$url = $item->alias;
			$target = '_blank';
		} else {
			$url = Route::url('page',array('page'=>$item->alias));
			$target = null;
		}
	?>
	<li><?php print HTML::anchor($url,$item->name,array('target'=>$target)); ?></li>
	<?php
		endforeach;
	?>
</ul>
