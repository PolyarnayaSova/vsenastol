<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Караван
 *
 * Шаблон заголовка
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="header">

	<a class="logo" href="/"></a>

	<div class="contacts_header"><?php print $header; ?></div>

	<div class="cart">
	<?php
		// состояние корзины
		print Content::cart_block_header();
	?>
	</div>
	<div class="cart-stub"></div>

	<?php print Content::top_menu(); ?>

</div>
