<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон подвала
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<?php if (isset($footer) && ! empty($footer)): ?>
<div class="footer_info"><?php print $footer; ?></div>
<?php endif; ?>

<div class="footer">
	<?php print Content::footer_menu(); ?>
	<div class="contacts_footer"><?php print $contacts_footer; ?></div>
	<div class="cap"></div>
	<div class="copyright"><?php print $copyright; ?></div>
	<div class="counters"><?php print $counters; ?></div>
</div>
