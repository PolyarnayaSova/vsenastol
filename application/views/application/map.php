<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Карта сайта
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

// список брендов
function block_brands($brands_block) {

	if ($brands_block->loaded()) {
		$brands = $brands_block->contents->order_by('name')->find_all();
		if (count($brands)>0) {
			print HTML::anchor(Route::url('brands'),'Бренды');
			print "<ul>\n";
			foreach ($brands as $brand) {
				print '<li>' . HTML::anchor(Route::url('brands',array('action'=>'show','brand'=>Text::symbols($brand->name),'id'=>$brand->id)),$brand->name) . "</li>\n";
			}
			print "</ul>\n";
		}
	}
}

// дерево категорий товаров
function block_categories($tree,$parent=null) {

	if (count($tree)>0) {

		print "\n<ul>\n";

		foreach ($tree as $node) {

			$category = $node['node'];

			if ($parent && $parent->loaded()) {
				$url_param = array('category0'=>Text::symbols($parent->name),'c0_id'=>$parent->id,
									   'category1'=>Text::symbols($category->name),'c1_id'=>$category->id);
			} else {
				$url_param = array('category0'=>Text::symbols($category->name),'c0_id'=>$category->id);
			}

			print '<li>' . HTML::anchor(Route::url('category',$url_param),$category->name);

			if (isset($node['tree'])) {
				block_categories($node['tree'],$category);
			}

			print "</li>\n";
		}

		print "</ul>\n";
	}
}

function print_block($map) {

	if (count($map)>0) {

		print "<ul>\n";

		foreach ($map as $node) {

			print "<li>\n";

			if ($node['node']->key=='brands') {
				block_brands($node['node']);
			} else {

				if (strpos($node['node']->alias,'http://')===FALSE) {
					if ($node['node']->key) {
						print HTML::anchor(Route::url('page',array('page'=>$node['node']->key)),$node['node']->name);
					} else {
						print HTML::anchor(Route::url('page',array('page'=>$node['node']->alias)),$node['node']->name);
					}
				}
			}

			print "</li>\n";
		}

		// дерево категорий
		print "<li>Товары\n";

		$category_tree = new Model_Product_Category();

		print block_categories($category_tree->tree());

		print "</li>\n";

		print "</ul>\n";
	}
}
?>

<h1 class="title">Карта сайта</h1>
<ul>
	<?php print HTML::anchor('/','Главная'); ?>
	<?php print print_block($map); ?>
</ul>
