<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон меню заголовка
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="menu-wrap">
	<ul class="menu">
		<?php foreach ($menu as $item): ?>
		<li><?php print HTML::anchor(Route::url('page',array('page'=>$item)),'',array('class'=>$item)); ?></li>
		<?php endforeach; ?>
	</ul>
</div>
