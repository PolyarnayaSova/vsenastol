<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон списка категорий
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

	if (isset($category) && $category->loaded()) {
		$c0_id = $category->id;
		$c0_name = Text::symbols($category->name);
	} else {
		$c0_id = false;
		$c0_name = false;
	}
?>

<?php
	foreach ($categories as $c):

		$name = Text::symbols($c->name);

		if ($c0_id) {
			$url = Route::url('category',array('category0'=>$c0_name,'c0_id'=>$c0_id,'category1'=>$name,'c1_id'=>$c->id));
		} else {
			$url = Route::url('category',array('category0'=>$name,'c0_id'=>$c->id));
		}

		$image = Model_Product_Category_Image::element($c->id,1);
		$image_src = $image->path('list');
?>
	<div class="lot">
		<div class="image-wrap"><div class="image-wrap-row">
			<div class="image">
				<?php print HTML::anchor($url,HTML::image($image_src,array('alt'=>$c->name))); ?>
			</div>
		</div></div>
		<div class="description">
			<?php print HTML::anchor($url,$c->name,array('class'=>'title short-title')); ?>
			<?php if ($c->brief): ?>
			<p class="brief">
				<?php print nl2br($c->brief); ?>
			</p>
			<?php endif; ?>
		</div>
	</div>
<?php
	endforeach;
?>

