<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон меню категорий
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<ul class="menu">
	<span class="title-menu">Категории</span>

	<?php
		foreach ($menu as $item):
			$name = Text::symbols($item['node']->name);
			$url = Route::url('category',array('category0'=>$name,'c0_id'=>$item['node']->id));
			$class = isset($item['tree']) ? 'selected' : null;

			if (isset($item['tree']) && count($item['tree'])>0):
	?>
	<li>
		<?php print HTML::anchor($url,$item['node']->name,array('class'=>$class)); ?>
		<ul>
			<?php
				foreach ($item['tree'] as $sub_item):
				$name1 = Text::symbols($sub_item['node']->name);
				$url1 = Route::url('category',array('category0'=>$name,'c0_id'=>$item['node']->id,
																'category1'=>$name1,'c1_id'=>$sub_item['node']->id));
				$class1 = isset($sub_item['tree']) ? 'selected' : null;
				$count = $sub_item['node']->products->where('rest','>',0)->count_all();
			?>

			<?php if ($count>0): // отображаем только категории с товарами ?>
			<li><?php print HTML::anchor($url1,$sub_item['node']->name,array('class'=>$class1)); ?><span class="count-prod">(<?php print $count; ?>)</span></li>
			<?php endif; ?>

			<?php endforeach; ?>
		</ul>
	</li>

	<?php else: ?>

	<li><?php print HTML::anchor($url,$item['node']->name,array('class'=>$class)); ?></li>

	<?php endif; ?>

	<?php endforeach; ?>
</ul>
