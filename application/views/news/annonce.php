<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон анонса новостей
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<ul class="news">
	<?php print HTML::anchor(Route::url('news'),'Новости',array('class'=>'title-news')); ?>
	<?php foreach ($news as $n): ?>
	<li>
		<?php print HTML::anchor(Route::url('news',array('action'=>'show','id'=>$n->id)),$n->name); ?>
		<span class="date"><?php print $n->string_date(); ?></span>
	</li>
	<?php endforeach; ?>
</ul>
