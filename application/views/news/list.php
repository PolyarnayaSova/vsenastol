<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон списка новостей
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="news">

	<h1 class="title">Новости</h1>

	<?php
		foreach ($news as $n):
	?>
	<div class="news-item">
		<?php print HTML::anchor(Route::url('news',array('action'=>'show','id'=>$n->id)),$n->name,array('class'=>'title')); ?>
		<span class="date"><?php print $n->string_date(); ?></span>
		<p class="brief"><?php print  nl2br($n->brief); ?></p>
	</div>
	<?php
		endforeach;
	?>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager-wrap">
		<div class="pager">
			<?php print $pager; ?>
		</div>
	</div>
	<?php endif; ?>

</div>
