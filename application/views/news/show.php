<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон страница новости
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="news">

	<h1 class="title"><?php print $news->name; ?></h1>

	<span class="date"><?php print $news->string_date(); ?></span>

	<div class="text">
		<?php print $news->text; ?>
	</div>

</div>
