<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Формирование бланка заказа
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

	//if (isset($delivery) && is_numeric($delivery) && $delivery>0) {
	//	$delivery = str_replace(' ','&nbsp;',(number_format($delivery,2,',',' ') . ' руб.'));
	//} else {
	//	$delivery = 'бесплатно';
	//}

	if ( ! isset($delivery)) {
		$delivery = 0;
	}

	if ( ! isset($discount)) {
		$discount = 0;
	}

?>
<table border="0" cellspacing="0" cellpadding="5" class="order-products-blank">
	<tr>
		<th>Наименование</th>
		<th>Цена</th>
		<th>Кол-во</th>
		<th>Сумма</th>
	</tr>

	<?php
		$price_all = 0;
		foreach ($products as $p):
			$price_sum = $cart[$p->id]*$p->get_price();
			$price_all += $price_sum;
	?>
	<tr>
		<td align="left" class="name"><?php print ("{$p->name}<br />артикул: {$p->article}<br />" . 'УК' . sprintf("%08d",$p->code)); ?></td>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($p->get_price(),2,',',' ') . ' руб.')); ?></td>
		<td align="center" nowrap="nowrap" class="count"><?php print $cart[$p->id]; ?></td>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($price_sum,2,',',' ') . ' руб.')); ?></td>
	</tr>
	<?php endforeach; ?>

	<tr>
		<td align="right" class="name-title">Итого</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($price_all,2,',',' ') . ' руб.')); ?></td>
	</tr>

	<tr>
		<td align="right" class="name-title">Доставка</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>

		<?php if ($delivery == 0): ?>
			<td align="center" nowrap="nowrap" class="price">договорная</td>
		<?php elseif (Model_City::gift_delivery($city_id, $price_all)):
					$delivery = 0;
		?>
			<td align="center" nowrap="nowrap" class="price">бесплатно</td>
		<?php else: ?>
			<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($delivery,2,',',' ') . ' руб.')); ?></td>
		<?php endif; ?>

	</tr>

	<tr>
		<td align="right" class="name-title">Скидка</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="right" nowrap="nowrap" class="price"><?php print number_format($discount,0); ?>%</td>
	</tr>

	<tr>
		<td align="right" nowrap="nowrap" class="name-title">Итого с учетом скидки и доставки</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<?php $price_all_discount = $price_all- ($price_all / 100 * $discount) + $delivery; ?>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($price_all_discount,2,',',' ') . ' руб.')); ?></td>
	</tr>

</table>
