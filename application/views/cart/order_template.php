<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Формирование записи заказа
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

	$host = 'http://' . $_SERVER['SERVER_NAME'] . '/';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Заказ в Интернет-Магазине</title>

<?php print View::factory('cart/mail_styles'); ?>

</head>

<body>

<div class="body">
	<h1>Поступил Заказ в Интернет-Магазин &quot;Посуда&quot;</h1>
	<p><strong>Данные клиента:</strong></p>
	<p><strong>Ф.И.О.</strong>: <?php print $fio; ?></p>
	<p><strong>e-mail</strong>: <?php print $email; ?></p>
	<p><strong>Телефон</strong>: <?php print $phone; ?></p>
	<p><strong>Город доставки</strong>: <?php print $city; ?></p>
	<p><strong>Дополнительная информация</strong>:<br /> <?php print nl2br($text); ?></p>
	<p></p>
	<h2>Заказ:</h2>
	<?php print $table_products; ?>
</div>

</body>
</html>
