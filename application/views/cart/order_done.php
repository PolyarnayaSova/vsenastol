<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Страница завершения заказа
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="order">
	<h1 class="title"><?php print $title; ?></h1>
	<p><?php print $comment; ?></p>
</div>
