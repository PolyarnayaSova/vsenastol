<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Письмо для отправки клиенту
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

	$host = 'http://' . $_SERVER['SERVER_NAME'] . '/';
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Заказ в Интернет-Магазине</title>

<?php print View::factory('cart/mail_styles'); ?>

</head>

<body>

<div class="order-body">
	<h1>Заказ № <?php print $order_num; ?> в Интернет-Магазине &quot;Все на стол&quot;</h1>
	<h2>Увжаемый(ая), <?php print $fio; ?></h2>
	Вы сделали заказ в <a href="<?php print $host; ?>" target="_blank">Интернет-Магазине &quot;Все на стол&quot;</a>
	<p><strong>Ваши данные:</strong></p>
	<p><strong>Ф.И.О.</strong>: <?php print $fio; ?></p>
	<p><strong>Телефон</strong>: <?php print $phone; ?></p>
	<p><strong>Адрес доставки</strong>: <?php print $addr; ?></p>
	<p><strong>Город доставки</strong>: <?php print $city; ?></p>
	<p><strong>e-mail</strong>: <?php print $email; ?></p>
	<?php if ($self): ?>
	<p><strong>Самовывоз</strong></p>
	<?php endif; ?>
	<?php if ($text): ?>
	<p><strong>Дополнительная информация</strong>:<br /> <?php print nl2br($text); ?></p>
	<?php endif;?>
	<p></p>
	<h2>Заказ:</h2>
	<?php print $table_products; ?>
</div>

</body>
</html>
