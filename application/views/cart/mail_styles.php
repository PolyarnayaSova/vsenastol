<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Стили для отправки писем
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<style type="text/css">
	div.order-body {
		font-family:Arial, Helvetica, sans-serif;
		font-size:13pt;
		color:black;
		background:white;
	}

	div.body h1 {
		font-size:18pt;
	}

	div.body h2 {
		font-size:16pt;
	}

	div.body h3 {
		font-size:14pt;
	}

	div.body p {
		font-size:13pt;
	}

	div.body strong {
		font-size:13pt;
		font-weight:bold;
	}

	div.body table {
		border-collapse: collapse;
		border-spacing: 0;

		font-size:13pt;

		border:1px solid black;
	}

	div.body table th {
		border:1px solid black;
		background:lightgrey;
	}

	div.body table td {
		border:1px solid black;
	}
</style>
