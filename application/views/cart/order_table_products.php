<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Формирование списка товаров
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
	if (!isset($with_host)) {
		$with_host = true;
	}

	if (!isset($with_photos)) {
		$with_photos = true;
	}

	$last_class = $with_photos ? '' : ' last';

	if ($with_host) {
		$host = 'http://' . $_SERVER['SERVER_NAME'];
	} else {
		$host = '';
	}

	$delivery = Model_City::delivery($city_id);

?>
<table border="1" cellspacing="0" cellpadding="5" class="order-products">
	<tr>
		<th>Наименование</th>
		<th>Кол-во</th>
		<th>Цена</th>
		<?php if ($with_photos): ?>
		<th>Фото</th>
		<?php endif; ?>
	</tr>

	<?php
		$count_all = 0;
		$price_all = 0;
		foreach ($products as $p):
			$url = $host . Route::url('product',array('product'=>Text::symbols($p->name),'p_id'=>$p->id));
			$image = $p->get_first_image();
			$image_src = HTML::image((empty($host) ? '' : "$host/") . $image->path('list'));
			$count_all += $cart[$p->id];
			$price_all += $cart[$p->id]*$p->get_price();
	?>
	<tr>
		<td width="400" align="left" nowrap="nowrap" class="name">
			<?php
				print HTML::anchor($url,$p->name,array('target'=>'_blank')) . '<br />' .
						'артикул: ' . $p->article . '<br />' .
						'УК' . sprintf("%08d",$p->code);
			?>
		</td>
		<td width="50" align="center" nowrap="nowrap" class="count"><?php print $cart[$p->id]; ?></td>
		<td width="120" align="right" nowrap="nowrap" class="price<?php print $last_class; ?>"><?php print str_replace(' ','&nbsp;',(number_format($p->get_price(),2,',',' ') . ' руб.')); ?></td>

		<?php if ($with_photos): ?>
		<td width="100" align="center" valign="middle" class="photo last"><?php print $image_src; ?></td>
		<?php endif; ?>
	</tr>
	<?php endforeach; ?>

	<tr>
		<td align="right" nowrap="nowrap" class="sum"><strong>Сумма:</strong></td>
		<td align="center" nowrap="nowrap" class="sum-count"><strong><?php print $count_all; ?></strong></td>
		<td align="right" nowrap="nowrap" class="sum-price<?php print $last_class; ?>"><strong><?php print str_replace(' ','&nbsp;',(number_format($price_all,2,',',' ') . ' руб.')); ?></strong></td>

		<?php if ($with_photos): ?>
		<td class="last">&nbsp;</td>
		<?php endif; ?>
	</tr>

	<tr>
		<td align="right" nowrap="nowrap" class="sum"><strong>Доставка:</strong></td>
		<td>&nbsp;</td>

		<?php if ($delivery == 0): ?>
			<td align="center" nowrap="nowrap" class="price<?php print $last_class; ?>">договорная</td>
		<?php elseif (Model_City::gift_delivery($city_id, $price_all)):
					$delivery = 0;
		?>
			<td align="center" nowrap="nowrap" class="price<?php print $last_class; ?>">бесплатно</td>
		<?php else: ?>
			<td align="right" nowrap="nowrap" class="price<?php print $last_class; ?>"><?php print str_replace(' ','&nbsp;',(number_format($delivery,2,',',' ') . ' руб.')); ?></td>
		<?php endif; ?>

		<?php if ($with_photos): ?>
		<td class="last">&nbsp;</td>
		<?php endif; ?>
	</tr>

	<tr>
		<td align="right" nowrap="nowrap" class="sum"><strong>Итого:</strong></td>
		<td>&nbsp;</td>
		<td align="right" nowrap="nowrap" class="sum-price<?php print $last_class; ?>"><strong><?php print str_replace(' ','&nbsp;',(number_format($price_all + $delivery,2,',',' ') . ' руб.')); ?></strong></td>

		<?php if ($with_photos): ?>
		<td class="last">&nbsp;</td>
		<?php endif; ?>
	</tr>

</table>
