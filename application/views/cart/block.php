<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Блок состояния корзины в заголовке
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<noindex>
<p class="cart-logo">Корзина</p>

<?php if ($cart['count']>0): ?>
	<div class="info">
		<p>сумма: <span class="price"><?php print number_format($cart['price'],2,',',' '); ?> руб.</span></p>
		<p>товаров: <span class="count"><?php print $cart['count']; ?></span></p>
	</div>
	<?php print HTML::anchor(Route::url('cart',array('action'=>'order')),'Оформить заказ',array('class'=>'order','rel'=>'nofollow')); ?>
<?php else: ?>
	<div class="info">
		<p class="empty">корзина пуста</p>
	</div>
<?php endif; ?>

</noindex>
