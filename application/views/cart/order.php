<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Страница заказа
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

	$fio = isset($fio) ? $fio : '';
	$email = isset($email) ? $email : '';
	$phone = isset($phone) ? $phone : '';
	$addr = isset($addr) ? $addr : '';
	$city = isset($city) ? $city : 0;
	$text = isset($text) ? $text : '';
	$self = isset($self) ? $self : 0;

?>
<?php if (!$is_ajax): ?>
<?php print Form::open(Route::url('cart',array('action'=>'send')),array('method'=>'POST','id'=>'send_order')); ?>
<div class="order">
<?php endif; ?>

	<h1 class="title">Корзина</h1>
	<?php if (empty($products) || count($products)==0): ?>
	корзина пуста
	<script type="text/javascript" language="javascript">
		<!--
		if (window.$) {
			$(document).ready(function(){
				$('DIV.order-form').hide();
			});
		}
		-->
	</script>
	<?php else: ?>
		<?php
			$sum_count = 0;
			$sum_price = 0;

			foreach ($products as $p):

				$url = Route::url('product',array('product'=>Text::symbols($p->name),'p_id'=>$p->id));

				$sum_count += $cart['products'][$p->id];
				$sum_price += $p->get_price()*$cart['products'][$p->id];

				$image = $p->get_first_image();
				$image_src = $image->path('list');
		?>
		<div class="lot">
			<div class="image-wrap with-right-margin"><div class="image-wrap-row">
				<div class="image">
					<?php print HTML::anchor($url,HTML::image($image_src,array('alt'=>$p->name))); ?>
				</div>
			</div></div>
			<div class="name">
				<span class="caption"></span>
				<?php print HTML::anchor($url,$p->name); ?>
			</div>
			<div class="count">
				<span class="caption">Кол-во</span>
				<?= ($step==1)?Form::input('product_'.$p->id,$cart['products'][$p->id],array('id'=>$p->id,'size'=>5)):$cart['products'][$p->id]; ?>
			</div>
			<div class="article"><span class="caption">Артикул</span><?php print $p->article; ?></div>
			<div class="price"><span class="caption" style="text-align:center;">Цена</span><span class="price"><?php print number_format($p->get_price(),2,',',' ') . ' руб.'; ?></span></div>
			<? if($step==1){ ?>
				<div class="del"><?php print HTML::anchor(Route::url('cart',array('action'=>'delfromorder','id'=>$p->id)),'',array('class'=>'del','title'=>'Удалить товар из корзины')); ?></div>
			<? } ?>
		</div>
		<?php
			endforeach;

			$delivery_price=0;
			if($city==1&&$sum_price<3000){
				$delivery_price=200;
			}
			else if(($city==3||$city==4||$city==5)&&$sum_price<5000){
				$delivery_price=300;
			}
			else if($city==2||$city==10000){
				$delivery_price=-1;
			}
			
			$delivery_price_text="Договорная";
			if($delivery_price>=0){
				$delivery_price_text=$delivery_price.",00 руб";
			}
			
		?>

		<div class="lot" id='delivery_string' <? if($step==1){ ?>style='display:none;'<? } ?>>
			<div class="sum" style='margin-left: 120px;width: 300px;'>Доставка:</div>
			<div class="sum" style='width: 40px;'>&nbsp;</div>
			<div class="sum sum-delivery" style='text-align: right;width: 130px;margin-left: 120px;'><?= $delivery_price_text; ?></div>
		</div>
		
		<div class="lot">
			<div class="sum sum-caption">Итого:</div>
			<div class="sum sum-count"><?php print $sum_count; ?></div>
			<div class="sum sum-price"><span data-price='<?= intval($sum_price); ?>'><?= intval($sum_price+$delivery_price); ?></span>,00 руб</div>
		</div>

	<?php	endif; ?>

<?php if (!$is_ajax): ?>
</div>

<div class="order-form">
	<?php if (isset($warning_message) && !empty($warning_message)): ?>
	<p class="warning"><?php print $warning_message; ?></p>
	<?php endif; ?>
	<div <? if($step==2) echo "style='display:none';"; ?>>
		<div class="field"><span class="prefix">*</span><?php print Form::input('fio',$fio,array('size'=>250,'title'=>'Ф.И.О.')); ?></div>
		<div class="field"><span class="prefix">*</span><?php print Form::input('phone',$phone,array('size'=>250,'title'=>'Номер телефона')); ?></div>
		<div class="field"><span class="prefix">*</span><?php print Form::input('addr',$addr,array('size'=>250,'title'=>'Адрес доставки')); ?></div>
		<div class="field"><span class="prefix">*</span><?php print Form::select('city',$cities,$city,array('title'=>'Город доставки')); ?></div>
		<div class="field"><span class="prefix"> </span><?php print Form::input('email',$email,array('size'=>250,'title'=>'e-mail')); ?></div>
		<div class="field"><span class="prefix"> </span><?php print Form::textarea('text',$text,array('cols'=>200,'rows'=>5,'title'=>'Дополнительная информация')); ?></div>
		<!--<div class="field"><span class="prefix"> </span><?php print Form::checkbox('self',1,$self==1); ?>самовывоз</div>-->
		<div class="field"><span class="prefix"> </span><?php print Form::checkbox('acquainted',1,($step==2)); ?>с <?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'terms')),'условиями доставки'); ?> ознакомлен(а)</div>
		<input type="hidden" name="step" value="<?= $step; ?>">
	</div>
	<div <? if($step==1) echo "style='display:none';"; ?>>
		<p><b>Ф.И.О.</b>: <?= $fio; ?></p>
		<p><b>Номер телефона</b>: <?= $phone; ?></p>
		<p><b>Адрес доставки</b>: <?= $addr; ?></p>
		<p><b>Город доставки</b>: <?= $cities[$city]; ?></p>
		<p><b>e-mail</b>: <?= $email; ?></p>
		<p><b>Дополнительная информация</b>: <?= $text; ?></p>
		<?/*<p><b>Cамовывоз</b>: <?= ($self)?"Да":"Нет"; ?></p>*/?>
		
	</div>
	<div class="field" style="margin-top:10px;">
		<? if($step==1){ ?>
			<span class="prefix"> </span><?php print Form::submit('order','заказать'); ?>
		<? }else{ ?>
			<a style='display: block;float: left;font-size: 20px;color: #ff853c;padding: 2px 10px;text-decoration:none;max-width: 500px;background: #fff6ca;border: 2px solid #ffbd0c;border-radius: 15px;' href="/cart/order">Вернуться к оформлению заказа</a>
			<span class="prefix"> </span><?php print Form::submit('order','Отправить заказ'); ?>
		<? } ?>
	</div>

	<div class="help-form">поля, обозначенные <span class="star">*</span> обязательны для заполнения</div>
</div>

<?php print Form::close(); ?>
<?php endif; ?>
