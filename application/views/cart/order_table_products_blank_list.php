<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Формирование бланка заказа для списков заказов
 *
 * @author ESV Corp. (С) 01.2013
 *
 */

?>
<table border="0" cellspacing="0" cellpadding="2" class="order-products-blank">

	<tr>
		<th>наименование</th>
		<th>ук</th>
		<th>артикул</th>
		<th>цена</th>
		<th>кол-во</th>
		<th>сумма</th>
	</tr>

	<?php
		$count_all = 0;
		$price_all = 0;
		foreach ($products as $p):
			$price_sum = $cart[$p->id] * $p->get_price();
			$count_all += $cart[$p->id];
			$price_all += $price_sum;
	?>
	<tr>
		<td align="left" class="name"><?php print $p->name; ?></td>
		<td align="center" class="name"><?php print ('УК' . sprintf("%08d",$p->code)); ?></td>
		<td align="center" class="name"><?php print $p->article; ?></td>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($p->get_price(),2,',',' '))); ?></td>
		<td align="center" nowrap="nowrap" class="count"><?php print $cart[$p->id]; ?></td>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($price_sum,2,',',' '))); ?></td>
	</tr>
	<?php endforeach; ?>

	<tr>
		<td align="right" class="name-title" colspan="4">Итого</td>
		<td align="center" nowrap="nowrap" class="count"><?php print $count_all; ?></td>
		<td align="right" nowrap="nowrap" class="price"><?php print str_replace(' ','&nbsp;',(number_format($price_all,2,',',' '))); ?></td>
	</tr>

</table>
