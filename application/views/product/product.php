<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон Информация товара
 *
 * @author ESV Corp. (С) 12.2011
 *
 */

	const MAX_PHOTOS = 3; // максимальное количество мини-фото

	$image = $product->get_first_image();
	$image_src = $image->path('info');

	$images = $product->get_images();

	$brand = $product->brand;
	$material = $product->material;

?>
<div class="product">
	<h1 class="title"><?php print $product->name; ?></h1>
	<div class="info">
		<div class="info-image-wrap"><div class="info-image-wrap-row">
			<div class="info-image">
				<?php print HTML::image($image_src,array('alt'=>$product->name,'id'=>'main_image')); ?>
			</div>
		</div></div>
		<div class="description">
			<?if ((count($images)>1)):?>
			<div class="photos">
				<?php
					if (count($images)>1):
					$count = MAX_PHOTOS;
					foreach ($images as $img):
						$img_src = $img->path('list');
						$img_full = $img->path('info');
				?>
					<div class="image-wrap with-right-margin"><div class="image-wrap-row">
						<div class="image">
							<?php print HTML::anchor($img_full,HTML::image($img_src),array('id'=>'small_image')); ?>
						</div>
					</div></div>
				<?php
						if (--$count==0) break;
					endforeach;
					endif;
				?>
			</div>
			<?endif;?>
			<span class="price">Цена: <?php print number_format($product->get_price(),2,',',' '); ?> руб.</span>
			<?if ($product->rest == 0):?>
			<b>Извините, на данный момент товара нет в наличии</b>
				<br/>
				<br/>	<br/>
			<?endif;?>
			<ul class="parameters">
				<?php if ($brand->loaded()): ?>
				<li>Производитель: <span class="value"><?php print $brand->name; ?></span></li>
				<?php endif; ?>
				<li>Артикул: <span class="value"><?php print $product->article; ?></span></li>
				<?php if ($material->loaded()): ?>
				<li>Материал: <span class="value"><?php print $material->name; ?></span></li>
				<?php endif; ?>
				<li>Вес: <span class="value"><?php print $product->weight; ?></span></li>
				<li>Штрихкод: <span class="value"><?php print $product->barcode; ?></span></li>
				<li>Единицы измерения: <span class="value"><?php print $product->unit; ?></span></li>
			</ul>
			<div class="to_cart_wrapper">
				<?if ($product->rest > 0):?>
			<noindex>
				<?php
				if (Cart::in_cart($product->id)) {
					$a_text = 'из корзины';
					$action = 'del';
				} else {
					$a_text = 'в корзину';
					$action = 'add';
				}

				print HTML::anchor(Route::url('cart',array('action'=>$action,'id'=>$product->id)),$a_text,array('class'=>'cart-add','id'=>$product->id,'action'=>$action,'rel'=>'nofollow'));
				?>
			</noindex>
				<?endif?>


				</div>
		</div>



	</div>

	<p class="text">
		<?php print nl2br($product->text); ?>
	</p>

</div>

<script type="text/javascript" language="javascript">
<!--
	$(document).ready(function(){
		var main_image = $('IMG#main_image');

		$('DIV.photos').on('click','A#small_image',function(){

			main_image.attr('src',$(this).attr('href'));

			return false;
		});

	});
-->
</script>
