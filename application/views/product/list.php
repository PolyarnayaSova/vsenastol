<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон списка товаров
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>

<?php if (isset($title) && $title): ?>
<h1 class="title"><?php print $title; ?></h1>
<?php endif; ?>

<?php
	// используется при выводе результатов расширенного поиска
	if (isset($advanced) && $advanced):
?>
<p class="advanced_search_params">
	<?php
		if (!empty($brands_names)) {
			print "<strong>В брендах: </strong>$brands_names";
			if (!empty($categories_names)) {
				print '<br />';
			}
		}

		if (!empty($categories_names)) {
			print "<strong>В категориях товаров: </strong>$categories_names";
		}
	?>
</p>
<?php
	endif;
?>

<?php
	foreach ($products as $p):

		$name = Text::symbols($p->name);

		if (isset($url_add) && $url_add) {
			$url = Route::url('product',array_merge(array('product'=>$name,'p_id'=>$p->id),$url_add));
		} else {
			// формирование "хвоста" категорий для каждого товара
			$category = $p->category;

			if (!$category->loaded()) {
				throw new HTTP_Exception_404('Категория товара не найдена');
			}

			$parent = $category->category;

			if ($parent->loaded()) {
				$url_add_ = array('category0'=>Text::symbols($parent->name),'c0_id'=>$parent->id,
									   'category1'=>Text::symbols($category->name),'c1_id'=>$category->id);
			} else {
				$url_add_ = array('category0'=>Text::symbols($category->name),'c0_id'=>$category->id);
			}

			$url = Route::url('product',array_merge(array('product'=>$name,'p_id'=>$p->id),$url_add_));
		}

		$image = Model_Product_Image::element($p->id,1);
		$image_src = $image->path('list');
?>
	<div class="lot">
		<div class="image-wrap"><div class="image-wrap-row">
			<div class="image">
				<?php print HTML::anchor($url,HTML::image($image_src,array('alt'=>$p->name))); ?>
			</div>
		</div></div>
		<div class="description">
			<?php print HTML::anchor($url,$p->name,array('class'=>'title')); ?>
			<span class="price">Цена: <?php print number_format($p->get_price(),0,',',' '); ?> руб.</span>
			<?php if ($p->brief): ?>
			<p class="brief short-brief">
				<?php print nl2br($p->brief); ?>
			</p>
			<?php endif; ?>
			<noindex>
				<?php
				if (Cart::in_cart($p->id)) {
					$a_text = 'из корзины';
					$action = 'del';
				} else {
					$a_text = 'в корзину';
					$action = 'add';
				}

				print HTML::anchor(Route::url('cart',array('action'=>$action,'id'=>$p->id)),$a_text,array('class'=>'cart-add','id'=>$p->id,'action'=>$action,'rel'=>'nofollow'));
				?>
			</noindex>
		</div>



	</div>
<?php
	endforeach;
?>

<?php if ($pager): ?>
<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
<div class="pager-wrap">
	<div class="product-pager">
		<?php print $pager; ?>
	</div>
	<div class="url_all">
		<?php if (isset($url_all) && !empty($url_all)): ?>
		<?php print HTML::anchor($url_all,'показать все'); ?>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>
