<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон списка новостей
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="brands">

	<h1 class="title">Бренды</h1>

	<?php
		foreach ($brands as $b):
			$image = Model_Content_Image::element($b->id,1);

			if ($image->loaded()) {
				$with_photo = ' with-photo';
				$image_src = $image->path('list');
			} else {
				$with_photo = '';
				$image_src = null;
			}

			$url = Route::url('brands',array('action'=>'show','brand'=>Text::symbols($b->name),'id'=>$b->id));
			$products_url = Route::url('brands',array('action'=>'products','brand'=>Text::symbols($b->name),'id'=>$b->id));
	?>
	<div class="item-wrap">

		<?php if ($image_src): ?>
		<div class="image-wrap"><div class="image-wrap-row"><div class="image">
			<?php print HTML::anchor($url,HTML::image($image_src,array('alt'=>$b->name))); ?>
		</div></div></div>
		<?php endif; ?>

		<div class="brand-item <?php print $with_photo; ?>">
			<div class="title-wrap">
			<?php print HTML::anchor($url,$b->name,array('class'=>'title')); ?>
			<?php print HTML::anchor($products_url,'товары ' . $b->name,array('class'=>'products')); ?>
			</div>
			<p class="brief"><?php print  nl2br($b->brief); ?></p>
		</div>

	</div>
	<?php
		endforeach;
	?>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager-wrap">
		<div class="pager">
			<?php print $pager; ?>
		</div>
	</div>
	<?php endif; ?>

</div>
