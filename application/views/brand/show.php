<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон страница Бренда
 *
 * @author ESV Corp. (С) 12.2011
 *
 */
?>
<div class="brands">

	<h1 class="title"><?php print $brand->name; ?></h1>

	<div class="text">
		<?php print $brand->text; ?>
	</div>

</div>
