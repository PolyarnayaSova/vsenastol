<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон страница совет
 *
 * @author ESV Corp. (С) 08.2012
 *
 */
?>
<div class="advices">

	<h1 class="title"><?php print $advice->name; ?></h1>

	<div class="text">
		<?php print $advice->text; ?>
	</div>

</div>
