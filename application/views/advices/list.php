<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * @project Посуда
 *
 * Шаблон списка советов
 *
 * @author ESV Corp. (С) 08.2012
 *
 */
?>
<div class="advices">

	<h1 class="title">Полезные советы</h1>

	<?php
		foreach ($advices as $a):
	?>
	<div class="advice-item">
		<?php print HTML::anchor(Route::url('advices',array('action'=>'show','id'=>$a->id)),$a->name,array('class'=>'title')); ?>
		<p class="brief"><?php print  nl2br($a->brief); ?></p>
	</div>
	<?php
		endforeach;
	?>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager-wrap">
		<div class="pager">
			<?php print $pager; ?>
		</div>
	</div>
	<?php endif; ?>

</div>
