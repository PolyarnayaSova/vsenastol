<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * Ошибка: страница не найдена
 *
 * @author ESV Corp. (C) 12.2011
 */
?>

<div class="error">
	<?php print $content; ?>
</div>
