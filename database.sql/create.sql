#
# Создание таблиц
#

#
# Контент страницы
#
CREATE TABLE contents (
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   parent_id		INT UNSIGNED NOT NULL DEFAULT 0,
   `level`        INT UNSIGNED DEFAULT 0,
	position			INT UNSIGNED DEFAULT 0,
   visible        INT(1) UNSIGNED DEFAULT 1,
   system         INT(1) UNSIGNED DEFAULT 0,
   type           INT(1) UNSIGNED DEFAULT 0,
   `key`          VARCHAR(255) DEFAULT '',
   name           VARCHAR(1024) DEFAULT '',
   `alias`        VARCHAR(1024) DEFAULT '',
   brief          TEXT NULL DEFAULT NULL,
   `text`         TEXT NULL DEFAULT NULL,
   title          VARCHAR(1024) DEFAULT '',
   keywords       TEXT NULL DEFAULT NULL,
   description    TEXT NULL DEFAULT NULL,

	KEY content (parent_id),
  	KEY `alias` (alias(10)),
   KEY `key` (`key`(10)),
	KEY position (position),
	KEY pos_in_parent (parent_id,position),
	KEY `level` (`level`),
	KEY type (type)

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


#
# Отдельные элементы контента страниц
#
CREATE TABLE content_items (
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   parent_id	   INT UNSIGNED DEFAULT 0,
	position			INT UNSIGNED DEFAULT 0,
   visible        INT(1) UNSIGNED DEFAULT 1,
   system         INT(1) UNSIGNED DEFAULT 0,
   type           INT(1) UNSIGNED DEFAULT 0,
   name           VARCHAR(1024) DEFAULT '',
   `alias`        VARCHAR(1024) DEFAULT '',
   `data`         TEXT NULL DEFAULT NULL,

	KEY content (parent_id),
  	KEY `alias` (`alias`(10)),
	KEY position (position),
	KEY pos_in_parent (parent_id,position),
   KEY ctp (parent_id,type,position),

   FOREIGN KEY content (parent_id) REFERENCES contents(id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


#
# Категории
#
CREATE TABLE categories (
	id				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	parent_id	INT UNSIGNED NOT NULL DEFAULT 0,
	`level`		INT UNSIGNED NOT NULL DEFAULT 0,
	position		INT UNSIGNED NOT NULL DEFAULT 0,
	visible		INT(1) UNSIGNED NOT NULL DEFAULT 1,
	name			VARCHAR(1024) NOT NULL,
	`alias`		VARCHAR(1024) NOT NULL,
	brief			TEXT NULL DEFAULT NULL,
	`text`		TEXT NULL DEFAULT NULL,
	title			VARCHAR(1024) DEFAULT '',
	keywords		TEXT NULL DEFAULT NULL,
	description	TEXT NULL DEFAULT NULL,

	KEY category (parent_id),
	KEY position (position),
	KEY pos_in_parent (parent_id,position),
	KEY `level` (`level`),
	KEY name (name(10)),
	KEY alias (alias(10))

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#
# Отдельные элементы: изображения и файлы категорий
#
CREATE TABLE category_items (
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   parent_id	   INT UNSIGNED DEFAULT 0,
	position			INT UNSIGNED DEFAULT 0,
   visible        INT(1) UNSIGNED DEFAULT 1,
   system         INT(1) UNSIGNED DEFAULT 0,
   type           INT(1) UNSIGNED DEFAULT 0,
   name           VARCHAR(1024) DEFAULT '',
   `alias`        VARCHAR(1024) DEFAULT '',
   `data`         TEXT NULL DEFAULT NULL,

	KEY category (parent_id),
  	KEY `alias` (`alias`(10)),
	KEY position (position),
	KEY pos_in_parent (parent_id,position),
   KEY ctp (parent_id,type,position),

   FOREIGN KEY fcategory (parent_id) REFERENCES categories(id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#
# Справочник материалов товаров
#
CREATE TABLE product_materials (
	id				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name			VARCHAR(1024) NOT NULL,
	`alias`		VARCHAR(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#
# Товары
#
CREATE TABLE products (
	id				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	created		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	category_id	INT UNSIGNED NOT NULL DEFAULT 0,
	brand_id		INT UNSIGNED NOT NULL DEFAULT 0,
	material_id	INT UNSIGNED NOT NULL DEFAULT 0,
	code			INT UNSIGNED NOT NULL DEFAULT 0,					# уникальный код
	`new`			INT(1) UNSIGNED NOT NULL DEFAULT 1,				# признак - новый
	`action`		INT(1) UNSIGNED NOT NULL DEFAULT 1,				# признак - участие в акции
	check_image	INT(1) UNSIGNED NOT NULL DEFAULT 1,				# признак - необходима проверка в подготовленных изображениях (для парсинга прайса)
	rest			DECIMAL(10,3) UNSIGNED NOT NULL DEFAULT 0,	# остаток
	weight		DECIMAL(7,3) UNSIGNED NOT NULL DEFAULT 0,		# вес
	capacity		DECIMAL(11,8) UNSIGNED NOT NULL DEFAULT 0,	# объем
	opt_price	DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 0,	# оптовая цена
	price			DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT 0,	# действующая цена
	opt_unit		VARCHAR(10)  NULL DEFAULT NULL,					# единицы измерения
	unit			VARCHAR(10)  NULL DEFAULT NULL,
	article		VARCHAR(250) NULL DEFAULT NULL,
	barcode		VARCHAR(250) NULL DEFAULT NULL,
	name			VARCHAR(1024) NOT NULL,
	`alias`		VARCHAR(1024) NOT NULL,
	brief			VARCHAR(1024) NULL DEFAULT NULL,
	`text`		TEXT NULL DEFAULT NULL,

	KEY category (category_id),
	KEY brand (brand_id),
	KEY code (code),
	KEY name (name(10)),
	KEY `alias` (`alias`(10)),
	KEY created (created),
	KEY price (price),
	KEY opt_price (opt_price),
	KEY `new` (`new`),
	KEY `action` (`action`),
	KEY added (added),

	FOREIGN KEY fcategory (category_id) REFERENCES categories(id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#
# Отдельные элементы товаров: изображения и файлы категорий
#
CREATE TABLE product_items (
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   parent_id	   INT UNSIGNED DEFAULT 0,
	position			INT UNSIGNED DEFAULT 0,
   visible        INT(1) UNSIGNED DEFAULT 1,
   system         INT(1) UNSIGNED DEFAULT 0,
   type           INT(1) UNSIGNED DEFAULT 0,
   name           VARCHAR(1024) DEFAULT '',
   `alias`        VARCHAR(1024) DEFAULT '',
   `data`         TEXT NULL DEFAULT NULL,

	KEY product (parent_id),
  	KEY `alias` (`alias`(10)),
	KEY position (position),
	KEY pos_in_parent (parent_id,position),
   KEY ctp (parent_id,type,position),

   FOREIGN KEY fproduct (parent_id) REFERENCES products(id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#
# Новости
#
CREATE TABLE news (
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   visible        INT(1) UNSIGNED DEFAULT 1,
   name           VARCHAR(1024) DEFAULT '',
   `alias`        VARCHAR(1024) DEFAULT '',
	created			TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`date`			DATE NULL DEFAULT NULL,
   brief          TEXT NULL DEFAULT NULL,
   `text`         TEXT NULL DEFAULT NULL,
   title          VARCHAR(1024) DEFAULT '',
   keywords       TEXT NULL DEFAULT NULL,
   description    TEXT NULL DEFAULT NULL,

  	KEY `alias` (alias(10)),
	KEY created (created),
	KEY `date` (`date`)

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#
# Полезные советы
#
CREATE TABLE advices (
   id             INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   visible        INT(1) UNSIGNED DEFAULT 1,
   name           VARCHAR(1024) DEFAULT '',
   `alias`        VARCHAR(1024) DEFAULT '',
   brief          TEXT NULL DEFAULT NULL,
   `text`         TEXT NULL DEFAULT NULL,
   title          VARCHAR(1024) DEFAULT '',
   keywords       TEXT NULL DEFAULT NULL,
   description    TEXT NULL DEFAULT NULL,

  	KEY `alias` (alias(10))

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


#
# Заказы
#
CREATE TABLE orders (
   id			INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	created	TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	dclosed	DATE NULL DEFAULT NULL,
	closed	INT(1) UNSIGNED DEFAULT 0,
	status	INT(1) UNSIGNED DEFAULT 0,
	self		INT(1) UNSIGNED DEFAULT 0,	# самовывоз
   fio	   VARCHAR(250) DEFAULT '',
   email	   VARCHAR(250) DEFAULT '',
   phone	   VARCHAR(250) DEFAULT '',
	addr		VARCHAR(250) DEFAULT '',
	city		INT UNSIGNED DEFAULT 0,
   `text`   TEXT NULL DEFAULT NULL,
	info		TEXT NULL DEFAULT NULL,
	blank		TEXT NULL DEFAULT NULL,
	comment	TEXT NULL DEFAULT NULL,

	KEY created (created),
	KEY dclosed (dclosed),
	KEY closed (closed),
	KEY status (status),
	KEY city (city)

) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
