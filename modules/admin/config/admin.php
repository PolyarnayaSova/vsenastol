<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 * Config Admin
 *
 * @package    Admin
 * @module		Admin
 * @category   Config file
 * @author     ESV Corp. since 07.2011
 */

return array (
	'caption'	=> 'Главная',	// заголовок меню для перехода на главную страницу админки
	'name'		=> 'admin',		// имя модуля админки
	'path'		=> 'admin',		// путь, используемый для URL админки
	'route'		=> 'admin', 	// имя роута для админки
	'include'	=> array (		// пути, используемые для подключения CSS, scripts, images
		'include',
		'media'
	),
	'logo'		=> 'include/icon/home.png', // логотип
	// конфигурация для шаблона заголовка страницы:
	// пути, файлы стилей, скрипты
	'head_page' => array (
		'author' => 'ESV Corp. &copy; 2011',
		'path' => array (
			'css'			=> 'include/css/',
			'script'		=> 'include/js/',
			'scripts'	=> 'include/js/',
			'image'		=> 'include/images/',
			'images'		=> 'include/images/'
		),
		'css' => array (
			'admin.css',
			'list.css',
			'form.css'
		),
		'scripts' => array (
			'init.js',	// инициализация различных подсистем
			'admin.js'
		),
	),
	'cookie_message' => 'admin_message',
	// привилегии для различных блоков редактирования
	// приведение к блоку привелегий в базе привелегий
	'privileges' => array (
		'admin' => 'admin',
		'seo' => 'seo-content'
	)
);
?>
