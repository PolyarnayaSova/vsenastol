<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Main controller
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
class Controller_Admin extends Controller_Basepage {

	private static $logged_in = false,
						$modules_menu = array(),
						$module_sub_menu = array();

	private static $message,	// сообщение
						$status;		// статус


	// генерация меню админки - пути к модулям, сохраняет в $modules_menu и
	// возвращает этот этот массив:
	// ключ - ссылка, значение - заголовок
	private function menu() {

		$modules = Admin::modules();

		foreach ($modules as $module) {
			if (!isset($module['route'])) {
				throw new Kohana_Exception('Неверная таблица модулей: пустой роут');
			}

			// модуль добавляется в меню только если у него указан заголовок
			// заголовок может быть null, тогда этот модуль внутренний и не попадает в систему меню
			if ($module['caption']!==null) {
				self::$modules_menu[Route::url($module['route'])] = $module['caption'];
			}
		}

		return self::$modules_menu;
	}


	// генерация меню модуля, сохраняет в $module_sub_menu и
	// и возвращает этот массив:
	// ключ - ссылка, значение - заголовок
	private function module_sub_menu() {

		if ($actions = Admin::actions()) {

			foreach ($actions as $href => $caption) {
				self::$module_sub_menu[$href] = $caption;
			}
		}

		return self::$module_sub_menu;
	}


	// логотип модуля
	private function logo() {

		$admin_prefix = Admin::admin_prefix();
		$module = Admin::module();

		if (($icon = Kohana::$config->load($module . '.logo')) ||
			 ($icon = Kohana::$config->load(str_replace($admin_prefix,'',$module) . '.logo'))) {

			$icon = Admin::prefix() . $icon;

		} else {

			// по-умолчанию используем логотип админки
			$icon = Admin::admin_prefix() . Kohana::$config->load(Admin::admin_module() . '.logo');

		}

		return $icon ? $icon : false;
	}


	// подключение css c учетом префикса пути текущего модуля
	// если указан DOCROOT, то префикс сбрасывается и подключаются файлы
	// относительно корня
	protected function css($css,$prefix='') {

		if ($prefix===DOCROOT) {
			$prefix = '';
		} else {
			$prefix = Admin::prefix() . $prefix;
		}

		return parent::css($css,$prefix);
	}


	// подключение scripts c учетом префикса пути текущего модуля
	// если указан DOCROOT, то префикс сбрасывается и подключаются файлы
	// относительно корня
	protected function scripts($scripts,$prefix='') {

		if ($prefix===DOCROOT) {
			$prefix = '';
		} else {
			$prefix = Admin::prefix(). $prefix;
		}

		return parent::scripts($scripts,$prefix);
	}


	public function before() {

		parent::before();

		// устанавливаем обработчик исключений
		if (!isset($_SERVER['KOHANA_ENV']) || strtoupper($_SERVER['KOHANA_ENV'])!='DEVELOPMENT') {
			set_exception_handler(array('Admin_Exception', 'handler'));
		}

		Admin::check_for_module();

		// определяем, произведен вход или нет
		self::$logged_in = Auth::instance()->logged_in('login');

		// построение меню модулей
		self::menu();

		// построение подменю модуля
		self::module_sub_menu();

		$admin_module = Admin::admin_module();
		$admin_prefix = Admin::admin_prefix();

		// подключаем JQuery
		parent::JQuery();

		// подключаем JQueryUI
		parent::JQueryUI();

		// подключаем ExtJS
		parent::ExtJS();

		// подключаем CKEditor
		parent::CKEditor();

		// scripts
		parent::scripts(Kohana::$config->load($admin_module . '.head_page.scripts'),
			$admin_prefix . Kohana::$config->load($admin_module . '.head_page.path.scripts'));

		// css
		parent::css(Kohana::$config->load($admin_module . '.head_page.css'),
			$admin_prefix . Kohana::$config->load($admin_module . '.head_page.path.css'));


		// подключаем css и скрипты текущего модуля, если присутствует конфигурация
		$module = Admin::module();
		if ($admin_module!=$module) {

			if (Kohana::$config->load($module . '.head_page') ||
				 Kohana::$config->load(($module=str_replace($admin_prefix,'',$module)) . '.head_page')) {

				// подключение таблиц и стилей модуля
				// внутри структуры модуля
				self::css(Kohana::$config->load($module . '.head_page.css'), Kohana::$config->load($module . '.head_page.path.css'));
				self::scripts(Kohana::$config->load($module . '.head_page.scripts'), Kohana::$config->load($module . '.head_page.path.scripts'));
				// в корне структуры относительно DOCROOT
				$root_config = Kohana::$config->load($module . '.head_page.root');
				if (!empty($root_config) && is_array($root_config)) {
					parent::css(Kohana::$config->load($module . '.head_page.root.css'), Kohana::$config->load($module . '.head_page.root.path.css'));
					parent::scripts(Kohana::$config->load($module . '.head_page.root.scripts'), Kohana::$config->load($module . '.head_page.root.path.scripts'));
				}
			}
		}

		$this->title = 'Administration tools';
		$this->author = "ESV Corp. &copy; 2011";
	}


	public function after() {

		if (self::$logged_in) {

			// проверяем собщения для отображения
			list($message,$status) = self::message();

			$this->content =
				View::factory(Admin::admin_prefix() . 'content')
					->set('logo',self::logo())
					->set('modules_menu',self::$modules_menu)
					->set('module_sub_menu',self::$module_sub_menu)
					->set('content',$this->content)
					->set('message',$message)
					->set('status',$status);

		} else {

			// если не админская часть выполняется, перенаправляем в админку
			// вернемся сюда же, но по правильному роуту, пути
			if (Admin::route()!=Admin::admin_route()) {
				$this->request->redirect(Admin::admin_route());
				return;
			}

			$this->content = View::factory(Admin::admin_prefix() . 'login');
		}

		parent::after();
	}


	public function action_index() {

		$this->content = View::factory('admin/default');
	}


	// получение блока сообщения
	public function message() {

		// получаем сообщение
		list($message,$status) = Admin::message();

		if (!empty($message)) {
			$message = View::factory('admin/message')
				->set('message',$message)
				->set('status',$status);
		} else {
			$message = null;
			$status = 0;
		}

		return array($message,$status);
	}


	public function action_login() {

		$auth = Auth::instance();

		$auth->logout();

		self::$logged_in = false;

		// если переданы данные для аутентификации, попробуем войти
		if ($this->request->method()=='POST' &&
			 ($post=$this->request->post()) &&
			 ($username=Arr::get($post,'user')) &&
			 ($password=Arr::get($post,'pass'))) {

			$auth->login($username,$password,true);
		}

		$this->request->redirect(Route::url(Admin::admin_route()));
	}


	// ВНИМАНИЕ!!!
	// инициализация - добавить администратора
	public function action_init() {

		// проверка существующего пользователя 'admin'
		$user = new Model_User(array('username'=>'admin'));

		// если пользователя не существует
		if (!$user->loaded()) {

			$user->username = 'admin';
			$user->password = '1234';
			$user->email = 'admin@admin.ru';
			$user->save();

			// определение привелегий
			$user->remove('roles');
			$user->add('roles',array(1,2)); // возможности логина и администратора
		}

		$this->request->redirect(Route::url(Admin::admin_route()));
	}
}
