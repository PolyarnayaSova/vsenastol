<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Error handler controller
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 11.2011
 */
class Controller_Admin_Error extends Controller_Basepage {

	public function before() {
		parent::before();

		// подключаем стили и скрипты админки
		$admin_module = Admin::admin_module();
		$admin_prefix = Admin::admin_prefix();

		// JQuery
		parent::JQuery();

		// css
		parent::css(Kohana::$config->load($admin_module . '.head_page.css'),
			$admin_prefix . Kohana::$config->load($admin_module . '.head_page.path.css'));

		// scripts
		parent::scripts(Kohana::$config->load($admin_module . '.head_page.scripts'),
			$admin_prefix . Kohana::$config->load($admin_module . '.head_page.path.scripts'));
	}

	public function action_error() {

		$code = $this->request->param('code',null);

		list($message,$status) = Admin::message();

		if ($code!==null) {
			// обработка специальных кодов
			switch ($code) {
				case 'browser': // браузер не поддерживает необходимую функциональность
					$message = View::factory('admin/error/browser');
					$status = Admin::MSG_INFO;
					break;
				default:
					$message .= ", код: $code";
			}
		}

		$this->title = 'Ошибка';

		$this->content = View::factory('admin/error/error')
			->set('message',$message)
			->set('status',$status)
			->set('code',$code);
	}
}
