<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * access for include files: CSS, scripts, images, etc.
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
class Controller_Admin_Include extends Controller {

	public function action_get() {

		// Get the file path from the request
		$file = $this->request->param('file');

		$path = Admin::get_route_include();

		// Find the file extension
		$ext = pathinfo($file, PATHINFO_EXTENSION);

		$file = $path . $file;

		if (is_file($file) && is_readable($file)) {

			// Check if the browser sent an "if-none-match: <etag>" header, and tell if the file hasn't changed
			$this->response->check_cache(sha1($this->request->uri()).filemtime($file), $this->request);

			// Send the file content as the response
			$this->response->body(file_get_contents($file));

			// Set the proper headers to allow caching
			$this->response->headers('content-type',  File::mime_by_ext($ext));
			$this->response->headers('last-modified', date('r', filemtime($file)));

		} else {

			// Return a 404 status
			$this->response->status(404);
		}
	}
}