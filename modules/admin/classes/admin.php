<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Main control class
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */

abstract class Admin {

	const DEFAULT_ADMIN_PATH = 'admin',
			DEFAULT_ADMIN_ROUTE = 'admin',
			DEFAULT_ADMIN_MODULE_NAME = 'admin',
			DEFAULT_ADMIN_CAPTION = 'Главная';

	// типы сообщений
	const MSG_MESSAGE = 0,	// уведомление
			MSG_INFO		= 0,
			MSG_SUCCESS = 1,	// успех
			MSG_OK		= 1,
			MSG_WARNING = 2,	// предупреждение
			MSG_WARN		= 2,
			MSG_ALERT	= 2,
			MSG_ERROR	= 3,	// ошибка
			MSG_FAIL		= 3,
			MSG_ABORT	= 3;

	public $const_DEFAULT_ADMIN_INCLUDES = array('include');

	private static $modules = array(),
						$includes = array();

	private static $message = null;
	private static $msg_status = self::MSG_MESSAGE;


	private function __construct() { }


	// наименование меню, заголовок для модуля админки
	public static function admin_caption() {

		$name = Kohana::$config->load('admin.caption');
		if (empty($name)) $name=self::DEFAULT_ADMIN_CAPTION;

		return $name;
	}

	// путь до модуля админки
	public static function admin_path() {

		$name = Kohana::$config->load('admin.path');
		if (empty($name)) $name=self::DEFAULT_ADMIN_PATH;

		return $name;
	}

	// получение имя роута админки
	public static function admin_route() {

		$name = Kohana::$config->load('admin.route');
		if (empty($name)) $name=self::DEFAULT_ADMIN_ROUTE;

		return $name;
	}


	// получение имени модуля админки
	public static function admin_module() {

		$name = Kohana::$config->load('admin.name');
		if (empty($name)) $name = self:: DEFAULT_ADMIN_MODULE_NAME;

		$name = trim($name,' ' . DIRECTORY_SEPARATOR);

		return $name;
	}


	// префикс для модуля админки
	public static function admin_prefix() {

		return self::admin_path()  . DIRECTORY_SEPARATOR;
	}


	// наименование каталогов для включаемых файлов
	public static function admin_includes() {

		$includes = Kohana::$config->load('admin.include');
		if (empty($includes)) $includes = self::$const_DEFAULT_ADMIN_INCLUDES;

		return $includes;
	}


	// таблица модулей
	public static function modules() {
		return self::$modules;
	}


	// таблица actions для определенного модуля
	public static function actions($module=null) {

		$module = trim($module,' ' . DIRECTORY_SEPARATOR);

		// если не указано, то получаем имя текущего модуля
		if (empty($module)) $module = self::module();

		if (isset(self::$modules[$module]['actions'])) return self::$modules[$module]['actions'];

		return false;
	}


	// имя исполняемого роута
	public static function route() {

		return Route::name(Request::current()->route());
	}


	// проверка на то, что указанный модуль существует для работы
	// должна запускаться в контроллере, перед началом работы
	public static function check_for_module() {

		try {
			self::module();
		} catch (Exception $e) {
			throw new Admin_Exception('Данный адрес не существует',null,404);
		}
	}


	// имя исполняемого модуля
	public static function module($by=null) {

		// по-умолчанию используем текущий роут, который получаем из запроса
		if (empty($by)) {
			$by = Request::current()->route();
		}

		if (is_object($by)) {
			if ($by instanceof Request) {
				$route = $by->route();
			} elseif ($by instanceof Route) {
				$route = $by;
			} else {
				throw new Kohana_Exception('Ошибка получения имени модуля: объект класса ' . get_class($by) . ' не может быть использован');
			}

			$route = Route::name($route);

			if (self::admin_route()==$route) return self::admin_module();

			foreach (self::$modules as $name => $module) {
				foreach ($module['routes'] as $r) {
					if ($route==$r) return $name;
				}
			}

			throw new HTTP_Exception_404("Ошибка получения имени модуля: роут '$route' не найден, для пути: " . Request::current()->uri());
		}

		throw new Kohana_Exception('Ошибка получения имени модуля: не определен объект');
	}


	// получение префикса пути для определенного модуля, если не указан, то для текущего
	public static function prefix($module=null) {

		$module = trim($module,' ' . DIRECTORY_SEPARATOR);

		// если не указано, то получаем имя текущего модуля
		if (empty($module)) $module = self::module();

		if (self::admin_module()==$module) return self::admin_prefix();

		if (isset(self::$modules[$module])) return self::$modules[$module]['prefix']  . DIRECTORY_SEPARATOR;

		return false;
	}


	// наименование меню, заголовок для модуля
	public static function caption($module=null) {

		$module = trim($module,' ' . DIRECTORY_SEPARATOR);

		// если не указано, то получаем имя текущего модуля
		if (empty($module)) $module = self::module();

		$modules = self::modules();

		if (isset($modules[$module]['caption'])) return $modules[$module]['caption'];

		return false;
	}


	// получение пути включений для текущего роута
	// для каждого модуля определены свои пути подключаемых файлов
	public static function get_route_include() {

		$route = self::route();

		if (!$route || !isset(self::$includes[$route])) return false;

		return self::$includes[$route];
	}


	// назначение маршрутов модуля для доступа к включаемым файлам
	public static function set_includes_module($module) {

		$system_modules = Kohana::modules();

		$includes = self::admin_includes();

		foreach ($includes as $include) {

			$include = trim($include,' ' . DIRECTORY_SEPARATOR);

			if (empty($include)) continue;

			$route = "$module/$include";

			if (self::admin_module()==$module) {
				$prefix = $module;
			} else {
				$prefix = self::$modules[$module]['prefix'];
			}

			// Static file serving (CSS, JS, images)
			Route::set($route, "$prefix/$include(/<file>)", array('file' => '.+'))
				->defaults(array(
					'controller' => 'admin_include',
					'action'     => 'get',
					'file'       => NULL,
				));

			// для данного роута запоминаем путь
			self::$includes[$route] = $system_modules[$module] . $include . DIRECTORY_SEPARATOR;
		}
	}


	// проверка, зарегистрирован ли модуль
	private static function isRegisteredModule($module) {

		$module=trim($module,' ' . DIRECTORY_SEPARATOR);

		return isset(self::$modules[$module]);
	}


	// регистрация модуля в админке
	// @param string $name		- наименование модуля
	// @param string $route 	- роут по-умолчанию для модуля
	// @param string $caption	- заголовок, наименование модуля, используется для отображения в меню,
	//										если caption = null, тогда данный модуль не отображается в меню
	public static function RegisterModule($module, $route, $caption=null) {

		$caption	= ($caption!==null ? trim($caption,' ') : null);
		$route	= trim($route,' ' . DIRECTORY_SEPARATOR);
		$module	= trim($module,' ' . DIRECTORY_SEPARATOR);

		if (empty($module) || self::admin_module()==$module) {
			throw new Kohana_Exception("Ошибка регистрации модуля '$module'");
		}

		$system_modules = array_keys(Kohana::modules());

		if (!in_array($module,$system_modules)) {
			throw new Kohana_Exception("Модуль '$module' не зарегистрирован в системе");
		}

		if (!self::isRegisteredModule($module)) {

			// проверка, включает ли имя модуля префикс пути до модуля админки
			// т.е. имя модуля может быть задано как admin/module или просто module
			$admin_prefix = self::admin_prefix();

			$prefix = strpos($module,$admin_prefix)===0 ? $module : ($admin_prefix . $module);

			self::$modules[$module] = array('route' => $route, 'caption' => $caption, 'prefix' => $prefix, 'routes' => array());

			// добавляем маршруты для доступа к включаемым файлам:
			// css, scripts, images и т.п.
			self::set_includes_module($module);

			return true;
		}

		return false;
	}


	// добавление маршрута
	// @param string $module	- наименование модуля
	// @param string $name				- то же, что и в Route
	// @param string $uri_callback	- то же, что и в Route
	// @param string $regex				- то же, что и в Route
	// @return Route::set
	public static function RouteSet($module, $route, $uri_callback=NULL, $regex=NULL) {

		$module = trim($module,' ' . DIRECTORY_SEPARATOR);

		if (empty($module)) throw new Exception('Module name not found');

		try {
			Route::get($route);
			throw new Exception("Route '$route' already exists");
		} catch (Kohana_Exception $e) {
			// Все хорошо - такого роута еще нет
		}

		// добавляем в таблицу роутов текущего модуля
		self::$modules[$module]['routes'][] = $route;

		// админские модули подключаем напрямую без префикса
		if (self::admin_module()==$module) {

			// если еще не определен основной роут для админки в таблице модулей
			if (!isset(self::$modules[$module]['route'])) {
				self::$modules[$module]['caption'] = self::admin_caption();
				self::$modules[$module]['route'] = self::admin_route();
			}

			return Route::set($route, $uri_callback, $regex);
		}

		// проверка регистрации модуля админки, который должен уже быть в таблице
		$admin_module = self::admin_module();
		if (!self::isRegisteredModule($admin_module)) {
			throw new Kohana_Exception("Главный модуль '$admin_module' не зарегистрирован - добавление маршрута невозможно");
		}

		// и проверка регистрации самого модуля
		if (!self::isRegisteredModule($module)) {
			throw new Kohana_Exception("Модуль '$module' не зарегистрирован - добавление маршрута невозможно");
		}

		// проверка, включает ли $uri_callback префикс пути до модуля админки
		// т.е. $uri_callback может быть задано как admin/uri или просто uri
		$admin_prefix = self::admin_prefix();

		$uri_callback = strpos($uri_callback,$admin_prefix)===0 ? $uri_callback : ($admin_prefix . $uri_callback);

		return Route::set($route, $uri_callback, $regex);
	}


	// добавление действия, элемента подменю для модуля
	public static function ActionSet($module, $action, $caption) {

		$action = trim($action,' '.DIRECTORY_SEPARATOR);

		// проверка регистрации модуля
		if (!self::isRegisteredModule($module)) {
			throw new Kohana_Exception("Модуль '$module' не зарегистрирован - добавление Action '$action' невозможно");
		}

		$modules = self::modules();
		$prefix = self::prefix($module);
		$admin_prefix = self::admin_prefix();

		// проверка, включает ли $action префикс пути до модуля админки
		// т.е. $action может быть задан как admin/uri или просто uri
		$action = strpos($action,$admin_prefix)===0 ? $action : ($admin_prefix . $action);

		if (strpos($action,$prefix)!==0) {
			throw new Kohana_Exception("Action '$action' не относится к модулю '$module' - добавление невозможно");
		}

		$action = DIRECTORY_SEPARATOR . $action;

		if ($modules[$module]['caption']===null) {
			throw new Kohana_Exception("Модуль '$module' является внутренним - добавление action '$action' невозможно");
		}

		self::$modules[$module]['actions'][$action] = $caption;
	}


	//
	//=== сервисные функции
	//

	// сообщение - установить, получить
	public static function message($message=null,$status=self::MSG_MESSAGE,$cookie=true) {

		$cookie_msg = Cookie::$salt = Kohana::$config->load('admin.cookie_message');

		if  ($message) {
			self::$message = $message;
			self::$msg_status = $status;

			Cookie::set($cookie_msg,serialize(array(self::$message,self::$msg_status)),300);

			return $message;
		}

		if (empty(self::$message) && ($cookie = Cookie::get($cookie_msg,false))) {

			list(self::$message,self::$msg_status) = unserialize($cookie);

		}

		Cookie::delete($cookie_msg);

		self::$message = trim(self::$message);

		if (!empty(self::$message)) {
			self::$message = nl2br(self::$message);
		}

		return array(self::$message, self::$msg_status);
	}


	// текущий пользователь, зарегистрировавшийся в системе, либо NULL
	// @return Model_User or NULL
	public static function user() {

		return Auth::instance()->get_user();
	}

	// проверка, имеет ли пользователь, зарегистрировавшийся в системе, определенный уровень привелегий
	// @return boolean
	public static function logged_in($role) {

		return Auth::instance()->logged_in($role);
	}
}
