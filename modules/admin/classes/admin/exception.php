<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Main control class
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */

class Admin_Exception extends Kohana_Exception {

   public function __construct($message = 'Unknown error', array $variables = NULL, $code = 0) {
      parent::__construct($message, $variables, $code);
   }

	public static function handler(Exception $e) {

		$status = Admin::MSG_ERROR;

      switch (get_class($e)) {

			case 'Admin_Exception':
				if ($e->getCode()==404) {
					$message = 'Страница не найдена';
				} else {
					$message = $e->getMessage();
				}
				break;

			case 'Kohana_Request_Exception':
			case 'HTTP_Exception_404':
				$message = 'Страница не найдена: ' . $e->getMessage();
				break;

			case 'Kohana_Exception':
			case 'HTTP_Exception':
			case 'HTTP_Exception_400':
				$message = $e->getMessage();
				break;

			default:
				return parent::handler($e);
      }

		// установим сообщение и статус
		Admin::message($message,$status);
		$request = Request::factory(Route::url(Admin::admin_route().'/error',array('action'=>'error')))->execute();
		print $request->send_headers()->body();
		return TRUE;

	}
}
