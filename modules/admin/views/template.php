<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 * 
 * Main template
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<?php
	print View::factory('head')
		->set('meta',$meta)
		->set('title',$title)
		->set('keywords',$keywords)
		->set('description')
		->set('author',$author)
		->set('css',$css)
		->set('scripts',$scripts);
?>
<body>
<div class="header">
	<h1>Админка</h1>
</div>

<div class="body">
    <div class="col_left">
    <h1>Меню</h1>
    </div>
    
    <div class="col_center">
    Меню
    </div>
</div>

<div class="footer">
</div>

</body>
</html>
