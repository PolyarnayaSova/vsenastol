<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Login template
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
?>
<style type="text/css">
BODY {
	background:ivory;
}
</style>

<div class="login">

	<?php print Form::open(Route::url(Admin::admin_route().'/login'),array('method'=>'post')); ?>

		<table border="0">
			<tr>
				<th colspan="3" align="center">Администрирование</th>
			</tr>
			<tr>
				<td width="30%" rowspan="3" style="text-align:center; vertical-align:top;">
					<img src="/admin/include/icon/lock.png" border="0" />
				</td>
				<td align="right">логин</td>
				<td width="50%"><?php print Form::input('user','',array('size'=>15)); ?></td>
			</tr>
			<tr>
				<td align="right">пароль</td>
				<td width="50%"><?php print Form::password('pass','',array('size'=>15)); ?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align="left"><?php print Form::submit('enter','войти'); ?></td>
			</tr>
		</table>

	<?php print Form::close(); ?>

</div>
