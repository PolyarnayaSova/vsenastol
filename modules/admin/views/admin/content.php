<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Main template
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
?>

<div class="wrap">

	<div class="header"><div class="clearfix">
		<div class="info">
			система администрирования
		</div>
		<div class="logo">
			<?php print HTML::image($logo); ?>
		</div>
		<div class="inner">
			<h1><?php $module_caption = Admin::caption(); print $module_caption; ?></h1>
			<h4>пользователь: <?php print Admin::user()->username; ?></h4>
		</div>
	</div></div>


	<div class="wrap_menu"><div class="clearfix">

		<div class="menu"><div class="clearfix">
			<?php
				// пункт "выход"
				print HTML::anchor(Route::url(Admin::admin_route().'/login'), 'выйти', array('class'=>'logout'));

				foreach ($modules_menu as $href => $caption) {

					print HTML::anchor($href,$caption,array('class'=>'menu_item'));

				}
			?>
		</div></div>

		<?php if (isset($module_sub_menu) && !empty($module_sub_menu)): ?>
			<div class="submenu"><div class="clearfix">
				<div class="module_menu_caption"><?php print "$module_caption:"; ?></div>
				<?php

					foreach ($module_sub_menu as $href => $caption) {

						print HTML::anchor($href,$caption,array('class'=>'menu_item'));

					}
				?>
			</div></div>
		<?php endif; ?>

	</div></div>

	<div class="body"><div class="clearfix">

		<?php
			// если есть сообщение
			if ($message) { print $message; }
		?>

		<div class="content">
				<?php print $content; ?>
		</div>

	</div></div>

	<div class="footer">
		<div class="copyright">
			ESV Corp. &copy; 2011
		</div>
	</div>

</div>
