<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Error message block
 * Блок отображения ошибок
 *
 * @package    Admin
 * @module		Admin
 * @category   Template
 * @author     ESV Corp. since 11.2011
 */
?>
<div class="message">
	<?php if ($status==Admin::MSG_MESSAGE): ?>
	<div class="msg_message">
		<?php print HTML::image(Admin::admin_prefix() . "include/icon/32/info.png"); ?>
		<p><?php print $message; ?></p>
	</div>
	<?php endif; ?>

	<?php if ($status==Admin::MSG_SUCCESS): ?>
	<div class="msg_success">
		<?php print HTML::image(Admin::admin_prefix() . "include/icon/32/success.png"); ?>
		<p><?php print $message; ?></p>
	</div>
	<?php endif; ?>

	<?php if ($status==Admin::MSG_WARNING): ?>
	<div class="msg_warning">
		<?php print HTML::image(Admin::admin_prefix() . "include/icon/32/warning.png"); ?>
		<p><?php print $message; ?></p>
	</div>
	<?php endif; ?>

	<?php if ($status==Admin::MSG_ERROR): ?>
	<div class="msg_error">
		<?php print HTML::image(Admin::admin_prefix() . "include/icon/32/error.png"); ?>
		<p><?php print $message; ?></p>
	</div>
	<?php endif; ?>
</div>
