<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Error page template
 *
 * @package    Admin
 * @module		Admin
 * @category   Template
 * @author     ESV Corp. since 11.2011
 */
?>
<div class="wrap">
	<div class="body"><div class="clearfix">
		<?php print View::factory('admin/message')->set('message',$message)->set('status',$status); ?>
	</div></div>
</div>
