<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Error Browser page template
 *
 * @package    Admin
 * @module		Admin
 * @category   Template
 * @author     ESV Corp. since 11.2011
 */
?>

<style type="text/css">
BODY {
	background:none;
}
</style>

Ваш браузер не поддерживает необходимую функциональность.<br />
Попробуйте один из следующих:
<a href="http://www.google.com/chrome?hl=ru" target="_top">Chrome</a>,&nbsp;
<a href="http://mozilla-russia.org/" target="_top">Firefox</a>,&nbsp;
<a href="http://ru.opera.com/" target="_top">Opera</a><br />
либо какой-то другой...
