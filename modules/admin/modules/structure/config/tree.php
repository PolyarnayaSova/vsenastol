<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Config Tree
 *
 * @package    Tree
 * @module		Tree
 * @category   Config file
 * @author     ESV Corp. since 11.2011
 */

return array (
	// скрипты находятся в корне структуры - DOCROOT
	'scripts' => array (
		'include/js/lib/ajax/ajax_navigation.js',
		'include/js/lib/ajax/jquery.history.js',
		'include/js/lib/ajax/tree/tree.js'
	),
	// таблицы стилей находятся в корне структуры DOCROOT
	'css' => array (
		'include/js/lib/ajax/tree/css/tree.css'
	)
);
?>
