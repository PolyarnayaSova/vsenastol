<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Structure module
 *
 * Abstract class interface Tree
 *
 * @package    Structure
 * @category   Controller_Tree_Interface
 * @module		Structure/Tree
 * @author     ESV Corp. since 10.2011
 */

// интерфейс Controller_Tree_Interface
// необходима раелизация следующих методов:
// action_tree(POST[node])	- получение элементов дерева
// action_get/id				- получение контента определенного элемента
// action_add/id				- добавление потомка к элементу
// action_del/id				- удаление элемента
// action_move(POST[drag_id,drop_id,point]) - перемещение элемента
//

interface Controller_Tree_Interface {

	// получение потомков элемента
	// в данный метод через POST передается параметр 'node'
	// ожидается ответ в виде JSON, описывающий элементы потомков
	// данного 'node', обязательно присутствие 'id', 'leaf'
	// id = 0, если должны быть получены элементы корня
	public function action_tree();

	// получение контента элемента
	// id передается первым параметром: action = get, далее id : get/id
	public function action_get();

	// добавление элемента
	// id передается первым параметром: action = get, далее id : add/id
	// id элемента, к которому добавляется потомок, id=0, если добавление в корень дерева
	public function action_add();

	// удаление элемента
	// id передается первым параметром: action = get, далее id : get/id
	public function action_del();

	// перемещение элементов
	// в данный метод через POST передаются параметры:
	// drag_id - перемещаемый элемент
	// drop_id - элемент, на который происходит перемещение
	// point		- одно из: 'above' - над drop_id, 'below' - под drop_id
	public function action_move();

}
