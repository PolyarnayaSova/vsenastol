<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Tree class
 *
 * Используется для отрисовки блоков с использованием дерева
 * использует JavaScript-ы в DOCROOT/include/lib/ajax/, DOCROOT/include/lib/ajax/tree
 *
 * @package    Tree
 * @category   Class
 * @author     ESV Corp. since 07.2011
 */

abstract class Tree {

	private static $was_init = false,
						$was_css = false,
						$was_scripts = false;

	// префиксы init генерируются полные id
	private static $tree_id = 'tree_', // id элемента дерева
						$tree_block_id = '__ajax_tree_', // id блока (div) для отрисовки дерева
						$tree_var_name = '__AJAX_TREE'; // наименование переменной для дерева

	// конфигурация для генерации шаблона отображения
	private static
		$root_id,
		$root_name,
		$width,
		$height,
		$dataUrl,
		$getUrl,
		$addUrl,
		$delUrl,
		$moveUrl,
		$content_block_id = '__ajax_content__';


	private function __construct() { }


	private static function check_valid() {

		if (!self::$was_init) {
			throw new Kohana_Exception('Ошибка использования Tree: класс не был инициализирован перед использованием');
		}

		if (!self::$was_css) {
			throw new Kohana_Exception('Ошибка использования Tree: не были получены таблицы стилей');
		}

		if (!self::$was_scripts) {
			throw new Kohana_Exception('Ошибка использования Tree: не были получены скрипты');
		}
	}


	// инициализация параметров дерева
	public static function init (
		$root_id,	// id корня дерева
		$root_name,	// наименование корня дерева
		$width,		// ширина
		$height,		// высота
		$url,			// url контроллера для работы с деревом, должен реализовать Controller_Interface_Tree
		$content_block_id=null	// id блока (div) контента, в который загружается контент по ajax
										// сделано, чтобы возможно было переопределять стили
	) {

		$root_name = trim($root_name);

		if (empty($root_name) || empty($width) || empty($height)) {
			throw new Kohana_Exception("Ошибка инициализации Tree: root_name='$root_name', width='$width', height='$height'");
		}

		$url = trim($url,' ' . DIRECTORY_SEPARATOR);
		if (empty($url)) {
			throw new Kohana_Exception("Ошибка инициализации Tree: неверный url '$url'");
		}
		$url =  DIRECTORY_SEPARATOR . $url . DIRECTORY_SEPARATOR;

		self::$root_id		= $root_id;
		self::$root_name	= $root_name;
		self::$width		= $width;
		self::$height		= $height;
		self::$dataUrl		= $url . 'tree'; 								// url для получения дерева
																					// не должен заканчиваться на '/'
		self::$getUrl		= $url . 'get' . DIRECTORY_SEPARATOR;	// url для получения контент-страницы элемента,
																					// используется как getUrl + id элемента, контент-страница
																					// которого должна быть получена
																					// должен заканчиваться на '/'
		self::$addUrl		= $url . 'add' . DIRECTORY_SEPARATOR;	// url для добавления элемента, используется как
																					// addUrl + id элемента, к которому добавляется
																					// должен заканчиваться на '/'
		self::$delUrl		= $url . 'del' . DIRECTORY_SEPARATOR;	// url для удаления элемента,  используется как
																					// delUrl + id элемента, который удаляется
																					// должен заканчиваться на '/'
		self::$moveUrl		= $url . 'move';								// url для перемещения элемента,  используется как url с передачей POST-запроса:
																					// параметры:
																					//	drag_id - id перемещаемого элемента
																					// drop_id - id элемента, на который перемещается
																					// point   - точка перемещения: 'above' или 'below'
																					// не должен заканчиваться на '/'

		// генерация случайных id
		self::$tree_id .= rand(date('is'),9999);
		self::$tree_block_id .= rand(date('is'),9999) . '__';


		$content_block_id = trim($content_block_id);
		if (!empty($content_block_id)) {
			self::$content_block_id = $content_block_id;
		}

		self::$was_init = true;
	}


	// получение списка файлов таблиц стилей
	public static function css() {

		$css = Kohana::$config->load('tree.css');

		if (empty($css)) {
			throw new Kohana_Exception('Ошибка конфигурации Tree: не могу получить таблицы стилей');
		}

		self::$was_css = true;

		return $css;
	}


	// получение списка файлов скриптов
	public static function scripts() {

		$scripts = Kohana::$config->load('tree.scripts');

		if (empty($scripts)) {
			throw new Kohana_Exception('Ошибка конфигурации Tree: не могу получить js-скрипты');
		}

		self::$was_scripts = true;

		return $scripts;
	}


	// контент
	public static function content() {

		self::check_valid();

		return
			View::factory('tree/tree_content')
				->set('root_id',self::$root_id)
				->set('root_name',self::$root_name)
				->set('width',self::$width)
				->set('height',self::$height)
				->set('dataUrl',self::$dataUrl)
				->set('getUrl',self::$getUrl)
				->set('addUrl',self::$addUrl)
				->set('delUrl',self::$delUrl)
				->set('moveUrl',self::$moveUrl)
				->set('tree_id',self::$tree_id)
				->set('tree_block_id',self::$tree_block_id)
				->set('tree_var_name',self::$tree_var_name)
				->set('content_block_id',self::$content_block_id);
	}


	// блок скпипта добавления элемента в дерево
	public static function content_add_node($add_node_id) {

		if (empty($add_node_id)) {
			throw new Kohana_Exception('Ошибка использования Tree: невозможно реализовать скрипт добавления пустого node');
		}

		return
			View::factory('tree/tree_add_node')
				->set('add_node_id',$add_node_id)
				->set('tree_var_name',self::$tree_var_name);
	}


	// получение, установка имени переменной для хранения объекта ExtJS Tree
	public static function var_name($var_name=null) {

		$var_name = trim($var_name);

		if (!empty($var_name)) {
			self::$tree_var_name = $var_name;
		}

		return self::$tree_var_name;
	}
}
