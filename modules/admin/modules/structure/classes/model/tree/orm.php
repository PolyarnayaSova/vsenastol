<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Model Tree_ORM
 *
 * @package    Structure
 * @category   Model
 * @module		Tree
 * @author     ESV Corp. since 10.2011
 */

// предполагает обязательное наличие полей структуры:
// id				INT UNSIGNED
// parent_id	INT UNSIGNED
// level			INT UNSIGNED
// position		INT UNSIGNED

abstract class Model_Tree_ORM extends Model_List_Sorted_ORM {

   // добавление дочернего элемента, возможно добавление даже к незагруженному
	// ноду, тогда происходит добавление в корень дерева, если нод для добавления не указан (null),
	// тогда создается новый нод
	// Возвращает: добавленный элемент
   public function add_child(Model_Tree_ORM $node=null,$values=array(),$columns=null) {

      // пустой нод
      if (!$node) {
			$class = get_called_class();
			$node = new $class();
		}

      if ($this->loaded() && $this->id>0) {
         $id    = $this->id;
         $level = $this->level+1;
      } else {
         $id    = 0;
         $level = 0;
      }

		if (!empty($values)) {

			if (empty($columns)) {
				$columns = array_keys($this->list_columns());
			}

			unset($columns['id']);
			unset($columns['parent_id']);
			unset($columns['level']);
			unset($columns['position']);

			$node->values($values,$columns);
		}


		$node->id = null; // обязательно сбрасываем id - добавляем новый элемент, чтобы отработал
								// метод create - используется механизм наследования
								// см. Model_List_Sorted_ORM
								// позиция вычисляется автоматически
      $node->parent_id	= $id;
      $node->level		= $level;
      $node->save();

      return $node;
   }


 	// определение, имеет ли потомков
	public function has_children() {

		if (!$this->loaded()) return false;

		$count = $this->where('parent_id','=',$this->id)->count_all();

		return (0 < $count);
	}


	// является листом
	public function is_leaf() {
		return !$this->has_children();
	}


	// является ли объект потомком $by
	public function is_descendant($by) {

		if (!$this->loaded()) return false;

		$node = self::get($by);

		if (!$node || !$node->loaded()) {
			return false;
		}

		$path = $this->path();

		// ищем в пути текущего объекта ($this) появление объекта $by
		foreach ($path as $p) {
			if ($p->id == $node->id) {
				return true;
			}
		}

		return false;
	}


	// является ли объект предком $by
	public function is_ancestor($by) {

		if (!$this->loaded()) return false;

		$node = self::get($by);

		$path = $node->path();

		// ищем в пути $by появление текущего объекта ($this)
		foreach ($path as $p) {
			if ($p->id == $this->id) {
				return true;
			}
		}

		return false;
	}


	// получение прямых потомков элемента дерева
	// если элемент не загружен, получаем элементы корня
	public function children($order_by='position') {

		$parent_id = $this->loaded() ? $this->id : 0;

		$class = get_called_class();

		$model = new $class();

		if (is_array($order_by) && count($order_by)==2) {
			return $model->where('parent_id','=',$parent_id)->order_by($order_by[0],$order_by[1])->find_all();
		}

		return $model->where('parent_id','=',$parent_id)->order_by($order_by)->find_all();
	}


	// получение полного дерева элемента, если элемент не загружен, то дерево от корня
	// возвращает:
	// массив, где каждый элемент - массив:
	// node: Model_Tree (или потомок) - элемент дерева
	// tree: array - поддерево элементов
	public function tree($order_by='position') {

		$result = array();

		$nodes = $this->children($order_by);

		foreach ($nodes as $node) {

			$el = array();

			$el['node'] = $node;

			if ($node->has_children()) {
				$el['tree'] = $node->tree($order_by);
			}

			$result[] = $el;
		}

		return $result;
	}


	// получение потомков в виде как это возвращает 'tree'
	// т.е. каждый элемент - массив с единственным элементом 'node'
	public function children_tree($order_by='position') {

		$children = $this->children($order_by);

		$tree = array();

		foreach ($children as $node) {
			$tree[] = array('node' => $node);
		}

		return $tree;
	}


	////// получение дерева контент-страниц с выбранным путем в виде массива
	////// путь выбранных элементов задается с помощью $path
	////public static function getPathTree($path, $deep=0) {
	////
	////	if (empty($path) || !isset($path[$deep])) return array();
	////
	////	$el = $path[$deep];
	////
	////	$result = array();
	////
	////	// получаем элементы текущего уровня
	////	$contents = self::children(intval($el->content_id));
	////
	////	foreach ($contents as $content) {
	////
	////		$node = array();
	////
	////		$node['node'] = $content;
	////
	////		// определяем, контент-страница является частью "пути" выделенного элемента?
	////		// т.е. данный элемент должен быть выделен как текущий, каждый на своем уровне
	////		if ($content->id == $el->id) {
	////			if (isset($path[$deep+1])) {
	////				$node['tree'] = self::getPathTree($path, $deep+1);
	////			} else {
	////				$node['tree'] = self::children_tree($el);
	////			}
	////		}
	////
	////		$result[] = $node;
	////	}
	////
	////	return $result;
	////}


	// получение пути
	// от текущего узла до корня или элемента с определенным уровнем
	public function path($level=0) {

		$node = $this;

		$class = get_called_class();

		$result = array();
		$check = array();

		while ($node && $node->loaded() && $node->level>=$level) {

			// если идентификатор уже в массиве, то обнаружилось зацикливание
			if (in_array($node->id,$check)) return array();

			$check[] = $node->id;

			array_unshift($result,$node);

			$parent = new $class($node->parent_id);

			if ($parent->loaded()) {
				$node = $parent;
			} else {
				$node=null;
			}
		}

		return $result;
	}


	// построение дерева для функции path_tree
	private function _path_tree($path,$deep=0) {

		if (empty($path) || !isset($path[$deep])) return array();

		$el = $path[$deep];

		$result = array();

		// получаем сестринские элементы текущего элемента
		$class = get_called_class();
		$siblings = new $class(intval($el->parent_id));
		$siblings = $siblings->children();

		foreach ($siblings as $item) {

			$node = array();

			$node['node'] = $item;

			// если элемент находится в выделенном пути, тогда получаем для него поддерево
			if ($item->id==$el->id) {
				if (isset($path[$deep+1])) {
					$node['tree'] = $this->_path_tree($path,$deep+1);
				} elseif ($item->has_children()) {
					$node['tree'] = $item->children_tree();
				} else {
					$node['tree'] = array();
				}
			}

			$result[] = $node;
		}

		return $result;
	}


	// получение пути в виде дерева - от текущего узла до корня или элемента с определенным уровнем
	// для каждого элемента в пути получаем сестринские элементы, для элементов в пути - поддерево их потомков
	// каждый элемент - массив:
	// 'node' - элемент
	// 'tree' - дерево потомков, если элемент является частью пути
	public function path_tree($level=0) {

		$path = $this->path($level);

		if (empty($path)) return array();

		return $this->_path_tree($path);
	}


	// удаление
	public function delete() {

		if ($this->loaded()) {

			// если есть потомки, их также удаляем
			if ($this->has_children()) {
				$children = $this->children();
				foreach ($children as $child) {
					$child->delete();
				}
			}

			parent::delete();
		}
	}
}
?>
