<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Structure module
 *
 * List Type Image model
 *
 * @package    Structure
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// Модель: элемент-изображение
//

//
// обязательно присутствие в таблице полей:
// 'data' для записи информации об изображении заменяется на более логичное имя поля 'file'
// 'name' 	- наименование изображения или изображений, когда происходит распаковка архива
// 'alias'	- псевдоним наименований
//

abstract class Model_List_Sorted_Type_Image_ORM extends Model_List_Sorted_Type_Item_ORM {

	protected static 	$_config = null,					// раздел конфигурации для работы с данным типом элементов
							$_type = parent::TYPE_IMAGE;	// значение поля 'type' при создании элемента (используется в Model_Content_Item)


	// переопределение наименований полей
	public function __get($name) {

		if ($name=='image') {
			return parent::__get('data');
		}

		return parent::__get($name);
	}

	// переопределение наименований полей
	public function __set($name, $val) {

		if ($name=='image') {
			return parent::__set('data', $val);
		}

		return parent::__set($name, $val);
	}

	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}

	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}


	// загрузка файла изображения
	// $files_upload	- элемент загрузки из массива $_FILES
	// $parent_id		- принадлежность
	// $name 			- имя для файла, комментарий
	public static function load($files_upload,$parent_id,$name) {

		if (empty($parent_id)) {
			throw new Kohana_Exception('Невозможно определить принадлежность изображения для загрузки: $parent_id==0');
		}

		$config = static::$_config;

		if (empty($config)) {
			throw new Kohana_Exception('Невозможно получить конфигурацию для загрузки изображения');
		}

		$loaded = false;

		if (!empty($files_upload) && is_array($files_upload) && isset($files_upload['name'])) {

			$path = DOCROOT . Kohana::$config->load("$config.path");
			$path_zip = DOCROOT . Kohana::$config->load("$config.path_zip");
			$valid_ext = Kohana::$config->load("$config.ext");

			if (empty($path) || empty($path_zip) || empty($valid_ext)) {
				throw new Kohana_Exception("Ошибка конфигурации");
			}

			// обработка эелемента(ов) загружаемого файла
			$keys = array_keys($files_upload);

			// преобразуем одиночную загрузку во множественную
			if (!is_array($files_upload['name'])) {
				foreach ($keys as $k) {
					$files_upload[$k] = array($files_upload[$k]);
				}
			}

			$count_files = count($files_upload['name']);

			for ($ifile=0;$ifile<$count_files;$ifile++) {

				$file = array();
				foreach ($keys as $k) {
					if (isset($files_upload[$k][$ifile])) {
						$file[$k] = $files_upload[$k][$ifile];
					}
				}
				if (empty($file)) continue;

				// обработка файла
				if (Upload::not_empty($file) &&
					 Upload::valid($file) &&
					 Upload::type($file,Kohana::$config->load("$config.upload_ext")) &&
					 ($full_name=Upload::save($file,null,$path))!==FALSE) {

					$file_name = basename($full_name);
					$ext = pathinfo($file_name,PATHINFO_EXTENSION);

					if (strtolower($ext)=='zip') {

						$images = array();

						// очищаем каталог для распаковки
						array_map('unlink',glob($path_zip.'*'));

						// перемещаем файл
						if (rename($full_name,$path_zip.$file_name)) {

							// распаковываем архив
							$za = new ZipArchive();

							if ($za->open($path_zip.$file_name)===TRUE) {

								$loaded = 0;

								for ($i=0;$i<$za->numFiles;$i++) {

									$zf = $za->statIndex($i);
									$ext = strtolower(pathinfo($zf['name'],PATHINFO_EXTENSION));

									// выбираем из архива только изображения
									if (in_array($ext,$valid_ext)) {

										if ($content_file = $za->getFromIndex($i)) {

											// генерация уникального имени файла
											$add_prefix='';
											while ($fn = tempnam($path,"img_zip_{$parent_id}_{$add_prefix}")) {
												if (file_exists("$fn.$ext")) {
													$add_prefix .= date('is');
													continue;
												}
												if (rename($fn,"$fn.$ext")) {
													if (file_put_contents("$fn.$ext",$content_file)!==FALSE) {
														$loaded++;
														$images[] = array (
															'name' => "$name $loaded",
															'image' => basename("$fn.$ext")
														);
													}
												}
												break;
											}
										}
									}
								}
							}
						}

					} else {
						// загрузка одного файла
						$images = array (
							array (
								'name' => $name,
								'image' => $file_name
							)
						);
					}


					$class = get_called_class();
					$model = new $class();

					foreach ($images as $item) {

						$model->clear();

						$model->parent_id = $parent_id;
						$model->name = $item['name'];
						$model->image = $item['image'];
						$model->save();
					}

					$loaded = count($images);
				}
			}
		}

		return $loaded;
	}


	// получение пути к изображению определенного типа
	// $type - раздел в конфигурации, определяющий размеры и расположение изображения
	// $this->_config определяется в конфигурации, как блок параметров:
	// путь до каталога изображений, изображение-заглушка
	public function path($type=null) {

		$config = static::$_config;

		$orig = Kohana::$config->load("$config.path");
		$stub = Kohana::$config->load("$config.stub");

		if (empty($orig) || empty($stub)) {
			throw new Kohana_Exception("Невозможно получить конфигурацию для изображения: '$type'");
		}

		$name = $this->image;

		if (!$this->loaded() || empty($name) || ! is_readable(DOCROOT . "$orig$name")) {
			$name = $stub;
		}

		// проверяем оригинал
		if (is_readable(DOCROOT . "$orig$name")) {

			if (empty($type)) {
				return $orig . $name;
			}

			$path = Kohana::$config->load("$config.$type.path");
			$size = Kohana::$config->load("$config.$type.size");

			if (empty($path)) {
				throw new Kohana_Exception("Невозможно определить путь до изображения типа: '$type'");
			}

			if (empty($size)) {
				throw new Kohana_Exception("Невозможно определить размер изображения типа: '$type'");
			}

			// проверяем наличие каталога, если нет, пытаемся создать
			if (!file_exists(DOCROOT . $path)) {
				if (!mkdir(DOCROOT . $path,0777)) {
					throw new Kohana_Exception("Невозможно создать каталог: " . DOCROOT . $path);
				}
			}

			// проверяем наличие изображения необходимого размера
			if (!file_exists(DOCROOT . "$path$name")) {

				try {

					$image = Image::factory(DOCROOT . "$orig$name");

					if (is_array($size)) {
						$image->resize($size[0],$size[1]);
					} else {
						$image->resize($size,$size);
					}

					$image->save(DOCROOT . "$path$name");

				} catch (Kohana_Exception $e) {

					// при ошибке обработки изображения - удаляем его, возвращаем заглушку
					$this->delete(array('type','='));

					return $this->path($type);
				}
			}

		} else {
			//throw new Kohana_Exception("Невозможно получить оригинал изображения: " . DOCROOT . "$orig$name");
			return false;
		}

		return $path . $name;
	}


	// удаление подготовленных изображений
	public function delete_prepared_images() {

		if ($this->loaded()) {

			$config = static::$_config;

			if (empty($config)) {
				throw new Kohana_Exception('Невозможно получить конфигурацию для удаления подготовленных изображении');
			}

			$items = Kohana::$config->load("$config");

			if (empty($items)) {
				throw new Kohana_Exception('Невозможно получить конфигурацию для удаления подготовленных изображений: типы изображений');
			}

			// удаляем все подготовленные изображения
			foreach ($items as $item) {

				if (is_array($item) && isset($item['path'])) {
					@unlink(DOCROOT . $item['path'] . $this->image);
				}
			}
		}
	}


	// удаление изображения
	public function delete() {

		if ($this->loaded()) {

			$config = static::$_config;

			if (empty($config)) {
				throw new Kohana_Exception('Невозможно получить конфигурацию для удаления изображения');
			}

			$orig = Kohana::$config->load("$config.path");

			if (empty($orig)) {
				throw new Kohana_Exception('Невозможно получить конфигурацию для удаления изображения: оригинал');
			}

			// удаляем оригинал изображения
			@unlink(DOCROOT . $orig . $this->image);

			// удаляем все подготовленные изображения
			$this->delete_prepared_images();

			//$items = Kohana::$config->load("$config");
			//
			//if (empty($items)) {
			//	throw new Kohana_Exception('Невозможно получить конфигурацию для удаления изображений: типы изображений');
			//}
			//
			//// удаляем все подготовленные изображения
			//foreach ($items as $item) {
			//
			//	if (is_array($item) && isset($item['path'])) {
			//		@unlink(DOCROOT . "{$item['path']}$image");
			//	}
			//}

			parent::delete();
		}
	}
}
