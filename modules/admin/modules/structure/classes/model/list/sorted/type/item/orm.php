<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Structure module
 *
 * List_Type_Item model
 *
 * @package    Structure
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// базовый класс для типизированного списка
//

abstract class Model_List_Sorted_Type_Item_ORM extends Model_List_Sorted_ORM {

	const TYPE_UNKNOWN	= 0,
			TYPE_IMAGE		= 1, // изображение
			TYPE_FILE		= 2, // файл
			TYPE_PARAM		= 3, // параметр
			TYPE_GROUP		= 4; // группа параметров

	protected static $_type = self::TYPE_UNKNOWN; // тип элемента


	// добавление элемента - установка типа контента
	// дополнительно устанавливается условие для оганичения
	// группы по типу, чтобы сортировка при добавлении производилась
	// в рамках элементов одного типа
	public function create(Validation $validation = NULL) {

		$this->type = static::$_type;

		static::$_where = array('type','=',static::$_type);

		return parent::create($validation);
	}


	// получение, установка типа элемента
	public static function type($type=null) {

		if ($type!==null && is_numeric($type)) {
			static::$_type = $type;
		}

		return static::$_type;
	}


	// получение элемента, по-умолчанию - первый
	public static function element($parent_id,$position=1) {

		$class = get_called_class();

		$model = new $class();

		$el = $model
					->where('parent_id','=',$parent_id)
					->and_where('type','=',static::$_type)
					->and_where('position','=',$position)
					->find();

		return $el;
	}


	// получение элементов
	public static function elements($parent_id,$order_by='position') {

		$class = get_called_class();

		$model = new $class();

		if (is_array($order_by) && count($order_by)>=2) {
			$els = $model
						->where('parent_id','=',$parent_id)
						->and_where('type','=',static::$_type)
						->order_by($order_by[0],$order_by[1])
						->find_all();
		} else {
			$els = $model
						->where('parent_id','=',$parent_id)
						->and_where('type','=',static::$_type)
						->order_by($order_by)
						->find_all();
		}

		return $els;
	}


	// перемещение
	// $point перекрывается, поэтому дальше в наследовании можно игнорировать
	public static function move($drag_id,$drop_id,$point=null) {

		$class = get_called_class();

		$drag = new $class($drag_id);	// перемещаемый элемент
		$drop = new $class($drop_id);	// элемент, на место которого перемещается

		if (!($drag->loaded() && $drop->loaded())) {
			throw new Kohana_Exception('Элементы для перемещения не найдены');
		}

		if ($drag->type!=static::$_type || $drop->type!=static::$_type) {
			throw new Kohana_Exception('Элементы для перемещения не соответствую типу модели');
		}

		// $point перекрывается, поэтому дальше в наследовании можно игнорировать
		$point = $drag->position>$drop->position ? 'above' : 'below';

		// устанавливаем дополнительное условие для перемещения в группе определенного
		// типа элементов
		static::$_where = array('type','=',static::$_type);

		parent::move($drag_id,$drop_id,$point);
	}


	// удаление
	public function delete() {

		// устанавливаем дополнительное условие для последующей пересортировки
		// с учетом типа элемента
		static::$_where = array('type','=',static::$_type);

		parent::delete();
	}
}
?>
