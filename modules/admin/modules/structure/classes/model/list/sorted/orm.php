<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Model List_Sorted_ORM
 *
 * @package    Structure
 * @category   Model
 * @module		List_Sorted
 * @author     ESV Corp. since 10.2011
 */

// отсортированный список, возможно пренадлежащий какой-то группе (parent_id)
// предполагает обязательное наличие полей структуры:
// id				INT UNSIGNED
// parent_id	INT UNSIGNED
// position		INT UNSIGNED

abstract class Model_List_Sorted_ORM extends ORM {

	// условие для дополнительной сортировки
	// может использоваться в классах-потомках
	// дополнительное условие при перемещении и
	// пересортировке элементов, когда элементы
	// сортированного списка могут еще группироваться
	// по типам, используется далее в List_Type_Item_ORM
	protected static $_where = null;


	// получение элемента
	// элемент может быть указан:
	// id
	// array - условие для выборки
	// object - элемент данной модели или потомок
	public static function get($by) {

		if (empty($by)) $by=0;

		$node = false;

		$class = get_called_class();

		if (is_numeric($by)) {
			$node = new $class(array('id'=>$by));
		} elseif (is_string($by)) {
			$node = new $class(array('alias'=>$by));
		} elseif (is_array($by)) {
			$node = new $class($by);
		} elseif (is_object($by)) {
			if (is_a($by,$class)) {
				$node = $by;
			}
		}

		return $node;
	}


	// добавление элемента - вычисление позиции в группе
	public function create(Validation $validation = NULL) {

		$parent_id = empty($this->parent_id) ? 0 : $this->parent_id;

		$class = get_called_class();

		$model = new $class();

		$where = static::$_where;
		if (!empty($where) && is_array($where) && count($where)==3) {
			$model->where($where[0],$where[1],$where[2]);
		}

		// получаем значение позиции для нового потомка данного элемента
		$position = $model->where('parent_id','=',$parent_id)->count_all()+1;

		$this->position = $position;

		return parent::create($validation);
	}


	// получение родителя
	// если корневой элемент (parent_id==0), тогда возвращается незагруженный элемент
	public function get_parent() {

		$class = get_called_class();

		if ($this->loaded()) {
			$model = new $class(array('id','=',$this->parent_id));
		} else {
			$model = new $class();
		}

		return $model;
	}


	// пересортировка всех дочерних элементов
	public static function resort($parent_id) {

		$class = get_called_class();

		$model = new $class();

		$where = static::$_where;
		if (!empty($where) && is_array($where) && count($where)==3) {
			$model->where($where[0],$where[1],$where[2]);
		}

		$nodes = $model->where('parent_id','=',$parent_id)
			->order_by('position')
			->find_all();

		$position = 1;
		foreach ($nodes as $node) {
			if ($node->position!=$position) {
				$node->position = $position;
				$node->save();
			}
			$position++;
		}
	}


	// перемещение элементов в одном уровне
	// $move_id 			- перемещаемый элемент
	// $destination_id	- элемент, на который перемещается
	// $point				- точка перемещения: 'above' - над $destination_id, 'below' - под $destination_id
	public static function move($move_id,$destination_id,$point=null) {

		if (empty($move_id) || empty($destination_id) || empty($point) || ($point!='above' && $point!='below')) {
			return false;
		}

		$class = get_called_class();

		$move = new $class($move_id);
		$destination = new $class($destination_id);

		if (!$move->loaded() || !$destination->loaded() || $move->parent_id!=$destination->parent_id) {
			return false;
		}

		// получаем "родственников" - потомков того же родителя, что и у перемещаемых элементов
		$model = new $class();

		$where = static::$_where;
		if (!empty($where) && is_array($where) && count($where)==3) {
			$model->where($where[0],$where[1],$where[2]);
		}

		$elements = $model->where('parent_id','=',$move->parent_id)->order_by('position')->find_all();

		$position = 1;
		$positions = array();

		foreach ($elements as $el) {

			// перемещаемый элемент исключаем, т.к. он будет добавлен позже при вставке
			if ($el->id==$move->id) continue;

			// найден перемещаемый элемент
			if ($el->id==$destination->id) {

				// поместить над элементом
				if ($point=='above') {
					$positions[$move->id] = $position++;
					$positions[$destination->id] = $position++;
				} else { // под элементом
					$positions[$destination->id] = $position++;
					$positions[$move->id] = $position++;
				}
				continue;
			}

			$positions[$el->id] = $position++;
		}

		// записываем новый порядок
		foreach ($elements as $el) {
			if ($el->position!=$positions[$el->id]) {
				$el->position = $positions[$el->id];
				$el->save();
			}
		}

		return true;
	}


	// удаление элемента
	public function delete() {

		if ($this->loaded()) {

			$parent_id = $this->parent_id;

			parent::delete();

			self::resort($parent_id);
		}
	}
}
?>
