<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Structure module
 *
 * List Type File model
 *
 * @package    Structure
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// Модель: элемент-файл
//

//
//	обязательно присутствие в таблице полей:
// 'data' для записи информации об изображении заменяется на более логичное имя поля 'file'
// 'name' 	- наименование файла или файлов, когда происходит распаковка архива
// 'alias'	- псевдоним наименований
//

abstract class Model_List_Sorted_Type_File_ORM extends Model_List_Sorted_Type_Item_ORM {

	protected static 	$_config = null,					// раздел конфигурации для работы с данным типом элементов
							$_type = parent::TYPE_FILE;	// значение поля 'type' при создании элемента (используется в Model_Content_Item)


	// переопределение наименований полей
	public function __get($name) {

		if ($name=='file') {
			return parent::__get('data');
		}

		return parent::__get($name);
	}

	// переопределение наименований полей
	public function __set($name, $val) {

		if ($name=='file') {
			return parent::__set('data', $val);
		}

		return parent::__set($name, $val);
	}


	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}

	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}


	// размер файла в виде строки
	public function size_str() {

		if ($this->loaded() && ($path=$this->path())) {

			$size = filesize(DOCROOT . $path);

			if ($size>=1024*1024) { // мегабайты
				return number_format(round($size/(1024*1024),1),1,'.','') . ' Mb';
			} elseif ($size>=1024) { // килобайты
				return number_format(round($size/1024,1),1,'.','') . ' Kb';
			} else { // байты
				return $size;
			}
		}
	}


	// загрузка файла
	// $files_upload	- элемент загрузки из массива $_FILES
	// $parent_id		- принадлежность
	// $name 			- имя для файла, комментарий
	public static function load($files_upload,$parent_id,$name) {

		if (empty($parent_id)) {
			throw new Kohana_Exception('Невозможно определить принадлежность файла для загрузки: $parent_id==0');
		}

		$config = static::$_config;

		if (empty($config)) {
			throw new Kohana_Exception('Невозможно получить конфигурацию для загрузки файла');
		}

		$loaded = false;

		if (!empty($files_upload) && is_array($files_upload) && isset($files_upload['name'])) {

			// для каждого контента ($parent_id) свой подкаталог
			$path = DOCROOT . Kohana::$config->load("$config.path") . $parent_id . DIRECTORY_SEPARATOR;
			$path_zip = DOCROOT . Kohana::$config->load("$config.path_zip");
			$valid_ext = Kohana::$config->load("$config.ext");
			// возможно обозначение '*' - для загрузки без ограничений
			$upload_valid_ext = Kohana::$config->load("$config.upload_ext");
			if (empty($upload_valid_ext)) $upload_valid_ext='*';

			if (empty($path) || empty($path_zip) || empty($valid_ext)) {
				throw new Kohana_Exception("Ошибка конфигурации");
			}

			// для каждого контента отдельный каталог хранения файлов, id контента (parent_id)
			if (file_exists($path)) {
				if (!(is_dir($path) && is_writable($path))) {
					throw new Kohana_Exception("Каталог '$path' недоступен для записи");
				}
			} else {
				if (mkdir($path,0777)==FALSE) {
					throw new Kohana_Exception("Не могу создать каталог '$path'");
				}
			}

			// обработка эелемента(ов) загружаемого файла
			$keys = array_keys($files_upload);

			// преобразуем одиночную загрузку во множественную
			if (!is_array($files_upload['name'])) {
				foreach ($keys as $k) {
					$files_upload[$k] = array($files_upload[$k]);
				}
			}

			$count_files = count($files_upload['name']);

			for ($ifile=0;$ifile<$count_files;$ifile++) {

				$file = array();
				foreach ($keys as $k) {
					if (isset($files_upload[$k][$ifile])) {
						$file[$k] = $files_upload[$k][$ifile];
					}
				}
				if (empty($file)) continue;

				// обработка файла
				if (Upload::not_empty($file) &&
					 Upload::valid($file) &&
					 ($upload_valid_ext=='*' || Upload::type($file,$upload_valid_ext)) &&
					 ($original_name = Arr::get($file,'name')) &&
					 ($full_name=Upload::save($file,$original_name,$path))!==FALSE) {

					$file_name = basename($full_name);
					$ext = pathinfo($file_name,PATHINFO_EXTENSION);

					// создаем объект текущей модели
					$class = get_called_class();
					$model = new $class();

					if (strtolower($ext)=='zip' && Arr::get($_POST,'unpack',false)) {

						$files = array();

						// очищаем каталог для распаковки архива
						array_map('unlink',glob($path_zip.'*'));

						// перемещаем файл
						if (rename($full_name,$path_zip.$file_name)) {

							// распаковываем архив
							$za = new ZipArchive();

							if ($za->open($path_zip.$file_name)===TRUE) {

								// проверяем кодировку для имен Windows
								$encodings = mb_list_encodings();

								if (!in_array('CP866',$encodings)) {
									throw new Kohana_Exception("Encoding 'CP866' not found in system");
								}

								for ($i=0;$i<$za->numFiles;$i++) {

									$zf = $za->statIndex($i);
									$ext = strtolower(pathinfo($zf['name'],PATHINFO_EXTENSION));
									$fn = basename($zf['name']);

									//// выбираем из архива только допустимые типы файлов
									//// если указано без ограничений, тогда все
									if ($valid_ext=='*' || in_array($ext,$valid_ext)) {

										$fn = trim(iconv('CP866','UTF-8',$fn));
										$fn = str_replace(' ','_',$fn);

										// проверка на существование такого файла
										// запрет добавления дублирующихся имен, просто пропускаем
										$model->clear();
										$check = $model->where('parent_id','=',$parent_id)->where('type','=',static::$_type)->where('data','=',$fn)->find();

										if ($check->loaded()) continue;

										if ($content_file = $za->getFromIndex($i)) {

											if (file_put_contents($path . $fn,$content_file)!==FALSE) {
												$files[] = array (
													'name' => $name,
													'file' => $fn
												);
											}
										}
									}
								}
							}
						}

					} else {

						// проверка на существование такого файла
						// запрет добавления дублирующихся имен
						$model->clear();
						$check = $model->where('parent_id','=',$parent_id)->where('type','=',static::$_type)->where('data','=',$file_name)->find();
						if (!$check->loaded()) {
							$files = array (
								array (
									'name' => $name,
									'file' => $file_name
								)
							);
						} else { $files = array(); }
					}

					foreach ($files as $file) {

						$model->clear();

						$model->parent_id = $parent_id;
						$model->name = $file['name'];
						$model->file = $file['file'];
						$model->save();
					}

					$loaded = count($files);
				}
			}
		}

		return $loaded;
	}


	// получение пути к изображению определенного типа
	public function path() {

		if ($this->loaded()) {

			$config = static::$_config;

			if (empty($config)) {
				throw new Kohana_Exception('Не могу определить концигурацию для работы с файлом');
			}

			$path = Kohana::$config->load("$config.path") . $this->parent_id . DIRECTORY_SEPARATOR;
			$name = $this->file;

			// проверяем доступность файла
			if (is_readable(DOCROOT . "$path$name")) {

				return $path . $name;
			}
		}

		return false;
	}


	// удаление файла
	public function delete() {

		if ($this->loaded()) {

			$config = static::$_config;

			if (empty($config)) {
				throw new Kohana_Exception('Не могу определить концигурацию для работы с файлом');
			}

			$path = Kohana::$config->load("$config.path") . $this->parent_id . DIRECTORY_SEPARATOR;

			$file = $this->file;

			// удаляем сам файл
			@unlink(DOCROOT . "$path$file");

			// попытка удалить каталог, если там файлов нет то удаление пройдет успешно
			@rmdir(DOCROOT . $path);

			parent::delete();
		}
	}
}
