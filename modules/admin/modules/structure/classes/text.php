<?php

defined('SYSPATH') or die('No direct script access.');

class Text extends Kohana_Text {

	/**
	 * Translitirate text from ru to en
	 * @param string $string
	 * @return string
	 */

   public static function translit($string) {
      $tbl = array(
			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ж' => 'zh', 'з' => 'z',
			'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p',
			'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',	'ы' => 'i', 'э' => 'e', 'А' => 'A',
			'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
			'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
			'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Ы' => 'I', 'Э' => 'E', 'ё' => "yo", 'х' => "h",
			'ц' => "ts", 'ч' => "ch", 'ш' => "sh", 'щ' => "shch", 'ъ' => "tz", 'ь' => "mz", 'ю' => "yu", 'я' => "ya",
			'Ё' => "YO", 'Х' => "H", 'Ц' => "TS", 'Ч' => "CH", 'Ш' => "SH", 'Щ' => "SHCH", 'Ъ' => "TZ", 'Ь' => "MZ",
			'Ю' => "YU", 'Я' => "YA", " " => "_", "-" => "_"
		);
      $text = strtr(trim($string), $tbl);
      $text = preg_replace('/[\W\s]+/', '', $text);
      $text = preg_replace('/\_+/', '_', $text);
		$text = trim($text,' _');
      return strtolower($text);
   }

	// замена спецсимволов
	public static function symbols($string) {

		$symbols = array('.',',',':',';','/','\\','#','$','@','!','%','&','?','*','(',')','+','=','\'','"','<','>','`');

		$string = str_replace($symbols,' ',$string);
		$string = preg_replace('/\s+/',' ',$string);
		$string = trim($string);

		return $string;
	}

   public static function logging($text) {
      if (is_array($text) or is_object($text)) {
         $text = print_r($text, 1);
      }
      $fh = fopen('test.log', 'a+');
      fwrite($fh, $text . "\n");
      fclose($fh);
   }

   public static function clear_justify($text) {
      return str_replace('text-align: justify;', '', $text);
   }
}
