<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Tree
 *
 * Tree template
 * Используется для отрисовки блока скрипта добавления элемента в дерево
 * использует JavaScript-ы в DOCROOT/include/lib/ajax/, DOCROOT/include/lib/ajax/tree
 * подключаются в контроллере с использованием config
 *
 * @package    Structure
 * @module		Tree
 * @category   Template (см. Tree::content_add_node)
 * @author     ESV Corp. since 08.2011
 */
?>
<script language="javascript">
<!--
	if (window['<?php print $tree_var_name; ?>'] && (typeof window['<?php print $tree_var_name; ?>']['add_node'] == 'function')) {
		<?php print $tree_var_name; ?>.add_node(<?php print $add_node_id; ?>);
	}
-->
</script>
