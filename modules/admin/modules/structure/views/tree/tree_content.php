<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Tree
 *
 * Tree template
 * Используется для отрисовки блоков с использованием дерева
 * использует JavaScript-ы в DOCROOT/include/lib/ajax/, DOCROOT/include/lib/ajax/tree
 * подключаются в контроллере с использованием config
 *
 * @package    Structure
 * @module		Tree
 * @category   Template of Tree
 * @author     ESV Corp. since 08.2011
 */
?>

<div class="__ajax_tree_body__">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" cols="2">
		<tr>
			<td align="left" valign="top">
				<div class="__ajax_tree_tree__" id="<?php print $tree_block_id; ?>">
				</div>
			</td>
			<td align="left" valign="top">
				<div class="__ajax_tree_content__" id="<?php print $content_block_id; ?>">
				</div>
			</td>
		</tr>
	</table>
</div>

<script language="javascript">
<!--

	var <?php print $tree_var_name; ?>;

	// непременно используем ExtJS, т.к. дерево работает на нем
	Ext.onReady(function(){

		// дерево
		__AJAX_TREE = __TREE_init(
			<?php print $root_id; ?>,
			'<?php print $root_name; ?>',
			<?php print $width; ?>,
			<?php print $height; ?>,
			'<?php print $dataUrl; ?>',
			'<?php print $getUrl; ?>',
			'<?php print $addUrl; ?>',
			'<?php print $delUrl; ?>',
			'<?php print $moveUrl; ?>',
			'<?php print $tree_id; ?>',
			'<?php print $tree_block_id; ?>'
		);

		// ajax навигация
		initAjaxNavigation('DIV#<?php print $content_block_id; ?>');

		// установка обработчика для действий после submit форм
		setAjaxPostSubmit(function(){ <?php print $tree_var_name; ?>.reload_tree(); });

	});
-->
</script>
