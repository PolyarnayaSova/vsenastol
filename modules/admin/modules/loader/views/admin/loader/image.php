<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Image(s) Load template
 *
 * @package    Admin/Loader
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */

$form = Route::url(Admin::route(),array('action'=>'loadimage'));

$max_files = ini_get('max_file_uploads');

?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST','enctype'=>'multipart/form-data')); ?>

	<div class="form">

		<div class="caption">Загрузить фото товаров</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Файл</div>
					<?php	print Form::file('images[]',array('title'=>"Доступные форматы: jpg, jpeg, png, gif, zip (архив фото: jpg, jpeg, png, gif). Одновременно для загрузки не более: $max_files файлов",'multiple'=>'multiple')); ?>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('load','загрузить'); ?>
	</div>

	<?php print Form::close(); ?>

</div>
