<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Load Price template
 *
 * @package    Admin/Loader
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */

$form = Route::url(Admin::route(),array('action'=>'loadprice2'));

?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST','enctype'=>'multipart/form-data')); ?>

	<div class="form">

		<div class="caption">Загрузить дополнительные прайсы</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Прайс</div>
					<?php print Form::select('category_id', $categories); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Файл</div>
					<?php print Form::file('price',array('title'=>"В формате экспорта файла Excel - CSV")); ?>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('load','загрузить'); ?>
	</div>

	<?php print Form::close(); ?>

</div>
