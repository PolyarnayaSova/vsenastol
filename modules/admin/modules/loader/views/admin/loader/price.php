<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Load Price template
 *
 * @package    Admin/Loader
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */

$form = Route::url(Admin::route(),array('action'=>'loadprice'));

?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST','enctype'=>'multipart/form-data')); ?>

	<div class="form">

		<div class="caption">Загрузить прайс</div>

		<fieldset>

			<div class="field">

				<label>
					<div class="label">Поставщик</div>
					<?php print Form::select('supplier_id', $suppliers, 0); ?>
				</label>

				<label>
					<div class="label">Файл</div>
					<?php print Form::file('price',array('title'=>"В формате экспорта файла Excel - CSV")); ?>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('load','загрузить'); ?>
	</div>

	<?php print Form::close(); ?>

</div>
