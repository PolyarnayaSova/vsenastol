<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 *
 * Загрузка прайса и фотографий товаров
 *
 * @package    Admin/Loader
 * @category   Module
 * @author     ESV Corp. since 07.2011
 *
 */

class Loader_Exception extends Exception {

}

class Controller_Admin_Loader extends Controller_Admin {

	const START_PRICE = 3; // строка начала прайса (начиная с 0)

	const ADD_PRICE_PREFIX_MUL = 1000000; // множитель для префикса кода товара дополнительных прайсов
	const ADD_PRICE_ARTICLE_PREFIX = '000-'; // префикс для артикулов дополнительных прайсов

	const MAIN = 0;
	const RP = 1;
	const EVROPLASTIK = 2;
	const RC_VOSTOK = 3;
	const KOLPAKOV = 4;
	const MARKIK = 5;
	const OVELON = 6;
	const SERVIS_KLUCH = 7;

	// параметры для дополнительных прайсов
	protected static $add_price_categories = array (
																4859 => '',
																4860 => '',
																4861 => ''
															);

	protected static $add_price_prefixes = array (
																4859 => 1,
																4860 => 2,
																4861 => 3
															);

	protected static $suppliers = array (
		self::MAIN => 'Основной',
		//self::RP => 'Русские подарки'
		self::EVROPLASTIK => 'Европластик',
		self::RC_VOSTOK => 'РЦ Восток',
		self::KOLPAKOV => 'Колпаков',
		self::MARKIK => 'Маркик',
		self::OVELON => 'Овелон',
		self::SERVIS_KLUCH => 'Сервис ключ',
	);

	public static function getSupplierNameById($id){
		$arr = self::$suppliers;
		if ($arr[$id]) {
			return $arr[$id];
		} else {
			return "Не удалось установить поставщика";
		}

	}

	public function action_index() {

		$this->content = View::factory('admin/loader/loader');
	}


	// страница загрузки прайса
	public function action_price() {

		$this->content =
			View::factory('admin/loader/price')
				->set('suppliers', self::$suppliers);

	}


	// страница загрузки дополнительных прайсов
	public function action_price2() {

		// id категорий для загрузки прайсов
		$categories = self::$add_price_categories;

		$category = new Model_Product_Category();

		foreach ($categories as $id => $v) {

			$category->clear();

			$c = $category->where('id','=',$id)->find();

			if ( ! $c->loaded()) {
				throw new HTTP_Exception_400("Не найдена категория для загрузки прайса '{$id}'");
			}

			$categories[$id] = $c->name;
		}

		$this->content =
			View::factory('admin/loader/price2')
				->set('categories', $categories);
	}


	// загрузка прайса
	public function action_loadprice() {
		ini_set("memory_limit","256M");
		$warning_msg = '';
		$warning_strings = array();

		if ($this->request->method()!='POST') {
			throw new HTTP_Exception_400('Недопустимый запрос');
		}

		$price = Arr::get($_FILES,'price',array());
		$supplier_id = Arr::get($_POST, 'supplier_id', null);

		if (empty($price) || ! Upload::not_empty($price) || $supplier_id === null) {

			Admin::message('Файл не указан или не указан поставщик',Admin::MSG_WARNING);

		} else {

			$path = Kohana::$config->load("loader.path");
			if (empty($path)) {
				throw new Kohana_Exception("Ошибка конфигурации");
			}

			// очищаем каталог для загрузки
			@array_map('unlink',glob($path . '*'));

			if (Upload::valid($price) && Upload::type($price,array('csv')) && Upload::save($price,'price.csv',$path) !== FALSE) {

				$price = $path . 'price.csv';

				$product = new Model_Product();

				$product_images_path = Kohana::$config->load('product.product.image.path');
				if (empty($product_images_path)) {
					throw new Kohana_Exception("Ошибка конфигурации - не найден путь для изображений товаров");
				}
				$product_images_path = DOCROOT . $product_images_path;

				$prepared_images_path = Kohana::$config->load('loader.prepared_images_path');
				if (empty($prepared_images_path)) {
					throw new Kohana_Exception("Ошибка конфигурации - не найден путь для подготовленных изображений товаров");
				}

				$ext_images = Kohana::$config->load('product.product.image.ext');
				if (empty($ext_images)) {
					throw new Kohana_Exception("Ошибка конфигурации - не найдены типы изображений");
				}

				try {

					// перекодируем прайс
					if (($str=file_get_contents($price))===FALSE) {
						throw new Loader_Exception('Ошибка перекодирования прайса: чтение');
					}

					$str = iconv('Windows-1251','UTF-8',$str);

					if (file_put_contents($price,$str)===FALSE) {
						throw new Loader_Exception('Ошибка перекодирования прайса: запись');
					}

					if (($price=@fopen($price,'r'))===FALSE) {
						throw new Loader_Exception('Ошибка открытия файла прайса');
					}

					// пропускаем заголовок
					for ($i=0;$i<self::START_PRICE;$i++) {
						$csv = fgetcsv($price,0,';');
					}

					$line_num = 0;

					$category_hash = array();

					$added_products = 0;

					// обнуляем остатки у товаров
					DB::update('products')->set(array('rest'=>'0'))->where('supplier_id','=',$supplier_id)->execute();

					while (true) {

						$csv = fgetcsv($price,0,';');
						if ($csv === FALSE) break; // выход из цикла

						$line_num++;

						$values = array();

						$values['supplier_id'] = $supplier_id;

						// наименование товара
						if (preg_match('/(.*?)(\(.*?\d+\/\d+.*?\))/',$csv[1])) {
							$values['name'] = trim(preg_replace('/(.*?)(\(.*?\d+\/\d+.*?\))/',"$1",$csv[1]));
						} elseif (preg_match('/(.*?)(\(.*?\d+.*?\))/',$csv[1])) {
							$values['name'] = trim(preg_replace('/(.*?)(\(.*?\d+.*?\))/',"$1",$csv[1]));
						} else {
							$values['name'] = trim($csv[1]);
						}

						if (empty($values['name'])) continue;

						// категория и подкатегория
						$sub_category = trim($csv[11]);
						$category = trim($csv[12]);

						// для "Русские подарки обработка наименования подкатегории"
						if ($supplier_id == self::RP) {
							if (preg_match('/\d+\/\d+\s*\-\s*.+/',$sub_category)) {
								$sub_category = trim(preg_replace('/\d+\/\d+\s*\-\s*(.+)/',"$1",$sub_category));
							} elseif (preg_match('/\d+\s*\-\s*.+/',$sub_category)) {
								$sub_category = trim(preg_replace('/\d+\s*\-\s*(.+)/',"$1",$sub_category));
							}
						}

						$sub_category_alias = Text::translit($sub_category);
						$category_alias = Text::translit($category);

						if (isset($category_hash[$category_alias][$sub_category_alias])) {

							$sub_category_id = $category_hash[$category_alias][$sub_category_alias];

						} else {

							if (empty($category) || empty($sub_category)) {
								$warning_msg .= 'не указана категория или номенклатурная группа в строке ' . (self::START_PRICE+$line_num) . "\n";
								$warning_strings[] = self::START_PRICE+$line_num;
								continue;
							}

							// вычисляем id категории и подкатегории
							$category_id = Model_Product_Category::get_id_by_name($category);

							if ($category_id == 0) {
								$warning_msg .= 'невозможно определить номенклатурную группу в строке ' . (self::START_PRICE+$line_num) . "\n";
								$warning_strings[] = self::START_PRICE + $line_num;
								continue;
							}

							$sub_category_id = Model_Product_Category::get_id_by_name($sub_category,$category_id);

							if ($sub_category_id == 0) {
								$warning_msg .= 'невозможно определить категорию в строке ' . (self::START_PRICE+$line_num) . "\n";
								$warning_strings[] = self::START_PRICE + $line_num;
								continue;
							}

							$category_hash[$category_alias][$sub_category_alias] = $sub_category_id;
						}

						$values['category_id'] = $sub_category_id;

						// остаток
						if (!empty(trim($csv[2]))) {
							$values['rest'] = str_replace(',', '.', $csv[2]);
						}

						// артикул
						if (!empty(trim($csv[3]))) {
							$values['article'] = trim($csv[3]);
						}
						// уникальный код
						if ($supplier_id == self::RP) {
							//$values['code'] = intval(preg_replace('/(\d+)/',"$1",$csv[4])) + 100000000;
							$values['code'] = trim($csv[4]);

                            if ($values['code'] == 100000000) continue;

						} else {
							$values['code'] = trim($csv[4]);
							if (empty($values['code'])) {
								continue;
							}
							//$values['code'] = intval(preg_replace('/(\D+)(.*)/',"$2",$csv[4]));
						}

						// описание
						$csv[5] = trim($csv[5]);
						if (!empty(trim($csv[5]))) {
							$values['text'] = preg_replace('/;\s*/',"\n",$csv[5]);
						}

						// штрихкод
						if (!empty(trim($csv[6]))) {
							$values['barcode'] = trim($csv[6]);
						}
						// вес
						if (!empty(trim($csv[7]))) {
							$values['weight'] = floatval(str_replace(',', '.', $csv[7]));
						}
						// объем
						if (!empty(trim($csv[8]))) {
							$values['capacity'] = floatval(str_replace(',', '.', $csv[8]));
						}
						// бренд
						if (!empty(trim($csv[9]))) {
							$values['brand_id'] = Model_Product_Brand::get_id_by_name(trim($csv[9]));
						}
						// материал
						if (!empty(trim($csv[10]))) {
							$values['material_id'] = Model_Product_Material::get_id_by_name(trim($csv[10]));
						}
						// цена оптовая
						if (!empty(trim($csv[13]))) {
							$values['opt_price'] = floatval(str_replace(',', '.', str_replace(' ', '', str_replace('руб.', '', $csv[13]))));
						}
						// ед.измерения опт
						if (!empty(trim($csv[14]))) {
							$values['opt_unit'] = trim($csv[14]);
						}
						// цена
						if (!empty(trim($csv[15]))) {
							$values['price'] = floatval(str_replace(',', '.', str_replace(' ', '', str_replace('руб.', '', $csv[15]))));
						}
					/*	if ($values['price'] <= $values['opt_price']) {
							$values['price'] = $values['price'] * 1.15;
						}*/

						// ед.измерения
						if (isset($csv[16])) {
							$values['unit'] = trim($csv[16]);
						}

						if (empty($values['unit'])) {
							$values['unit'] = $values['opt_unit'];
						}


						$product->clear();
						$product = $product->where('code','like',$values['code'])->where('supplier_id','=',$supplier_id)->find();


						// не перезаписываем наименование и описание
						if ($product->loaded()) {
							unset($values['name']);
							unset($values['text']);
							// даже для существующего товара может быть возможен поиск изображения
							$count_images = $product->images->count_all();
							$values['check_image'] = ($count_images==0 ? 1 : 0);

						} else {
							$values['new'] = 1;
							$values['check_image'] = 1;
							$added_products++;
						}

						$product->values($values);

						$product->save();
					}

					// для товаров поиск подготовленных изображений
					$added_images = 0;

					$product = new Model_Product();
					$products = $product->where('check_image','=',1)->find_all();

					$image = new Model_Product_Image();

					foreach ($products as $p) {

						// поиск подготовленных изображений
						$name_search = trim($p->code);

						foreach ($ext_images as $ext) {
							foreach (glob($prepared_images_path . "$name_search.$ext") as $fname) {

								$fname_base = basename($fname);
								$fname_destination = $product_images_path . $fname_base;

								@unlink($fname_destination);

								if (copy($fname,$fname_destination)) {

									$image->clear();

									$image->image = $fname_base;
									$image->parent_id = $p->id;
									$image->save();

									$added_images++;
								}
							}
						}

						$p->check_image = 0;
						$p->save();
					}

					$loaded_string = "\nОработано строк: $line_num\nНовых товаров: $added_products, изображений: $added_images";

					if (empty($warning_strings)) {

						Admin::message("Прайс успешно загружен" . $loaded_string, Admin::MSG_OK);

					} else {

						// Admin::message("Прайс загружен c предупреждениями:\n" . $warning_msg . $loaded_string, Admin::MSG_WARNING);
						Admin::message("Прайс загружен c предупреждениями.\n" . $loaded_string, Admin::MSG_WARNING);

					}

				} catch (Loader_Exception $e) {

					Admin::message($e->getMessage(), Admin::MSG_ERROR);

				}

			} else {

				Admin::message('Ошибка загрузки прайса', Admin::MSG_ERROR);
			}
		}

		return self::action_price();
	}


	// загрузка дополнительного прайса
	public function action_loadprice2() {

		$warning_msg = '';
		$warning_strings = array();

		if ($this->request->method()!='POST') {
			throw new HTTP_Exception_400('Недопустимый запрос');
		}

		$category_id = Arr::get($_POST, 'category_id', 0);

		$category = new Model_Product_Category($category_id);

		if ( ! $category->loaded()) {

			Admin::message('Ошибка загрузки прайса: не верно указана категория', Admin::MSG_ERROR);

			return self::action_price2();
		}

		$price = Arr::get($_FILES,'price',array());

		if (empty($price) || ! Upload::not_empty($price)) {

			Admin::message('Файл не указан', Admin::MSG_WARNING);

		} else {

			$path = Kohana::$config->load("loader.path");

			if (empty($path)) {
				throw new Kohana_Exception("Ошибка конфигурации");
			}

			// очищаем каталог для загрузки
			@array_map('unlink',glob($path . '*'));

			if (Upload::valid($price) && Upload::type($price,array('csv')) && Upload::save($price,'price.csv',$path)!==FALSE) {

				$price = $path . 'price.csv';

				$product = new Model_Product();

				$product_images_path = Kohana::$config->load('product.product.image.path');
				if (empty($product_images_path)) {
					throw new Kohana_Exception("Ошибка конфигурации - не найден путь для изображений товаров");
				}
				$product_images_path = DOCROOT . $product_images_path;

				$prepared_images_path = Kohana::$config->load('loader.prepared_images_path');
				if (empty($prepared_images_path)) {
					throw new Kohana_Exception("Ошибка конфигурации - не найден путь для подготовленных изображений товаров");
				}

				$ext_images = Kohana::$config->load('product.product.image.ext');
				if (empty($ext_images)) {
					throw new Kohana_Exception("Ошибка конфигурации - не найдены типы изображений");
				}

				try {

					// перекодируем прайс
					if (($str=file_get_contents($price))===FALSE) {
						throw new Loader_Exception('Ошибка перекодирования прайса: чтение');
					}

					$str = iconv('Windows-1251','UTF-8',$str);

					if (file_put_contents($price,$str)===FALSE) {
						throw new Loader_Exception('Ошибка перекодирования прайса: запись');
					}

					if (($price=@fopen($price,'r'))===FALSE) {
						throw new Loader_Exception('Ошибка открытия файла прайса');
					}

					// пропускаем заголовок
					for ($i=0;$i < self::START_PRICE + 5;$i++) {
						$csv = fgetcsv($price,0,';');
					}

					$line_num = 0;

					$added_products = 0;

					while (true) {

						$csv = fgetcsv($price,0,';');
						if ($csv===FALSE) break; // выход из цикла

						$line_num++;

						$values = array();

						$values['category_id'] = $category_id;

						// наименование товара
						$values['name'] = trim($csv[1]);

						// артикул
						$values['article'] = trim($csv[2]);

						// уникальный код - берется из артикула
						$values['code'] = intval(trim($csv[2]));

						// цена
						$values['opt_price'] = $values['price'] = floatval(str_replace(',','.',str_replace(' ','',str_replace('руб.','',$csv[3]))));

						// единицы измерения
						$values['opt_unit'] = $values['unit'] = trim($csv[4]);

						// не рассматриваем не значащие строки
						if (empty($values['name']) || empty($values['article']) || empty($values['code']) || empty($values['price'])) {
							continue;
						}

						// формируем код и артикул
						$values['code'] = $values['code'] + (self::$add_price_prefixes[$category_id] * self::ADD_PRICE_PREFIX_MUL);
						$values['article'] = self::ADD_PRICE_ARTICLE_PREFIX . $values['article'];

						$product->clear();
						$product = $product->where('code','=',$values['code'])->find();

						// не перезаписываем наименование и описание
						if ($product->loaded()) {
							unset($values['name']);
							// даже для существующего товара может быть возможен поиск изображения
							$count_images = $product->images->count_all();
							$values['check_image'] = ($count_images==0 ? 1 : 0);
						} else {
							$values['new'] = 1;
							$values['check_image'] = 1;
							$added_products++;
						}

						$product->values($values);

						$product->save();
					}

					// для товаров поиск подготовленных изображений
					$added_images = 0;

					$product = new Model_Product();
					$products = $product->where('check_image','=',1)->find_all();

					$image = new Model_Product_Image();

					foreach ($products as $p) {

						// поиск подготовленных изображений
						$name_search = sprintf("%07d",$p->code);

						foreach ($ext_images as $ext) {
							foreach (glob($prepared_images_path . "$name_search.$ext") as $fname) {

								$fname_base = basename($fname);
								$fname_destination = $product_images_path . $fname_base;

								@unlink($fname_destination);

								if (copy($fname,$fname_destination)) {

									$image->clear();

									$image->image = $fname_base;
									$image->parent_id = $p->id;
									$image->save();

									$added_images++;
								}
							}
						}

						$p->check_image = 0;
						$p->save();
					}

					$loaded_string = "\nОработано строк: $line_num\nНовых товаров: $added_products, изображений: $added_images";

					if (empty($warning_strings)) {

						Admin::message("Прайс успешно загружен" . $loaded_string, Admin::MSG_OK);

					} else {

						// Admin::message("Прайс загружен c предупреждениями:\n" . $warning_msg . $loaded_string, Admin::MSG_WARNING);
						Admin::message("Прайс загружен c предупреждениями.\n" . $loaded_string, Admin::MSG_WARNING);

					}

				} catch (Loader_Exception $e) {

					Admin::message($e->getMessage(), Admin::MSG_ERROR);

				}

			} else {

				Admin::message('Ошибка загрузки прайса', Admin::MSG_ERROR);
			}
		}

		return self::action_price2();
	}


	// страница загрузки фото
	public function action_image() {

		$this->content = View::factory('admin/loader/image');

	}


	// загрузка прайсаУспешно загруженоУспешно загружено
	public function action_loadimage() {
		ini_set("memory_limit","512M");
		set_time_limit(0);
		/*ini_set('MAX_EXECUTION_TIME', 86400);
		ini_set('MAX_EXECUTION_TIME', -1);*/

		if ($this->request->method()!='POST') {
			throw new HTTP_Exception_400('Недопустимый запрос');
		}

		$icount = 0;
		$warning_msg = '';
		$exts_load = array('jpg','jpeg','png','gif','zip');
		$exts = array('jpg','jpeg','png','gif','JPG','JPEG','PNG','GIF');

		$path = Kohana::$config->load("loader.path");
		if (empty($path)) {
			throw new Kohana_Exception("Ошибка конфигурации");
		}

		$product_images_path = Kohana::$config->load('product.product.image.path');
		if (empty($product_images_path)) {
			throw new Kohana_Exception("Ошибка конфигурации");
		}
		$product_images_path = DOCROOT . $product_images_path;

		$files = Arr::get($_FILES,'images',array());

		if (!empty($files) && isset($files['name'])) {

			$keys = array_keys($files);

			// преобразуем одиночную загрузку во множественную
			if (!is_array($files['name'])) {
				foreach ($keys as $k) {
					$files[$k] = array($files[$k]);
				}
			}

			$count_items = count($files['name']);
			for ($index=0;$index<$count_items;$index++) {

				$file = array();
				foreach ($keys as $k) {
					if (isset($files[$k][$index])) {
						$file[$k] = $files[$k][$index];
					}
				}
				if (empty($file)) continue;

				// очищаем каталог для загрузки
				@array_map('unlink',glob($path . '*'));

				if (Upload::not_empty($file) &&
					 Upload::valid($file) &&
					 Upload::type($file,$exts_load) &&
					 ($original_name = Arr::get($file,'name'))) {

					try {

						if (($full_name=Upload::save($file,$original_name,$path))===FALSE) {
							throw new Exception("Ошибка загрузки файла. Ошибка при сохранении.");
						}

						$file_name = basename($full_name);
						$ext = pathinfo($file_name,PATHINFO_EXTENSION);

						$images = array();

						if (strtolower($ext)=='zip') {

							// распаковываем архив
							$za = new ZipArchive();

							if ($za->open($path . $file_name)===TRUE) {

								for ($i=0;$i<$za->numFiles;$i++) {

									$zf = $za->statIndex($i);
									$ext = strtolower(pathinfo($zf['name'],PATHINFO_EXTENSION));
									$fn = basename($zf['name']);

									// выбираем из архива только допустимые типы файлов
									if (in_array($ext,$exts)) {

										if ($content_file = $za->getFromIndex($i)) {

											if (file_put_contents($path . $fn,$content_file)!==FALSE) {

												$images[] = $fn;
											}
										}
									}
								}

								$za->close();
							}

						} else {
							$images[] = $file_name;
						}

						$product = new Model_Product();
						// загрузка изображений
						foreach ($images as $img_name) {
							$result[] = $img_name;
							$iname = pathinfo($img_name,PATHINFO_FILENAME);

							$product->clear();
							$product = $product->where('code','like',$iname)->find();

							if ($product->loaded()) {

								$image = $product->images->where('data','like',$img_name)->find();

								if ($image->loaded()) {
									@unlink($product_images_path . $image->image);

									$image->delete_prepared_images();
								}

								if (rename($path . $img_name, $product_images_path . $img_name)) {

									$image->image = $img_name;
									$image->parent_id = $product->id;
									$image->save();
									$result2[] = $img_name;
									$icount++;
								}
							}
						}

					} catch (Exception $e) {
						Admin::message($e->getMessage(), Admin::MSG_ERROR);
					}
				}
			}
		/*	var_dump($result);
			var_dump($result2);*/
		}

		if ($icount>0) {
			Admin::message("Успешно загружено $icount фото", Admin::MSG_OK);
		} else {
			Admin::message("Ни одного фото не загружено", Admin::MSG_INFO);
		}

		return self::action_image();
	}
}
