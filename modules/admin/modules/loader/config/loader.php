<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * @package    Admin
 * @module 		Users
 * @category   Config file
 * @author     ESV Corp. (C) 12.2011
 */

	return array (
		// конфигурация для шаблона заголовка страницы:
		// пути, файлы стилей, скрипты
		'head_page' => array (
			'author' => 'ESV Corp. &copy; 2011',
			'path' => array (
				'css'			=> 'include/css/',
				'script'		=> 'include/js/',
				'scripts'	=> 'include/js/',
				'image'		=> 'include/images/',
				'images'		=> 'include/images/'
			),
			'css' => array (
			),
			'scripts' => array (
			)
		),
		'path' => DOCROOT . 'upload/loader/',
		'prepared_images_path' => DOCROOT . 'upload/price/images/', // подготовленные изображения для товаров
		'logo' => 'include/icon/loader.png'
	);
?>
