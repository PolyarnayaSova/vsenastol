<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Init file
 *
 * @package    Admin/Loader
 * @category   Init file
 * @author     ESV Corp. (C) 12.2011
 */

	Admin::RegisterModule('admin/loader','admin_loader','Загрузка');

	Admin::RouteSet('admin/loader','admin_loader','admin/loader(/<action>)')
		->defaults(array(
			'controller' => 'admin_loader',
			'action'     => 'index',
		));

	// добавим пункты подменю
	Admin::ActionSet('admin/loader',Route::url('admin_loader', array('action' => 'price')),'прайс');
	Admin::ActionSet('admin/loader',Route::url('admin_loader', array('action' => 'image')),'фото');
	//Admin::ActionSet('admin/loader',Route::url('admin_loader', array('action' => 'price2')),'дополнительные прайсы');
?>
