<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Tours page template
 *
 * @package    Admin/Tour
 * @module		Tour
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */
?>



<div class="list">

	<div class="listHeader"><?php print $brands_page->name; ?></div>

	<?php if (count($brands)>0): ?>

	<table border="0" cellpadding="0" cellspacing="0" width="800" cols="2">
		<th>наименование</th>
		<th>&nbsp;</th>
		<?php	foreach($brands as $b): ?>
		<tr>
			<td class="info"><?php print HTML::anchor(Route::url('admin_content',array('action'=>'edit','back'=>'back','id'=>$b->id)),$b->name); ?></td>
			<td width="20" class="last"><?php print HTML::anchor(Route::url('admin_content',array('action'=>'del' ,'id'=>$b->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данный бренд?');")); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php else: ?>

	<p>туров нет</p>

	<?php endif; ?>

	<div class="submit">
		<div class="margin"></div>
		<?php print HTML::anchor(Route::url('admin_content',array('action'=>'edit','back'=>'back','content_id'=>$brands_page->id,'time'=>(date('is') . rand(100,999)))),'добавить'); ?>
		<div class="delimiter">|</div>
		<?php print HTML::anchor(Route::url('admin_content',array('action'=>'editcontent','id'=>$brands_page->id,'time'=>(date('is') . rand(100,999)))),"страница '{$brands_page->name}'"); ?>
	</div>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager">
	<?php print  $pager; ?>
	</div>
	<?php endif; ?>

</div>
