<?php defined('SYSPATH') or die('No direct script access.');

	// параметры постраничной навигации
	return array (
		// для списка брендов
		'admin_brands' => array (
			'total_items'		=> 0, // заполняется при вычислении
			'items_per_page'	=> 20,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			'view'		=> 'admin/pager/floating'
		)
	);
?>
