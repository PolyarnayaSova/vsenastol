<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 *
 * Brands controller
 *
 * @package    Admin/Brand
 * @category   Module
 * @author     ESV Corp. (C) 12.2011
 */
class Controller_Admin_Brands extends Controller_Admin {

	public function before() {
		parent::before();

		// модуль будет использоваться в ajax-контенте, поэтому не отрисовываем шаблон
		$this->auto_render = false;
	}


	public function action_index() {

		$content_id = $this->request->param('content_id',0);

		$brands_page = Model_Content::get_by_key('brands');

		if (!$brands_page->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		$brands = new Model_Content();
		$brands->where('parent_id','=',$brands_page->id)->order_by('name');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.admin_brands');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($brands->reset(false)->find_all());

		$brands = $brands->offset($offset)->limit($limit)->find_all();

		$this->response->body(
			View::factory('admin/brand/brand')
				->set('brands_page',$brands_page)
				->set('brands',$brands)
				->set('pager',$pager)
		);
	}
}
