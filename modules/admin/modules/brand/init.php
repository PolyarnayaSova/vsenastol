<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 * Init file
 *
 * @package    Admin/Brand
 * @category   Init file
 * @author     ESV Corp. (C) 11.2011
 */

// при регистрации внутреннего модуля заголовок не указывается - модуль не попадает в меню
// доступ осуществляется через контент-страницы
Admin::RegisterModule('admin/brand','admin_brand');

Admin::RouteSet('admin/brand','admin_brand','admin/brand(/<action>(/<id>)(/content_<content_id>)(/ts_<time>))', array('id'=>'\d+','content_id'=>'\d+','time'=>'\d+'))
	->defaults(array(
		'controller' => 'admin_brands',
		'action'     => 'index',
	));
?>
