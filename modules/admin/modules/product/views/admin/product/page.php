<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Product page template
 *
 * @package    Admin
 * @module		Admin/Product
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */

	if ($product->loaded()) {
		$new = false;
		$caption = 'Редактировать товар';
		$submit = 'сохранить';
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$product->id));
		$code = $product->code;
	} else {
		$new = true;
		$caption = 'Добавить товар';
		$submit = 'добавить';
		$form = Route::url(Admin::route(),array('action'=>'save','category_id'=>$category_id));
		$code = null;
	}
?>

<?php
	// если есть сообщение
	if ($message) { print $message; }
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST')); ?>

	<div class="form">

		<?php print HTML::anchor(Route::url('admin_product_category',array('action'=>'edit','id'=>$category_id)),'&laquo; список товаров',array('class'=>'back','context'=>'DIV#products')); ?>

		<div class="caption"><?php print $caption; ?></div>

		<fieldset>

			<?php if ($product->supplier_id): ?>
			<div class="field">
				<label>
					<div class="label">Поставщик</div>
					<?php print Controller_Admin_Loader::getSupplierNameById($product->supplier_id); ?>
				</label>
			</div>
			<?php endif; ?>
			<?php if ($code): ?>
			<div class="field">
				<label>
					<div class="label">Уникальный код</div>
					<?php print $code; ?>
				</label>
			</div>
			<?php endif; ?>

			<div class="field">
				<label>
					<div class="label">Наименование</div>
					<?php print Form::input('name',$product->name,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Кратко</div>
					<?php print Form::textarea('brief',$product->brief,array('cols'=>80,'rows'=>2)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Описание</div>
					<?php print Form::textarea('text',$product->text,array('cols'=>80,'rows'=>5,'id'=>'wysiwyg')); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Производитель</div>
					<?php print Form::select('brand_id',$brands,$product->brand_id); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Остаток</div>
					<?php print Form::input('rest',$product->rest,array('size'=>12)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Артикул</div>
					<?php print Form::input('article',$product->article,array('size'=>20)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Штрихкод</div>
					<?php print Form::input('barcode',$product->barcode,array('size'=>20)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Вес</div>
					<?php print Form::input('weight',$product->weight,array('size'=>12)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Объем</div>
					<?php print Form::input('capacity',$product->capacity,array('size'=>12)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Материал</div>
					<?php print Form::select('material_id',$materials,$product->material_id); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Цена оптовая</div>
					<?php print Form::input('opt_price',$product->opt_price,array('size'=>10)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Ед. изм. оптовые</div>
					<?php print Form::input('opt_unit',$product->opt_unit,array('size'=>10)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Цена</div>
					<?php print Form::input('price',$product->price,array('size'=>10)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Ед. изм.</div>
					<?php print Form::input('unit',$product->unit,array('size'=>10)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Участвует в акции</div>
					<?php print Form::checkbox('action', 1, $product->action != 0); ?>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('save',$submit); ?>
	</div>

	<?php print Form::close(); ?>

</div>
