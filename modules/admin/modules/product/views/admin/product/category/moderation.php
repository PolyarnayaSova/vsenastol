<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Newest Products for moderation
 *
 * @package    Admin
 * @module		Admin/Product_Simple
 * @category   Template
 * @author     ESV Corp. (C) 01.2012
 */
?>

<h1 class="caption">Новые товары на модерацию</h1>

<div class="tabs">

	<ul>
		<li><a href="#category_products">товары</a></li>
	</ul>

	<div class="tab" id="category_products">
		<?php print Request::factory(Route::url('admin_products',array('action'=>'index','category_id'=>-1)))->execute(); ?>
	</div>

</div>

<script language="javascript">
<!--
	// табы
	if (window.$) {
		$(document).ready(function(){
			$('DIV.tabs').tabs({
				select: function() {
					$(this).find('DIV.message').remove(); // убрать сообщения при переключении закладок
				}
			});
		});
	}
-->
</script>
