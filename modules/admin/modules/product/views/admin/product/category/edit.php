<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Category pages block (tab) template
 *
 * @package    Admin
 * @module		Admin/Product_Simple
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */

	if ($category->loaded()) {
		$new = false;
		$page_url = Route::url(Admin::route(),array('action'=>'page','id'=>$category->id));
	} else {
		$new = true;
		$page_url = Route::url(Admin::route(),array('action'=>'page','category_id'=>$category_id));
	}

	$leaf_category = $category->is_leaf();
?>

<?php
	// загрузка файлов и изображений только для существующей категории
	if (!$new):
?>

<h1 class="caption">Категория: <?php print $category->name; ?></h1>

<div class="tabs">

	<ul>
		<?php if ($leaf_category): ?>
		<li><a href="#category_products">товары</a></li>
		<?php endif; ?>
		<li><a href="#category">страница категории</a></li>
		<li><a href="#images">изображения</a></li>
	</ul>

	<?php if ($leaf_category): ?>
	<div class="tab" id="category_products">
		<?php print Request::factory(Route::url('admin_products',array('action'=>'index','category_id'=>$category->id)))->execute(); ?>
	</div>
	<?php endif; ?>

	<div class="tab" id="category">

<?php endif; ?>

		<?php print Request::factory($page_url)->execute(); ?>

<?php
	// страницы параметров и товаров только для существующей категории
	if (!$new):
?>
	</div>

	<div class="tab" id="images">
		<?php print Request::factory(Route::url('admin_product_category_images',array('action'=>'index','category_id'=>$category->id)))->execute(); ?>
	</div>

</div>

<script language="javascript">
<!--
	// табы
	if (window.$) {
		$(document).ready(function(){
			$('DIV.tabs').tabs({
				select: function() {
					$(this).find('DIV.message').remove(); // убрать сообщения при переключении закладок
				}
			});
		});
	}
-->
</script>

<?php endif; ?>
