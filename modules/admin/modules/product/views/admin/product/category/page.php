<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Category page template
 *
 * @package    Admin
 * @module		Admin/Product
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */


	if ($category->loaded()) {
		$new = false;
		$caption = 'Редактировать страницу категории';
		$submit = 'сохранить';
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$category->id));
	} else {
		$new = true;
		$caption = 'Добавить категорию';
		$submit = 'добавить';
		$form = Route::url(Admin::route(),array('action'=>'save','category_id'=>$category_id));
		$category->visible = 1;
	}
?>

<?php
	// если есть сообщение
	if ($message) { print $message; }
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST')); ?>

	<div class="form">

		<div class="caption"><?php print $caption; ?></div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Наименование</div>
					<?php print Form::input('name',$category->name,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Кратко</div>
					<?php print Form::textarea('brief',$category->brief,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<table><tr><td>
					<div class="label">Текст</div>
					</td><td>
					<?php print Form::textarea('text',$category->text,array('cols'=>80,'rows'=>8,'id'=>'wysiwyg')); ?>
					</td></tr></table>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">видимая</div>
					<?php print Form::checkbox('visible', 1, $category->visible!=0); ?>
				</label>
			</div>

		</fieldset>

		<?php if (Admin::logged_in(Kohana::$config->load('admin.privileges.seo'))): ?>

		<div class="group">SEO</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Title</div>
					<?php print Form::input('title',$category->title,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Keywords</div>
					<?php print Form::textarea('keywords',$category->keywords,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Description</div>
					<?php print Form::textarea('description',$category->description,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

		</fieldset>

		<?php endif; ?>

	</div>

	<div class="submit">
		<?php print Form::submit('save',$submit); ?>
	</div>

	<?php print Form::close(); ?>

</div>

<script language="javascript">
<!--
	if (typeof window['ckeditor_start'] == 'function') {
		ckeditor_start('wysiwyg');
	}
-->
</script>
