<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Products page template
 *
 * @package    Admin/Product
 * @module		Product
 * @category   Template
 * @author     ESV Corp. since 11.2011
 */

// ВАЖНО!!! отображается в закладке модуля категорий

?>


<div class="list">

	<?php if (count($products)>0): ?>

	<table border="0" cellpadding="0" cellspacing="0" width="800">
		<th>название</th>
		<th>&nbsp;</th>
		<?php
			foreach($products as $p):
		?>
		<tr>
			<td class="info"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','id'=>$p->id)),$p->name); ?></td>
			<td width="20" class="last"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'del' ,'id'=>$p->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данный товар?')",'context'=>'DIV#category_products')); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php else: ?>
	<p>товаров нет</p>
	<?php endif; ?>

	<?php
		// в случае, если список на модерацию, тогда категория не загружена и невозможно добавить товар
		if ($category->loaded()):
	?>
	<div class="submit">
		<div class="margin"></div>
		<?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','category_id'=>$category->id,'time'=>(date('is') . rand(100,999)))),'добавить'); ?>
		<div class="delimiter">|</div>
		<?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'delall','category_id'=>$category->id,'time'=>(date('is') . rand(100,999)))),'очистить',array('onclick'=>"return confirm('Вы действительно хотите удалить ВСЕ товары в данной категории?')",'context'=>'DIV#category_products')); ?>
	</div>
	<?php endif; ?>

	<?php if ($pager): ?>
	<div class="pager">
	<?php print  Pagination::factory($pager); ?>
	</div>
	<?php endif; ?>

</div>

<script type="text/javascript" language="javascript">
<!--
	if (window.$) {
		$(document).ready(function(){
			// установить для ссылок постраничной навигации контекст
			$('DIV.pager A').attr('context','DIV#category_products');
		});
	}
-->
</script>
