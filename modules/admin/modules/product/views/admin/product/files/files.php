<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Content Files template
 *
 * @package    Admin/Content/Files
 * @module		Admin/Content
 * @category   Template
 * @author     ESV Corp. since 11.2011
 */

	$max_files = ini_get('max_file_uploads');
	
	const FILE_CONTEXT = 'DIV#files'; // блок для вывода изображений

	$allow_upload = implode(', ',Kohana::$config->load('content.file.upload_ext')); // файлы, доступные для загрузки

	$form = Route::url(Admin::route(),array('action'=>'add','content_id'=>$content->id));
?>

<?php
	// если есть сообщение
	if ($message) { print $message; }
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST','enctype' => 'multipart/form-data', 'context'=> FILE_CONTEXT)); ?>

	<div class="form">

		<div class="caption">Файлы</div>


		<?php if (count($files)>0): ?>

		<div class="list">
			<table border="0" cellpadding="0" cellspacing="0" width="800" cols="4">
				<th>файл</th>
				<th>&nbsp;</th>
				<th>комментарий</th>
				<th>&nbsp;</th>
				<?php	foreach($files as $file): ?>
				<tr>
					<td class="text nowrap" width="300"><?php print HTML::anchor($file->path(),$file->file,array('context'=>'pass through','target'=>'_blank')); ?></td>
					<td class="text nowrap right" width="10"><?php print $file->size_str(); ?></td>
					<td class="text"><?php print $file->name; ?></td>
					<td width="20" class="last" nowrap>
						<?php
							print HTML::anchor(
										Route::url(Admin::route(),	array('action'=>'del' ,'id'=>$file->id)),
										'удалить',
										array(
											'context'=>FILE_CONTEXT,
											'onclick'=>"return confirm('Вы действительно хотите удалить данный файл?');"
										)
									);
						?>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>

		<?php else: ?>

		<div class="list_items">
			<p>файлов нет</p>
		</div>

		<?php endif; ?>

		<div class="small_caption">Добавить файл</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Комментарий</div>
					<div class="input">
					<?php print Form::input('comment',$content->title,array('size'=>40)); ?>
					</div>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Файл</div>
					<div class="input">
					<?php print Form::file('file[]',array('title'=>"доступны для загрузки: $allow_upload. Одновременно до $max_files файлов.",'multiple'=>'multiple')); ?>
					</div>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">распаковать</div>
					<div class="input">
					<?php print Form::checkbox('unpack', 1, false, array('title'=>"Возможно распаковать zip-архив. ВНИМАНИЕ! Имена файлов, названные по-русски, могут быть усечены.")); ?>
					</div>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('save','добавить'); ?>
	</div>

	<?php print Form::close(); ?>

</div>
