<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Product pages block (tab) template
 *
 * @package    Admin
 * @module		Admin/Product
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */

	if ($product->loaded()) {
		$new = false;
		$page_url = Route::url(Admin::route(),array('action'=>'page','id'=>$product->id,'category_id'=>$category->id));
	} else {
		$new = true;
		$page_url = Route::url(Admin::route(),array('action'=>'page','category_id'=>$category->id));
	}
?>

<?php if (!$new): ?>
<h1 class="caption">Товар: <?php print $product->name; ?></h1>
<?php endif; ?>

<div class="tabs">

	<ul>
		<li><a href="#product">товар</a></li>
		<?php if (!$new): ?>
		<li><a href="#images">изображения</a></li>
		<?php endif; ?>
	</ul>

	<div class="tab" id="product">

		<?php print Request::factory($page_url)->execute(); ?>

	</div>

	<?php if (!$new): ?>
	<div class="tab" id="images">
		<?php print Request::factory(Route::url('admin_product_images',array('action'=>'index','product_id'=>$product->id)))->execute(); ?>
	</div>
	<?php endif; ?>

</div>

<script language="javascript">
<!--
	// табы
	if (window.$) {
		$(document).ready(function(){
			$('DIV.tabs').tabs({
				select: function() {
					$(this).find('DIV.message').remove(); // убрать сообщения при переключении закладок
				}
			});
		});
	}
-->
</script>
