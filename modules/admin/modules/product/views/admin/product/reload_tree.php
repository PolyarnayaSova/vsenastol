<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Add script for reloading tree
 * Используется при удалении всех товаров из категории, так чтобы
 * дерево перезагрузилось и категорию стало возможно удалить
 *
 * @package    Admin
 * @module		Admin/Product_Simple
 * @category   Template
 * @author     ESV Corp. (C) 01.2012
 */
?>

<script language="javascript">
<!--
	// перезагрузка дерева
	if (window.$) {
		$(document).ready(function(){

			<?php print Tree::var_name() . '.reload_tree()'; ?>;

		});
	}
-->
</script>
