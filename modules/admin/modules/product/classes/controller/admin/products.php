<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Products controller
 *
 * @package    Admin
 * @category   Module
 * @module		Admin/Product
 * @author     ESV Corp. since 10.2011
 */
class Controller_Admin_Products extends Controller_Admin {


	public function before() {

		parent::before();

		$this->auto_render = false;
	}


	public function action_index() {

		$category_id = $this->request->param('category_id',0);

		// список товаров на модерацию
		if ($category_id==-1) {

			$category = new Model_Product_Category();

			$products = new Model_Product();

			$products = $products->where('new','=',1);

		} elseif ($category_id == -2){

			$category = new Model_Product_Category();

			$products = new Model_Product();

			$products = $products->where('action','=',1);

		} else {

			$category = new Model_Product_Category($category_id);

			if (!$category->loaded()) {
				throw new HTTP_Exception_404('Категория для выборки товаров не найдена');
			}

			$products = $category->products;
		}

		$products->order_by('name')->order_by('created','DESC');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.admin_products');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($products->reset(false)->find_all());

		$products = $products->offset($offset)->limit($limit)->find_all();

		if ($pager['total_items']<=count($products)) {
			$pager = null;
		}

		$this->auto_render = false;

		$this->response->body(
			View::factory('admin/product/list')
				->set('category',$category)
				->set('products',$products)
				->set('pager',$pager)
		);
	}


	// добавить, редактировать товар
	public function action_edit() {

		$id = $this->request->param('id',0);
		$category_id = $this->request->param('category_id',0);

		$product = new Model_Product($id);

		if ($product->loaded()) {
			$category = $product->category;
		} else {
			$category = new Model_Product_Category($category_id);
		}

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория не найдена');
		}

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->response->body(
			View::factory('admin/product/edit')
				->set('category',$category)
				->set('product',$product)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// получить контент страницы для редактирования товара
	// HMVC
	public function action_page() {

		if ($this->request->is_external()) {
			throw new HTTP_Exception_400('Недопустимый запрос');
		}

		$id = $this->request->param('id',0);
		$category_id = $this->request->param('category_id',0);

		$product = new Model_Product($id);

		// получаем блок сообщения
		list($message,$status) = parent::message();

		// список брендов
		$brands = Model_Product_Brand::array_for_select();

		// список материалов
		$materials = Model_Product_Material::array_for_select();

		$this->response->body(
			View::factory('admin/product/page')
				->set('product',$product)
				->set('category_id',$category_id)
				->set('brands',$brands)
				->set('materials',$materials)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// сохранить товар
	public function action_save() {

		if ($this->request->method()!='POST') {
			$this->auto_render = false;
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$id = $this->request->param('id',0);
		$category_id = $this->request->param('category_id',0);

		$product = new Model_Product($id);

		if ($product->loaded()) {
			$category = $product->category;
		} else {
			$category = new Model_Product_Category($category_id);
		}

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория не найдена');
		}

		$name = $_POST['name'] = trim((isset($_POST['name']) ? $_POST['name'] : ''));

		// возврат к редактированию, если пустое наименование

		if (empty($name)) {
			Admin::message('Необходимо обязательно ввести наименование',Admin::MSG_WARNING);
			if ($product->loaded()) {
				$this->response->body(
					Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$product->id)))->execute()
				);
			} else {
				$this->response->body(
					Request::factory(Route::url(Admin::route(),array('action'=>'edit','category_id'=>$category->id)))->execute()
				);
			}
			return;
		}

		// checkbox
		$_POST['action'] = Arr::get($_POST, 'action', 0);

		$fields = array_keys($product->list_columns());
		$product->values($_POST,$fields);
		$product->category_id = $category->id;
		$product->new = 0; // всегда снимаем пометку - используется для редактирования товаров в списке модерации
		$product->save();

		Admin::message('Информация о товаре успешно сохранена',Admin::MSG_SUCCESS);

		// переход к редактированию
		$this->response->body(
			Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$product->id)))->execute()
		);
	}


	// удалить товар
	public function action_del() {

		$id = $this->request->param('id',0);

		$product = new Model_Product($id);

		if (!$product->loaded()) {
			throw new HTTP_Exception_404('Товар для удаления не найден');
		}

		$category = $product->category;

		$product->delete();

		// отображение root-страницы
		$this->response->body(
			Request::factory(Route::url(Admin::route(),array('action'=>'index','category_id'=>$category->id)))->execute()
		);
	}


	// удалить все товары в категории
	public function action_delall() {

		$category_id = $this->request->param('category_id',0);

		$category = new Model_Product_Category($category_id);

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория для удаления товаров не найдена');
		}

		$products = $category->products->find_all();

		// каждый товар удаляем отдельно, т.к. с ним могут быть связаны изображения
		foreach ($products as $p) {
			$p->delete();
		}

		// отображение root-страницы
		// дополнительно добавляем скрипт для перезагрузки дерева
		$this->response->body(
			Request::factory(Route::url(Admin::route(),array('action'=>'index','category_id'=>$category->id)))->execute() .
			View::factory('admin/product/reload_tree')
		);
	}
}
