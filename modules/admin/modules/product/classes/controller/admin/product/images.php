<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * product pages images controller
 *
 * @package    Admin
 * @category   Module
 * @module		Admin/product/Files
 * @author     ESV Corp. since 11.2011
 */



class Controller_Admin_Product_Images extends Controller_Admin {


	public function before() {
		parent::before();

		$this->auto_render = false;
	}


	public function action_index() {

		$product_id = $this->request->param('product_id',0);

		$product = new Model_Product($product_id);

		if (!$product->loaded()) {
			throw new HTTP_Exception_404('Товар для загрузки изображений не определен');
		}

		$images = $product->images->where('type','=',Model_Product_Image::type())->order_by('position')->find_all();

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->response->body(
			View::factory('admin/product/images/images')
				->set('product',$product)
				->set('images',$images)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// загрузка изображения контент-страницы
	public function action_add() {

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$name = Arr::get($_POST,'comment','');

		$product_id = $this->request->param('product_id',0);

		$product = new Model_Product($product_id);

		if (!$product->loaded()) {
			throw new Kohana_Exception('Товар для загрузки изображения не определен');
		}

		$file = Arr::get($_FILES,'image',array());

		if (!($count=Model_Product_Image::load($file,$product->id,$name))) {
			Admin::message('Изображение не загружено',Admin::MSG_WARNING);
		} elseif ($count>1) {
			Admin::message("Загружено изображений: $count",Admin::MSG_WARNING);
		}

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'product_id'=>$product->id)))->execute()
		);
	}


	// перемещение изображения в новую позицию
	public function action_move() {

		if ($this->request->method()!='POST' || !$this->request->is_ajax()) {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$drag_id = $this->request->post('drag_id'); // перемещаемый элемент
		$drop_id = $this->request->post('drop_id'); // элемент, на который переместили

		Model_Product_Image::move($drag_id,$drop_id);

		$image = new Model_Product_Image($drag_id);
		$product = $image->product;

		if (!$product->loaded()) {
			throw new HTTP_Exception_404('Товар перемещаемых элементов не найден');
		}

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'product_id'=>$product->id)))->execute()
		);
	}


	// удаление изображения
	public function action_del() {

		$id = $this->request->param('id',0);

		$image = new Model_Product_Image($id);

		if (!$image->loaded()) {
			throw new HTTP_Exception_404('Изображение для удаления не найдено');
		}

		$product = $image->product;

		if (!$product->loaded()) {
			throw new HTTP_Exception_404('Товар изображения для удаления не найдена');
		}

		$image->delete();

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'product_id'=>$product->id)))->execute()
		);
	}
}
?>
