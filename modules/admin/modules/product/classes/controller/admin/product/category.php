<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Category of products controller
 *
 * @package    Admin
 * @category   Controller
 * @module		Admin/Product
 * @author     ESV Corp. since 10.2011
 */

// контроллер для работы с деревом Tree, реализует интерфейс Controller_Tree_Interface
// необходима раелизация следующих методов:
// action_tree(POST[node])	- получение элементов дерева
// action_get/id				- получение контента определенного элемента
// action_add/id				- добавление потомка к элементу
// action_del/id				- удаление элемента
// action_move(POST[drag_id,drop_id,point]) - перемещение элемента
//

class Controller_Admin_Product_Category extends Controller_Admin implements Controller_Tree_Interface {


	public function action_index() {

		// инициализация дерева
		Tree::init(
			0,							//	root_id		- id корня дерева
			'Категории товаров',	// root_name	- наименование корня дерева
			300,						// width			- ширина
			650,						// height		- высота
			Route::url(Admin::route()),
			'category_product_tree_content'	// id DIV-а в который будет загружаться контент по ajax
														// установлены стили в product.css
		);

		parent::scripts(Tree::scripts(),DOCROOT);
		parent::css(Tree::css(),DOCROOT);

		$this->content = Tree::content();
	}


	// загрузка дерева в элемент ExtJS - дерево контент-страниц
   public function action_tree() {

		$this->auto_render = false;

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

      $id = Arr::get($_POST, 'node', 0);

      $result = array();

		$node = new Model_Product_Category(intval($id));

		$tree = $node->children();

		// формируем массив для передачи в виде JSON
		foreach ($tree as $node) {

			$is_leaf = $node->is_leaf();

			$reason = null;
			$allowDelete = $is_leaf;

			if ($node->products->count_all()>0) {
				$allowDelete = false;
				$reason = 'Категория содержит товары';
			}

			$node_array = array('id' => $node->id, 'parent_id' => $node->parent_id, 'text' => $node->name, 'leaf' => $is_leaf, 'allowDelete' => $allowDelete, 'reason' => $reason); // false, 'cls' => 'node_bold'

			$result[] = $node_array;
		}

		// добавляем в корень элементы:
		// "На модерацию", где будут появляться все вновь-добавленные товары
		// "Учавствуют в акции", товары с пометкой участия в акции
		if ($id==0) {
			$node_array = array('id' => -1, 'parent_id' => 0, 'text' => 'Новые товары на модерацию', 'leaf' => true, 'allowDelete' => false, 'reason' => 'Встроенный элемент', 'cls' => 'node_bold');
			$result[] = $node_array;
			$node_array = array('id' => -2, 'parent_id' => 0, 'text' => 'Товары участвующие в акции', 'leaf' => true, 'allowDelete' => false, 'reason' => 'Встроенный элемент', 'cls' => 'node_bold');
			$result[] = $node_array;
		}

      $this->response->body(json_encode($result));
   }


	// получение элемента - страница категории
	public function action_get() {

		$id = $this->request->param('id',0);

		// HMVC
		$this->auto_render = false;
		if (!empty($id)) {
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)))->execute()
			);
		} else {
			$this->response->body('');
		}
	}


	// добавить элемент - страницу категории
	public function action_add() {

		// переход на страницу редактирования
		$category_id = $this->request->param('id',0);

		// HMVC
		$this->auto_render = false;
		$this->response->body(
			Request::factory(Route::url(Admin::route(),array('action'=>'edit','category_id'=>$category_id)))->execute()
		);
	}


	// удалить элемент - страницу категории
	public function action_del() {

		$id = $this->request->param('id',0);

		$category = new Model_Product_Category($id);

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория для удаления не найдена');
		}

		$parent_category = $category->category;

		if ($category->is_leaf()) {
			$deleted = true;
			$category->delete();
		} else {
			$deleted = false;
		}

		$this->auto_render = false;

		if ($deleted) {
			// если удалена, переход к редактированию родительского элемента
			if ($parent_category->loaded()) {
				$this->response->body(
					Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$parent_category->id)))->execute()
				);
			} else {
				// либо отображение пустой страницы, если родительский элемент - корень
				$this->response->body('');
			}
		} else {
			// иначе - возврат к редактированию контент-страницы
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$category->id)))->execute()
			);
		}
	}


	// переместить старницу
	public function action_move() {

		$this->auto_render = false;

		if (!($this->request->method()=='POST' && $this->request->is_ajax())) {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$success = array (
			'success' => true,
			'msg' => "Ok"
		);

		$error = array (
			'success' => false,
			'msg' => "no move"
		);

		$drag_id = Arr::get($_POST,'drag_id');
		$drop_id = Arr::get($_POST,'drop_id');
		$point = Arr::get($_POST,'point');

		if (Model_Product_Category::move($drag_id,$drop_id,$point)) {
			$this->response->status(200);
			$this->response->body(json_encode($success));
		} else {
			$this->response->status(400);
			$this->response->body(json_encode($error));
		}
	}


	// добавить, редактировать контент-страницу, с возможностью перехода по ключевой странице
	public function action_edit() {

		$id = $this->request->param('id',0);
		$category_id = $this->request->param('category_id',0);
		$add_id = $this->request->param('add_id',0); // возможен переход к редактированию, когда добавлен элеменн

		$this->auto_render = false;

		// страница модерации
		if ($id == -1) {

			$content =	View::factory('admin/product/category/moderation');

		} elseif ($id == -2) {

			$content =	View::factory('admin/product/category/actions');

		} else {

			$category = new Model_Product_Category($id);

			$content =
				View::factory('admin/product/category/edit')
					->set('category',$category)
					->set('category_id',$category_id);

			// если добавлен новый элемент, присоединим блок скрипта для добавления элемента в дерево
			if (!empty($add_id)) {
				$content .= Tree::content_add_node($add_id);
			}
		}

		$this->response->body($content);
	}


	// получить контент страницы для редактирования категории
	// HMVC
	public function action_page() {

		if ($this->request->is_external()) {
			throw new HTTP_Exception_400('Недопустимый запрос');
		}

		$id = $this->request->param('id',0);
		$category_id = $this->request->param('category_id',0);

		$category = new Model_Product_Category($id);

		$this->auto_render = false;

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->response->body(
			View::factory('admin/product/category/page')
				->set('category',$category)
				->set('category_id',$category_id)
				->set('message',$message)
				->set('status',$status)
		);
	}



	//// непосредственное редактирование страницы, без учета ключевых страниц
	//public function action_editcategory() {
	//
	//	$id = $this->request->param('id',0);
	//	$category = new Model_Product_Category($id);
	//	if (!$category->loaded()) {
	//		throw new HTTP_Exception_404('Контент-страница не найдена');
	//	}
	//
	//	$count_images = $category->images->where('type','=',Model_Product_Category_Image::TYPE_IMAGE)->count_all();
	//	$count_files = $category->files->where('type','=',Model_Product_Category_File::TYPE_FILE)->count_all();
	//
	//	$this->auto_render = false;
	//	$this->response->body(
	//		View::factory('admin/category/edit')
	//			->set('category',$category)
	//			->set('category_id',$category->category_id)
	//			->set('count_images',$count_images)
	//			->set('count_files',$count_files)
	//	);
	//}


	// сохранить страницу категории
	public function action_save() {

		$this->auto_render = false;

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$id = $this->request->param('id',0);
		$category_id = $this->request->param('category_id',0);

		$_POST['visible'] = Arr::get($_POST,'visible',0);

		$category = new Model_Product_Category($id);

		$fields = array_keys($category->list_columns());

		if ($category->loaded()) {

			$new = false;

			$category->values($_POST,$fields);
			$category->save();

		} else {

			$new = true;

			if (empty($category_id)) {

				// добавим в корень
				$parent_category = new Model_Product_Category();

			} else {

				$parent_category = new Model_Product_Category($category_id);

				if (!$parent_category->loaded()) {
					throw new Kohana_Exception('Родительская категория не найдена');
				}
			}

			// добавим дочерний элемент
			$category = $parent_category->add_child($category,$_POST,$fields);
		}

		if ($new) {
			// переход к редактированию вновь созданной категории
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$category->id,'add_id'=>$category->id)))->execute()
			);
		} else {
			// возврат к редактированию категории
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$category->id)))->execute()
			);
		}
	}
}
