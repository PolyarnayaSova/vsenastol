<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Content pages files controller
 *
 * @package    Admin
 * @category   Module
 * @module		Admin/Content/Files
 * @author     ESV Corp. since 11.2011
 */

class Controller_Admin_Content_Files extends Controller_Admin {


	public function before() {
		parent::before();

		$this->auto_render = false;
	}


	public function action_index() {

		$content_id = $this->request->param('content_id',0);

		$content = new Model_Content($content_id);

		if (!$content->loaded()) {
			throw new Kohana_Exception('Контент-страница для загрузки фалов не определена');
		}

		$files = $content->files->where('type','=',Model_Content_File::type())->order_by('data')->find_all();

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->response->body(
			View::factory('admin/content/files/files')
				->set('content',$content)
				->set('files',$files)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// загрузка файла контент-страницы
	public function action_add() {

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$name = Arr::get($_POST,'comment','');

		$content_id = $this->request->param('content_id',0);

		$content = new Model_Content($content_id);

		if (!$content->loaded()) {
			throw new Kohana_Exception('Контент-страница для загрузки файла не определена');
		}

		$file = Arr::get($_FILES,'file',array());

		if (!($count=Model_Content_File::load($file,$content->id,$name))) {
			Admin::message('Файл(ы) не загружен',Admin::MSG_WARNING);
		} elseif ($count>1) {
			Admin::message("Загружено файлов: $count",Admin::MSG_INFO);
		}

		// выводим страницу списка файлов
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'content_id'=>$content->id)))->execute()
		);
	}


	// удаление файла
	public function action_del() {

		$id = $this->request->param('id',0);

		$file = new Model_Content_File($id);

		if (!$file->loaded()) {
			throw new HTTP_Exception_404('Файл для удаления не найден');
		}

		$content = $file->content;

		if (!$content->loaded()) {
			throw new HTTP_Exception_404('Контент-страница изображения для удаления не найдена');
		}

		$file->delete();

		// выводим страницу списка файлов
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'content_id'=>$content->id)))->execute()
		);
	}
}
?>
