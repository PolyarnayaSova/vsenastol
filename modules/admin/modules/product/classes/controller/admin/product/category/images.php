<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Category pages images controller
 *
 * @package    Admin
 * @category   Module
 * @module		Admin/category/Files
 * @author     ESV Corp. since 11.2011
 */



class Controller_Admin_Product_Category_Images extends Controller_Admin {


	public function before() {
		parent::before();

		$this->auto_render = false;
	}


	public function action_index() {

		$category_id = $this->request->param('category_id',0);

		$category = new Model_Product_Category($category_id);

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория для загрузки изображений не определена');
		}

		$images = $category->images->where('type','=',Model_Product_Category_Image::type())->order_by('position')->find_all();

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->response->body(
			View::factory('admin/product/category/images/images')
				->set('category',$category)
				->set('images',$images)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// загрузка изображения контент-страницы
	public function action_add() {

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$name = Arr::get($_POST,'comment','');

		$category_id = $this->request->param('category_id',0);

		$category = new Model_Product_Category($category_id);

		if (!$category->loaded()) {
			throw new Kohana_Exception('Контент-страница для загрузки изображения не определена');
		}

		$file = Arr::get($_FILES,'image',array());

		if (!($count=Model_Product_Category_Image::load($file,$category->id,$name))) {
			Admin::message('Изображение не загружено',Admin::MSG_WARNING);
		} elseif ($count>1) {
			Admin::message("Загружено изображений: $count",Admin::MSG_WARNING);
		}

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'category_id'=>$category->id)))->execute()
		);
	}


	// перемещение изображения в новую позицию
	public function action_move() {

		if ($this->request->method()!='POST' || !$this->request->is_ajax()) {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$drag_id = $this->request->post('drag_id'); // перемещаемый элемент
		$drop_id = $this->request->post('drop_id'); // элемент, на который переместили

		Model_Product_Category_Image::move($drag_id,$drop_id);

		$image = new Model_Product_Category_Image($drag_id);
		$category = $image->category;

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория перемещаемых элементов не найдена');
		}

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'category_id'=>$category->id)))->execute()
		);
	}


	// удаление изображения
	public function action_del() {

		$id = $this->request->param('id',0);

		$image = new Model_Product_Category_Image($id);

		if (!$image->loaded()) {
			throw new HTTP_Exception_404('Изображение для удаления не найдено');
		}

		$category = $image->category;

		if (!$category->loaded()) {
			throw new HTTP_Exception_404('Категория изображения для удаления не найдена');
		}

		$image->delete();

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'category_id'=>$category->id)))->execute()
		);
	}
}
?>
