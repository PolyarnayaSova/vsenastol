<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Модель: Материал товара
 *
 * @package    Product
 * @category   Model
 * @module		Product
 * @author     ESV Corp. (C) 12.2011
 */

class Model_Product_Material extends ORM {

	protected $_table_name = 'product_materials';


	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}


	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}


	// получение массива для select-box
	public static function array_for_select() {

		$materials = new Model_Product_Material();
		$materials = $materials->order_by('name')->find_all();

		$result = array(0 => '');

		foreach ($materials as $m) {
			$result[$m->id] = $m->name;
		}

		return $result;
	}


	// получение id материала по его названию
	// если материал не существует, он добавляется
	public static function get_id_by_name($name) {

		$name = trim($name);

		if (empty($name)) return 0;

		$alias = Text::translit($name);

		$material = new Model_Product_Material(array('alias'=>$alias));

		if (!$material->loaded()) {
			$material->name = $name;
			$material->save();
		}

		return $material->id;
	}
}
