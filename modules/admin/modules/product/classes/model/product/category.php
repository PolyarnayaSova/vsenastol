<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Модель: Категории товаров
 *
 * @package    Product
 * @category   Model
 * @module		Product
 * @author     ESV Corp. (C) 10.2011
 */

class Model_Product_Category extends Model_Tree_ORM {

	protected $_table_name = 'categories';

	// описание связей
	protected $_has_many = array (
		// подкатегории данного контента
		'categories' => array (
			'model'			=> 'product_category',
			'foreign_key'	=> 'parent_id'
		),
		'products' => array (
			'model'			=> 'product',
			'foreign_key'	=> 'category_id'
		),
		'images' => array (
			'model'			=> 'product_category_image',
			'foreign_key'	=> 'parent_id'
		),
	);

	protected $_belongs_to = array (
		'category' => array (
			'model'			=> 'product_category',
			'foreign_key'	=> 'parent_id'
		)
	);


	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}


	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}


	// используем алиас для названий полей
	// переопределение наименований полей
	public function __get($name) {

		if ($name=='category_id') {
			return parent::__get('parent_id');
		}

		return parent::__get($name);
	}


	// переопределение наименований полей
	public function __set($name, $val) {

		if ($name=='category_id') {
			return parent::__set('parent_id', $val);
		}

		return parent::__set($name, $val);
	}


	// получение id категории по наименованию
	// возможно указание id родительской категории, тогда рассматриваются подкатегории
	public static function get_id_by_name($name,$category_id=0) {

		$name = trim($name);

		if (empty($name)) return 0;

		$alias = Text::translit($name);

		if ($category_id) {
			$parent_category = new Model_Product_Category($category_id);
			if (!$parent_category->loaded()) {
				throw new Kohana_Exception('Невозможно определить родительскую категорию');
			}
		} else {
			$parent_category = new Model_Product_Category();
		}

		if ($parent_category->loaded()) {
			$category = $parent_category->categories->where('alias','=',$alias)->find();
		} else {
			$category = new Model_Product_Category(array('alias'=>$alias));
		}

		if (!$category->loaded()) {

			$fields = array('name');
			$values['name'] = $name;

			$category = $parent_category->add_child($category,$values,$fields);
		}

		return $category->id;
	}

	// получение изображений
	// первое изображение
	public function get_first_image() {
		return Model_Product_Category_Image::element($this->id,1);
	}


	// изображения категорий
	public function get_images($order_by='position') {
		return  Model_Product_Category_Image::elements($this->id);
	}
	

	// удаление
	// с категорией могут быть связаны параметры, изображения
	public function delete() {

		if ($this->loaded()) {

			// удаляем изображения
			$images = $this->get_images();
			if ($images && count($images)>0) {
				foreach ($images as $image) {
					$image->delete();
				}
			}

			parent::delete();
		}
	}
}
