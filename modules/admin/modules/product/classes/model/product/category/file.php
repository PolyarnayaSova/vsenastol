<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Content File model
 *
 * @package    Admin/Content
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// Модель: файл контент-страницы
//

class Model_Content_File extends Model_List_Sorted_Type_File_ORM {

	protected static $_config = 'content.file';	// раздел конфигурации для работы с файлами

	protected $_table_name = 'content_items';

	// описание связей
	protected $_belongs_to = array(
		// контент данного элемента
		'content' => array (
			'model'			=> 'content',
			'foreign_key'	=> 'parent_id'
		)
	);
}
