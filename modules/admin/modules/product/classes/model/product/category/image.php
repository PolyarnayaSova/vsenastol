<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Category Image model
 *
 * @package    Admin/Product
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// Модель: изображение категории
//

class Model_Product_Category_Image extends Model_List_Sorted_Type_Image_ORM {

	protected static $_config = 'product.category.image'; // раздел конфигурации для работы с данным типом элементов

	protected $_table_name = 'category_items';

	// описание связей
	protected $_belongs_to = array(
		// категория данного элемента
		'category' => array (
			'model'			=> 'product_category',
			'foreign_key'	=> 'parent_id'
		)
	);
}
