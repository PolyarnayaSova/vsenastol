<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Product Image model
 *
 * @package    Admin/Content
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// Модель: изображение товара
//

class Model_Product_Image extends Model_List_Sorted_Type_Image_ORM {

	protected static $_config = 'product.product.image'; // раздел конфигурации для работы с данным типом элементов

	protected $_table_name = 'product_items';

	// описание связей
	protected $_belongs_to = array(
		// товар данного элемента
		'product' => array (
			'model'			=> 'product',
			'foreign_key'	=> 'parent_id'
		)
	);
}
