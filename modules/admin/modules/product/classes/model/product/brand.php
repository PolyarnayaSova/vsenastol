<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 *
 * Product Brands
 *
 * @package    Admin/Product
 * @category   Model
 * @author     ESV Corp. (C) 11.2011
 */

//
// Модель: бренд товара
//

class Model_Product_Brand extends ORM {

	protected $_table_name = 'contents';

	// описание связей
	protected $_has_many = array(
		// товары данного бренда
		'products' => array (
			'model'			=> 'product',
			'foreign_key'	=> 'brand_id'
		)
	);

	// получение массива для select-box
	public static function array_for_select() {

		$brands_page = Model_Content::get_by_key('brands');

		if (!$brands_page->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		$brands = $brands_page->contents->order_by('name')->find_all();

		$result = array(0 => '');

		foreach ($brands as $b) {
			$result[$b->id] = $b->name;
		}

		return $result;
	}


	// получение id бренда по его названию
	// если бренд не существует, он добавляется
	public static function get_id_by_name($name) {

		$name = trim($name);

		if (empty($name)) return 0;

		$brands_page = Model_Content::get_by_key('brands');

		if (!$brands_page->loaded()) {
			throw new HTTP_Exception_404('Блок брендов не найден');
		}

		$alias = Text::translit($name);

		$brand = $brands_page->contents->where('alias','=',$alias)->find();

		if (!$brand->loaded()) {

			$fields = array('name');
			$values['name'] = $name;

			$brand = $brands_page->add_child($brand,$values,$fields);
		}

		return $brand->id;
	}
}
