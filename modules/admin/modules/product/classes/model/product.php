<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Модель: Товар
 *
 * @package    Product
 * @category   Model
 * @module		Product
 * @author     ESV Corp. (C) 11.2011
 */


class Model_Product extends ORM {

	protected $_has_many = array (
		'images' => array (
			'model'			=> 'product_image',
			'foreign_key'	=> 'parent_id'
		),
	);

	protected $_belongs_to = array (
		'category' => array (
			'model'			=> 'product_category',
			'foreign_key'	=> 'category_id'
		),
		'brand' => array (
			'model'			=> 'content',
			'foreign_key'	=> 'brand_id'
		),
		'material' => array (
			'model'			=> 'product_material',
			'foreign_key'	=> 'material_id'
		)
	);


	public function __get($name) {

		if ($name=='created') {
			$created = parent::__get($name);
			if ($created && preg_match('/(\d{4})\-(\d{2})\-(\d{2}) (\d{2})\:(\d{2})\:(\d{2})/',$created,$match)) {
				$created = "{$match[3]}.{$match[2]}.{$match[1]} {$match[4]}:{$match[5]}";
			}
			return $created;
		}

		return parent::__get($name);
	}


	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}

	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}


	// получение изображений
	// первое изображение
	public function get_first_image() {
		return Model_Product_Image::element($this->id,1);
	}


	// изображения категорий
	public function get_images($order_by='position') {
		return  Model_Product_Image::elements($this->id,$order_by);
	}


	// получить цену
	public function get_price() {

		$price = $this->price;

		/*if ($this->opt_price > 0 && $this->opt_price >= $price) {
			$price = $this->opt_price + $this->opt_price*0.1;
		}*/

		// надбавка 5% 17.12.2012
		//$price = ceil($price + $price * 0.05);

		return $price;
	}


	// удаление
	public function delete() {

		if ($this->loaded()) {

			// удаляем изображения
			$images = $this->get_images();
			if ($images && count($images)>0) {
				foreach ($images as $image) {
					$image->delete();
				}
			}

			parent::delete();
		}
	}
}
