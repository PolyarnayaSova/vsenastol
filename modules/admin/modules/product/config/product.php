<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Content configuration
 *
 * @package    Admin
 * @module 		Content
 * @category   Config file
 * @author     ESV Corp. (C) 07.2011
 */

	return array (
		// конфигурация для шаблона заголовка страницы:
		// пути, файлы стилей, скрипты
		'head_page' => array (
			'author' => 'ESV Corp. &copy; 2011',
			'path' => array (
				'css'			=> 'include/css/',
				'script'		=> 'include/js/',
				'scripts'	=> 'include/js/',
				'image'		=> 'include/images/',
				'images'		=> 'include/images/'
			),
			'css' => array (
				'product.css'
			),
			'scripts' => array (
			),
			// таблицы стилей и скрипты в корне структуры - DOCROOT
			'root' => array (
				'path' => array (
					'css' 		=> 'include/css/',
					'script'		=> 'include/js/',
					'scripts'	=> 'include/js/',
					'image'		=> 'include/images/',
					'images'		=> 'include/images/'
				),
				'css' => array (
					'jquery-ui-tabs.css',
				),
				'scripts' => array (
				),
			)
		),
		'logo' => 'include/icon/shopcart.png',

		// конфигурация для товаров
		'product' => array (
			// конфигурация для работы с изображениями
			'image' => array (
				'path' 		=> 'upload/product/images/',	// каталог хранения оригиналов
				'path_zip'	=> 'upload/zip/',					// каталог для распаковки архива изображений
				'ext' => array('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'),	// допустимые типы файлов
				'upload_ext'=> array('zip','jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'), // допустимые для загрузки типы файлов
				'stub'		=> 'stub.jpg',
				// изображения в админке
				'admin' => array (
					'path' => 'upload/product/images/100/',
					'size' => 100
				),
				// изображения в списке
				'list' => array (
					'path' => 'upload/product/images/100/',
					'size' => 100
				),
				// изображения в списке
				'icon' => array (
					'path' => 'upload/product/images/64/',
					'size' => 64
				),
				// изображения в инфо, полноразмерные
				'info' => array (
					'path' => 'upload/product/images/300/',
					'size' => 300
				),
			),
			// конфигурация для работы с файлами
			'file' => array (
				'path' 		=> 'upload/product/files/',	// каталог хранения оригиналов
				'path_zip'	=> 'upload/zip/',					// каталог для распаковки
				// доступные для загрузки типы файлов
				'upload_ext'=> array('zip','doc','docx','txt','xls','xlsx','pdf','png','jpg','jpeg','gif','rtf','rar','mp3','mpg','mpeg','avi'),
				// доступные для сохранения файлы
				'ext'			=> array('zip','doc','docx','txt','xls','xlsx','pdf','png','jpg','jpeg','gif','rtf','rar','mp3','mpg','mpeg','avi')
			)
		),

		// конфигурация для категорий
		'category' => array (
			// конфигурация для работы с изображениями
			// пути указаны относительно корня (DOCROOT)
			'image' => array (
				'path' 		=> 'upload/category/images/',	// каталог хранения оригиналов
				'path_zip'	=> 'upload/zip/',					// каталог для распаковки архива изображений
				'ext' => array('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'),	// допустимые типы файлов
				'upload_ext'=> array('zip','jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'), // допустимые для загрузки типы файлов
				'stub'		=> 'stub.jpg',
				// изображения в админке
				'admin' => array (
					'path' => 'upload/category/images/100/',
					'size' => 100
				),
				// изображения в списке
				'list' => array (
					'path' => 'upload/category/images/100/',
					'size' => 100
				),
				// изображения в списке
				'icon' => array (
					'path' => 'upload/category/images/64/',
					'size' => 64
				),
				// изображения в инфо, полноразмерные
				'info' => array (
					'path' => 'upload/category/images/300/',
					'size' => 300
				),
			),

			// конфигурация для работы с файлами
			// пути указаны относительно корня (DOCROOT)
			'file' => array (
				'path' 		=> 'upload/content/files/',	// каталог хранения оригиналов
				'path_zip'	=> 'upload/zip/',					// каталог для распаковки
				// доступные для загрузки типы файлов
				// возможно указание '*' для отмены ограничений
				'upload_ext'=> array('zip','doc','docx','txt','xls','xlsx','pdf','png','jpg','jpeg','gif','rtf','rar','mp3','mpg','mpeg','avi'),
				// доступные для сохранения файлы
				'ext'			=> array('zip','doc','docx','txt','xls','xlsx','pdf','png','jpg','jpeg','gif','rtf','rar','mp3','mpg','mpeg','avi')
			)
		)
	);
?>
