<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Init file
 *
 * @package    Admin/Product_Simple
 * @category   Init file
 * @author     ESV Corp. (C) 12.2011
 */

	Admin::RegisterModule('admin/product','admin_product_category','Товары');

	// изображения категории
	Admin::RouteSet('admin/product','admin_product_category_images','admin/product/category/images(/<action>(/<id>)(/category_<category_id>))',array('id'=>'\d+','category_id'=>'\d+'))
		->defaults(array(
			'controller' => 'admin_product_category_images',
			'action'     => 'index',
		));

	// категории
	Admin::RouteSet('admin/product','admin_product_category','admin/product/category(/<action>(/<id>)(/category_<category_id>)(/add_<add_id>))',array('id'=>'\-*\d+','category_id'=>'\d+','add_id'=>'\d+'))
		->defaults(array(
			'controller' => 'admin_product_category',
			'action'     => 'index',
		));

	// изображения товара
	Admin::RouteSet('admin/product','admin_product_images','admin/product/images(/<action>(/<id>)(/product_<product_id>))',array('id'=>'\d+','product_id'=>'\d+'))
		->defaults(array(
			'controller' => 'admin_product_images',
			'action'     => 'index',
		));

	// товары
	Admin::RouteSet('admin/product','admin_products','admin/product(/<action>(/<id>)(/category_<category_id>)(/ts_<time>))',array('id'=>'\d+','category_id'=>'\-*\d+','time'=>'\d+'))
		->defaults(array(
			'controller' => 'admin_products',
			'action'     => 'index',
		));
?>
