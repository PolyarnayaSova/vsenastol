<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * News page template
 *
 * @package    Admin/News
 * @module		News
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */
?>
<div class="listHeader">Полезные советы</div>

<div class="list">

	<?php if (count($advices)>0): ?>

	<table border="0" cellpadding="0" cellspacing="0" width="800" cols="4">
		<th>заголовок</th>
		<th>&nbsp;</th>
		<?php
			foreach($advices as $a):
		?>
		<tr>
			<td class="info"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','id'=>$a->id)),$a->name); ?></td>
			<td width="20" class="last"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'del' ,'id'=>$a->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данный совет?');")); ?></td>
		</tr>
		<?php
			endforeach;
		?>
	</table>

	<?php else: ?>
	<p>советов нет</p>
	<?php endif; ?>

	<div class="submit">
		<div class="margin"></div>
		<?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','time'=>(date('is') . rand(100,999)))),'добавить'); ?>
		<div class="delimiter">|</div>
		<?php print HTML::anchor(Route::url('admin_content',array('action'=>'editcontent','id'=>$content_id,'time'=>(date('is') . rand(100,999)))),"страница 'Советы'"); ?>
	</div>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager">
	<?php print  $pager; ?>
	</div>
	<?php endif; ?>

</div>
