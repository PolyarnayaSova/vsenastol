<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Advice template
 *
 * @package    Admin/Content
 * @module		News
 * @category   Template
 * @author     ESV Corp. since 10.2011
 */

	if ($advice->loaded()) {
		$caption = 'Редактировать совет';
		$submit = 'сохранить';
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$advice->id));
	} else {
		$caption = 'Добавить совет';
		$submit = 'добавить';
		$form = Route::url(Admin::route(),array('action'=>'save'));
		$advice->visible = 1;
	}
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST')); ?>

	<div class="form">

		<div class="caption"><?php print $caption; ?></div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Заголовок</div>
					<?php print Form::input('name',$advice->name,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Кратко</div>
					<?php print Form::textarea('brief',$advice->brief,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<table><tr><td>
					<div class="label">Текст</div>
					</td><td>
					<?php print Form::textarea('text',$advice->text,array('cols'=>80,'rows'=>8,'id'=>'wysiwyg')); ?>
					</td></tr></table>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Отображать</div>
					<?php print Form::checkbox('visible', 1, $advice->visible!=0); ?>
				</label>
			</div>

		</fieldset>

		<div class="group">SEO</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Title</div>
					<?php print Form::input('title',$advice->title,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Keywords</div>
					<?php print Form::textarea('keywords',$advice->keywords,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Description</div>
					<?php print Form::textarea('description',$advice->description,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('save',$submit); ?>
	</div>

	<?php print Form::close(); ?>

</div>

<script language="javascript">
<!--
	// функция определена в главном модуле админки
	// инициализация WYSIWYG
	if (typeof window['ckeditor_start'] == 'function') {
		ckeditor_start('wysiwyg');
	}
-->
</script>
