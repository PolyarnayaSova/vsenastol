<?php defined('SYSPATH') or die('No direct script access.');
//
// @uthor ESV Corp. (C) 2011
//
// Модель: советы
//
class Model_Advice extends ORM {

	protected $_table_name = 'advices';

	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}


	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}
}
?>
