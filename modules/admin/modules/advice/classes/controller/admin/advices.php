<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 *
 * News controller
 *
 * @package    Admin/Advices
 * @category   Module
 * @author     ESV Corp. (C) 08.2012
 */
class Controller_Admin_Advices extends Controller_Admin {

	private function return_back() {
		// возврат к списку советов
		$this->request->redirect(Route::url(Admin::route()));
	}


	public function before() {
		parent::before();

		// модуль будет использоваться в ajax-контенте, поэтому не отрисовываем шаблон
		$this->auto_render = false;
	}


	public function action_index() {

		$content_id = $this->request->param('content_id',0);

		$advices = new Model_Advice();
		$advices->order_by('id','DESC');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.admin_advices');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($advices->reset(false)->find_all());

		$advices = $advices->offset($offset)->limit($limit)->find_all();

		$this->response->body(
			View::factory('admin/advices/advices')
				->set('advices',$advices)
				->set('content_id',$content_id)
				->set('pager',$pager)
		);
	}


	// редактирование или добавление новости
	public function action_edit() {

		$id = $this->request->param('id', 0);

		$advice = new Model_Advice($id);

		$this->response->body(
			View::factory('admin/advices/edit')
				->set('advice', $advice)
		);
	}


	// сохранить
	public function action_save() {

		$id = $this->request->param('id',0);

		$_POST['visible'] = Arr::get($_POST,'visible',0);

		$advice = new Model_Advice($id);
		$fields = array_keys($advice->list_columns());

		$advice->values($_POST,$fields);
		$advice->save();

		// возврат к списку новостей
		$this->return_back();
	}


	// удалить
	public function action_del() {

		$id = $this->request->param('id',0);

		$advice = new Model_Advice($id);

		if ($advice->loaded()) {
			$advice->delete();
		}

		// возврат к списку новостей
		$this->return_back();
	}
}
