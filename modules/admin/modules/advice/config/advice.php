<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * News configuration
 *
 * @package    Admin
 * @module 		News
 * @category   Config file
 * @author     ESV Corp. since 07.2011
 */

	return array (
		// конфигурация для шаблона заголовка страницы:
		// пути, файлы стилей, скрипты
		'head_page' => array (
			'author' => 'ESV Corp. &copy; 2011',
			'path' => array (
				'css'			=> 'include/css/',
				'script'		=> 'include/js/',
				'scripts'	=> 'include/js/',
				'image'		=> 'include/images/',
				'images'		=> 'include/images/'
			),
			'css' => array (
			),
			'scripts' => array (
			)
		),
		'logo' => 'include/icon/news.png'
	);
?>
