<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Init file
 *
 * @package    Admin/Advice
 * @category   Init file
 * @author     ESV Corp. (C) 08.2012
 */

// при регистрации внутреннего модуля заголовок не указывается - модуль не попадает в меню
// доступ осуществляется через контент-страницы
Admin::RegisterModule('admin/advice','admin_advice');

Admin::RouteSet('admin/advice','admin_advice','admin/advices(/<action>(/<id>)(/content_<content_id>)(/ts_<time>))', array('id'=>'\d+','content_id'=>'\d+','time'=>'\d+'))
	->defaults(array(
		'controller' => 'admin_advices',
		'action'     => 'index',
	));
?>
