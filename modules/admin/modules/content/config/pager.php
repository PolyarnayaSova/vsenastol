<?php defined('SYSPATH') or die('No direct script access.');

	return array(

		'admin_gallery' => array (
			'total_items'		=> 0,
			'items_per_page'	=> 0, // устанавливается через Setting
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			//'view'		=> 'block/pager/floating'
		),

		'products' => array(
			'total_items'		=> 0,
			'items_per_page'	=> 20,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE
			// 'view'		=> 'block/pagination'
		),

		'orders' => array(
			'total_items'		=> 0,
			'items_per_page'	=> 20,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE
			// 'view'		=> 'block/pagination'
		),

		'discounts' => array(
			'total_items'		=> 0,
			'items_per_page'	=> 30,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE
			// 'view'		=> 'block/pagination'
		)
	);
?>