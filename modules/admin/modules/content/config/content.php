<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Content configuration
 *
 * @package    Admin
 * @module 		Content
 * @category   Config file
 * @author     ESV Corp. (C) 07.2011
 */

	return array (
		// конфигурация для шаблона заголовка страницы:
		// пути, файлы стилей, скрипты
		'head_page' => array (
			'author' => 'ESV Corp. &copy; 2011',
			'path' => array (
				'css'			=> 'include/css/',
				'script'		=> 'include/js/',
				'scripts'	=> 'include/js/',
				'image'		=> 'include/images/',
				'images'		=> 'include/images/'
			),
			'css' => array (
				'content.css',
			),
			'scripts' => array (
			),
			// таблицы стилей и скрипты в корне структуры - DOCROOT
			'root' => array (
				'path' => array (
					'css' 		=> 'include/css/',
					'script'		=> 'include/js/',
					'scripts'	=> 'include/js/',
					'image'		=> 'include/images/',
					'images'		=> 'include/images/'
				),
				'css' => array (
					'jquery-ui-tabs.css',
				),
				'scripts' => array (
				),
			)
		),
		'logo' => 'include/icon/content.png',

		// конфигурация для работы с изображениями
		// пути указаны относительно корня (DOCROOT)
		'image' => array (
			'path' 		=> 'upload/content/images/',	// каталог хранения оригиналов
			'path_zip'	=> 'upload/zip/',					// каталог для распаковки архива изображений
			'ext' => array('jpg','jpeg','png','gif'),	// допустимые типы файлов
			'upload_ext'=> array('zip','jpg','jpeg','png','gif'), // допустимые для загрузки типы файлов
			'stub'		=> 'stub.jpg',
			// изображения в админке
			'admin' => array (
				'path' => 'upload/content/images/100/',
				'size' => 100
			),
			// изображения в списке
			'list' => array (
				'path' => 'upload/content/images/100/',
				'size' => 100
			),
			// изображения в инфо, полноразмерные
			'info' => array (
				'path' => 'upload/content/images/300/',
				'size' => 300
			),
			// полноразмерные изображения
			'full' => array (
				'path' => 'upload/content/images/600/',
				'size' => array(600,400)
			),
			// изображения в списке туров
			'tour-list' => array (
				'path' => 'upload/content/images/tour_150x100/',
				'size' => array(150,100)
			),
			// изображения в списке галерей
			'gallery' => array (
				'path' => 'upload/content/images/gallery_150x100/',
				'size' => array(150,100)
			),
		),

		// конфигурация для работы с файлами
		// пути указаны относительно корня (DOCROOT)
		'file' => array (
			'path' 		=> 'upload/content/files/',	// каталог хранения оригиналов
			'path_zip'	=> 'upload/zip/',					// каталог для распаковки
			// доступные для загрузки типы файлов
			// возможно указание '*' для отмены ограничений
			'upload_ext'=> array('zip','doc','docx','txt','xls','xlsx','pdf','png','jpg','jpeg','gif','rtf','rar','mp3','mpg','mpeg','avi'),
			// доступные для сохранения файлы
			'ext'			=> array('zip','doc','docx','txt','xls','xlsx','pdf','png','jpg','jpeg','gif','rtf','rar','mp3','mpg','mpeg','avi')
		)
	);
?>
