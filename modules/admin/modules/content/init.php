<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 * Init file
 *
 * @package    Admin/Content
 * @category   Init file
 * @author     ESV Corp. since 08.2011
 */

	Admin::RegisterModule('admin/content','admin_content','Контент');

	// примечание: параметр 'back' используется для возможности отображения ссылки возврата, когда контент-страницей управляет
	// отдельный модуль, т.е. возврат к редактированию родительской контент-страницы, обязательно должен быть определен 'content_id'
	Admin::RouteSet('admin/content','admin_content','admin/content(/<action>(/<id>)(/content_<content_id>)(add_<add_id>)(/ts_<time>)(/<back>))',array('id'=>'\d+','content_id'=>'\d+','add_id'=>'\d+','time'=>'\d+','back'=>'back'))
		->defaults(array(
			'controller' => 'admin_content',
			'action'     => 'index',
		));

	// работа с изображениями
	Admin::RouteSet('admin/content','admin_content_images','admin/content_image(/<action>(/<id>)(/content_<content_id>)(/ts_<time>))',array('id'=>'\d+','content_id'=>'\d+','time'=>'\d+'))
		->defaults(array(
			'controller' => 'admin_content_images',
			'action'     => 'index',
		));

	// работа с файлами
	Admin::RouteSet('admin/content','admin_content_files','admin/content_file(/<action>(/<id>)(/content_<content_id>)(/ts_<time>))',array('id'=>'\d+','content_id'=>'\d+','time'=>'\d+'))
		->defaults(array(
			'controller' => 'admin_content_files',
			'action'     => 'index',
		));
?>
