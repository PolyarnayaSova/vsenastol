<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Main template
 *
 * @package    Admin
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */
?>
<div class="wrap_ajax_body">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" cols="2">
		<tr>
			<td width="300" align="left" valign="top">
				<div class="tree_contents">
					<div id="tree"></div>
				</div>
			</td>
			<td align="left" valign="top">
				<div class="wrap_content_ajax">
					<div class="content_ajax" id="content_ajax"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
