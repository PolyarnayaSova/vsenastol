<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Content Images template
 *
 * @package    Admin/Content/Images
 * @module		Admin/Content
 * @category   Template
 * @author     ESV Corp. since 11.2011
 */

	$max_files = ini_get('max_file_uploads');

	const COLUMNS = 7;
	const IMAGE_CONTEXT = 'DIV#images'; // блок для вывода изображений

	$allow_upload = implode(', ',Kohana::$config->load('content.image.upload_ext')); // файлы, доступные для загрузки

	$form = Route::url(Admin::route(),array('action'=>'add','content_id'=>$content->id));
?>

<?php
	// если есть сообщение
	if ($message) { print $message; }
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST','enctype' => 'multipart/form-data', 'context'=> IMAGE_CONTEXT)); ?>

	<div class="form">

		<div class="caption">Изображения</div>

		<div class="list_items"><div class="list_images">

			<?php if (($count=count($images))>0): ?>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" cols="<?php print COLUMNS; ?>">

				<?php
					$col = 0;
					foreach ($images as $image):

						if ($col==0) {
							print '<tr>';
						}

						$col++;
				?>
				<td width="<?php print intval(100/COLUMNS) . "%"; ?>" align="center" valign="middle">
					<div class="wrap_image">
						<div class="image">
							<?php
								print HTML::anchor(
											Route::url(Admin::route(),array('action'=>'del','id'=>$image->id)),
											'',
											array(
												'class'=>'delete',
												'onclick'=>"return confirm('Вы действительно хотите удалить данное изображение?');",
												'context' => IMAGE_CONTEXT
											)
										);
							?>
							<div class="middle0"><div class="middle1"><div class="middle2">
							<?php print HTML::image($image->path('admin'),array('alt'=>$image->name,'image_id'=>$image->id,'dragdrop'=>'dragdrop')); ?>
							</div></div></div>
						</div>
						<div class="title">
							<?php	print $image->name; ?>
						</div>
					</div>
				</td>

				<?php
						// если отрисовали последнее изображение, тогда заканчиваем ряд колонок
						if (--$count==0) {
							print str_repeat("<td>&nbsp;</td>\n",COLUMNS-$col);
							$col = COLUMNS;
						}

						// если последняя колонка в строке
						if ($col==COLUMNS) {
							print '</tr>';
							$col = 0;
						}
					endforeach;
				?>
			</table>

			<?php else: ?>

			<p>изображений нет</p>

			<?php endif; ?>

		</div></div>

		<div class="small_caption">Добавить изображение</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Название</div>
					<div class="input">
					<?php print Form::input('comment','',array('size'=>40)); ?>
					</div>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Изображение</div>
					<div class="input">
					<?php print Form::file('image[]',array('title'=>"Доступны для загрузки: $allow_upload. Одовременно до $max_files файлов.",'multiple'=>'multiple')); ?>
					</div>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('save','добавить'); ?>
	</div>

	<?php print Form::close(); ?>

</div>

<script language="javascript">
<!--
if (window.$) {
	$(document).ready(function(){

		$('IMG[dragdrop]').draggable({
			zIndex: 1000,
			stack: 'IMG[dragdrop]',
			revert: 'invalid',
			scrooll: false,
			containment: 'DIV.list_images',
			distance: 20,
			cursor: "move",
			cursorAt: { left: 50, top: 50 }
		});

		$('IMG[dragdrop]').droppable({
			accept: 'IMG[dragdrop]',
			drop: function(event,ui) {

				var content = $('DIV#images'); // сначала определим контент для загрузки

				if (content.length) {

					var drag_id = $(ui.draggable).attr('image_id');
					var drop_id = $(this).attr('image_id');

					var params = {
						'drag_id':	drag_id,
						'drop_id':	drop_id
					};

					$(ui.draggable).draggable({
						// только после завершения операции делаем ajax-запрос на перемещение
						stop: function(){

							$.ajax({
								url: '/admin/content_image/move',
								type: 'post',
								async: false,
								cache: false,
								dataType: 'html',		// ответ сервера
								data: params,
								complete: function() {
								},
								success: function(response, textStatus, jqXHR) {
									//$('ui.draggable').removeClass("ui-draggable");
									//$(this).droppable("destroy");
									//$(ui.draggable).draggable("destroy");
									content.html(response);
								}
							});
						}
					});
				}
			}
		});

		$('IMG[dragdrop]').disableSelection();

	});
}
-->
</script>
