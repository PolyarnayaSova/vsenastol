<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit Content page template
 *
 * @package    Admin/Content
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */

	if (!isset($back)) $back = null;

	if ($content->loaded()) {
		$new = false;
		$caption = 'Редактировать страницу';
		$submit = 'сохранить';
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$content->id,'back'=>$back));
	} else {
		$new = true;
		$caption = 'Добавить страницу';
		$submit = 'добавить';
		$form = Route::url(Admin::route(),array('action'=>'save','content_id'=>$content_id,'back'=>$back));
		$content->visible = 1;
	}


	// исключенные компоненты:
	// файлы:
	/*
	// <li><a href="#files">файлы</a><?php if ($count_files): ?><div class="count"><?php // print "($count_files)"; ?></div><?php endif; ?></li>
	*/
?>


<?php
	// загрузка файлов и изображений только для существующей страницы
	if (!$new):
?>
<div class="tabs">

	<ul>
		<li><a href="#content">страница</a></li>
		<li><a href="#images">изображения</a><?php if ($count_images): ?><div class="count"><?php print "($count_images)"; ?></div><?php endif; ?></li>
	</ul>

	<div class="tab" id="content">
<?php endif; ?>

		<?php
			// если есть сообщение
			if ($message) { print $message; }
		?>

		<div class="wrap_form">

			<?php print Form::open($form,array('method'=>'POST')); ?>

			<div class="form">

				<?php
					// возможность указать, что можно вернуться к родительской контент-странице
					if (isset($back) && !empty($back) && $content_id) {
						print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','id'=>$content_id)),'',array('class'=>'back-arrow','title'=>'вернуться'));
					}
				?>

				<div class="caption"><?php print $caption; ?></div>

				<fieldset>

					<div class="field">
						<label>
							<div class="label">Наименование</div>
							<?php print Form::input('name',$content->name,array('size'=>80)); ?>
							<?php print Form::hidden('name-changed','0'); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<div class="label">Кратко</div>
							<?php print Form::textarea('brief',$content->brief,array('cols'=>80,'rows'=>4)); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<table><tr><td>
							<div class="label">Текст</div>
							</td><td>
							<?php print Form::textarea('text',$content->text,array('cols'=>80,'rows'=>8,'id'=>'wysiwyg')); ?>
							</td></tr></table>
						</label>
					</div>

				</fieldset>

				<?php if (Admin::logged_in(Kohana::$config->load('admin.privileges.admin'))): ?>

				<div class="group">Special</div>

				<fieldset>

					<div class="field">
						<label>
							<div class="label">key</div>
							<?php print Form::input('key',$content->key,array('size'=>20)); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<div class="label">visible</div>
							<?php print Form::checkbox('visible', 1, $content->visible!=0); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<div class="label">system</div>
							<?php print Form::checkbox('system', 1, $content->system!=0); ?>
						</label>
					</div>

				</fieldset>

				<?php endif; ?>

				<?php if (Admin::logged_in(Kohana::$config->load('admin.privileges.seo'))): ?>

				<div class="group">SEO</div>

				<fieldset>

					<div class="field">
						<label>
							<div class="label">Title</div>
							<?php print Form::input('title',$content->title,array('size'=>80)); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<div class="label">Keywords</div>
							<?php print Form::textarea('keywords',$content->keywords,array('cols'=>80,'rows'=>4)); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<div class="label">Description</div>
							<?php print Form::textarea('description',$content->description,array('cols'=>80,'rows'=>4)); ?>
						</label>
					</div>

					<div class="field">
						<label>
							<div class="label">URL alias</div>
							<?php print Form::input('alias',$content->alias,array('size'=>80)); ?>
							<?php print Form::hidden('alias-changed','0'); ?>
						</label>
					</div>

				</fieldset>

				<?php else:
							// сохранение seo-aliasa при изменении имени не seo-пользователем
							// только в том случае, если alias уже существует
							if (!empty($content->alias)) {
								print Form::hidden('alias',$content->alias);
							}
						endif;
				?>

			</div>

			<div class="submit">
				<?php print Form::submit('save',$submit); ?>
			</div>

			<?php print Form::close(); ?>

		</div>

<?php
	// загрузка файлов и изображений только для существующей страницы
	if (!$new):
?>

	</div>

	<div class="tab" id="images">
		<?php print Request::factory(Route::url('admin_content_images',array('action'=>'index','content_id'=>$content->id)))->execute(); ?>
	</div>

	<?php if (false): ?>
	<div class="tab" id="files">
		<?php print Request::factory(Route::url('admin_content_files',array('action'=>'index','content_id'=>$content->id)))->execute(); ?>
	</div>
	<?php endif; ?>

</div>

<?php endif; ?>

<script language="javascript">
<!--
	if (typeof window['ckeditor_start'] == 'function') {
		ckeditor_start('wysiwyg');
	}

	if (window['$']) {

		$(document).ready(function(){

			$('DIV.tabs').tabs();

			// обработка ввода алиаса - приоритет перед генерируемым
			$('INPUT[name="name"]').change(function(){
				$('INPUT[name="name-changed"]').val(1);
			});

			$('INPUT[name="alias"]').change(function(){
				$('INPUT[name="alias-changed"]').val(1);
			});
		});

	}
-->
</script>
