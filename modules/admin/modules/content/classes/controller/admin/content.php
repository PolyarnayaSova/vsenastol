<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 *
 * Content pages controller
 *
 * @package    Admin
 * @content   Module
 * @module		Admin/Content
 * @author     ESV Corp. (C) 12.2011
 */

// контроллер для работы с деревом Tree, реализует интерфейс Controller_Tree_Interface
// необходима раелизация следующих методов:
// action_tree(POST[node])	- получение элементов дерева
// action_get/id				- получение контента определенного элемента
// action_add/id				- добавление потомка к элементу
// action_del/id				- удаление элемента
// action_move(POST[drag_id,drop_id,point]) - перемещение элемента
//

class Controller_Admin_Content extends Controller_Admin implements Controller_Tree_Interface {


	public function action_index() {

		// инициализация дерева
		Tree::init(
			0,							//	root_id		- id корня дерева
			'Контент',				// root_name	- наименование корня дерева
			300,						// width			- ширина
			650,						// height		- высота
			Route::url(Admin::route()),
			'content_tree_content'	// id DIV-а в который будет загружаться контент по ajax
											// установлены стили в content.css
		);

		parent::scripts(Tree::scripts(),DOCROOT);
		parent::css(Tree::css(),DOCROOT);

		$this->content = Tree::content();
	}


	// загрузка дерева в элемент ExtJS - дерево контент-страниц
   public function action_tree() {

		$this->auto_render = false;

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

      $id = Arr::get($_POST, 'node', 0);

      $result = array();

		$node = new Model_Content(intval($id));

		$tree = $node->children();

		// формируем массив для передачи в виде JSON
		foreach ($tree as $node) {

			$is_leaf = $node->is_leaf();

			// контент-страница считается так же конечной (листом), когда у нее определен роут для работы
			// с данной контент-страницей, т.е. дочерние элементы данного контента обрабатываются самим
			// модулем
			if (($key = Content_Key::get($node)) && !empty($key) && isset($key['route'])) {
				$is_leaf = true;
			}

			// если ключевая страница, проверяем на возможность удаления
			// иначе - проверка только наличия потомков
			$allowDelete = ($is_leaf && (empty($key) || (isset($key['allow_delete']) && $key['allow_delete']===true)));

			if (empty($node->key)) {
				$node_array = array('id' => $node->id,
										  'parent_id' => $node->content_id,
										  'text' => $node->name,
										  'key'=> $node->key,
										  'leaf' => $is_leaf,
										  'allowDelete' => $allowDelete);
			} else {
				$node_array = array('id' => $node->id,
										  'parent_id' => $node->content_id,
										  'text' => $node->name,
										  'key'=> $node->key,
										  'leaf' => $is_leaf,
										  'allowDelete' => $allowDelete,
										  'cls' => 'node_bold'); // выделение ключевых элементов
			}

			$result[] = $node_array;
		}

      $this->response->body(json_encode($result));
   }


	// получение элемента - страница категории
	public function action_get() {

		$id = $this->request->param('id',0);

		// HMVC
		$this->auto_render = false;
		if (!empty($id)) {
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)))->execute()
			);
		} else {
			$this->response->body('');
		}
	}


	// добавить элемент - страницу категории
	public function action_add() {

		// переход на страницу редактирования
		$content_id = $this->request->param('id',0);

		// HMVC
		$this->auto_render = false;
		$this->response->body(
			Request::factory(Route::url(Admin::route(),array('action'=>'edit','content_id'=>$content_id)))->execute()
		);
	}


	// удалить страницу
	public function action_del() {

		$id = $this->request->param('id',0);

		$content = new Model_Content($id);

		if (!$content->loaded()) {
			throw new HTTP_Exception_404('Контент-страница для удаления не найдена');
		}

		$parent_content = $content->content;

		$key = Content_Key::get($content);

		if ($content->is_leaf() && (empty($key) || (isset($key['allow_delete']) && $key['allow_delete']===true))) {
			$deleted = true;
			$content->delete();
		} else {
			$deleted = false;
		}

		$this->auto_render = false;

		if ($deleted) {
			// если удалена, переход к редактированию родительского элемента
			if ($parent_content->loaded()) {
				$this->response->body(
					Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$parent_content->id)))->execute()
				);
			} else {
				// либо отображение пустой страницы, если родительский элемент - корень
				$this->response->body('');
			}
		} else {
			// иначе - возврат к редактированию контент-страницы
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$content->id)))->execute()
			);
		}
	}


	// переместить старницу
	public function action_move() {

		$this->auto_render = false;

		if (!($this->request->method()=='POST' && $this->request->is_ajax())) {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$success = array (
			'success' => true,
			'msg' => "Ok"
		);

		$error = array (
			'success' => false,
			'msg' => "no move"
		);

		$drag_id = Arr::get($_POST,'drag_id');
		$drop_id = Arr::get($_POST,'drop_id');
		$point = Arr::get($_POST,'point');

		if (Model_Content::move($drag_id,$drop_id,$point)) {
			$this->response->status(200);
			$this->response->body(json_encode($success));
		} else {
			$this->response->status(400);
			$this->response->body(json_encode($error));
		}
	}


	// добавить, редактировать контент-страницу, с возможностью перехода по ключевой странице
	public function action_edit() {

		$id = $this->request->param('id',0);
		$content_id = $this->request->param('content_id',0);
		$add_id = $this->request->param('add_id',0); // возможен переход к редактированию, когда добавлен элемент
																	// см. в коде шаблона редактирования добавление к дереву
		$back = $this->request->param('back');	// отображать ссылку 'вернуться', используется если в контент страница
															// используется под управлением другого модуля

		$content = new Model_Content($id);

		$count_images = 0;
		$count_files = 0;

		if ($content->loaded()) {

			// проверка на ключевую контент-страницу
			if (($key = Content_Key::get($content)) && !empty($key) && isset($key['route'])) {

				// получаем роут и action для перехода
				list($route,$action) = Content_Key::get_route_action($key);

				// передаем управление другому модулю, в качестве ключа передаем id контента
				$this->request->redirect(Route::url($route,array('action'=>$action,$key['id']=>$content->id)));

				return;
			}

			$content_id = $content->parent_id;

			$count_images = $content->images->where('type','=',Model_Content_Image::type())->count_all();
			$count_files = $content->files->where('type','=',Model_Content_File::type())->count_all();
		}

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->auto_render = false;
		$content_page =
			View::factory('admin/content/edit')
				->set('content',$content)
				->set('content_id',$content_id)
				->set('count_images',$count_images)
				->set('count_files',$count_files)
				->set('message',$message)
				->set('status',$status)
				->set('back',$back);

		// если добавлен новый элемент, присоединим блок скрипта для добавления элемента в дерево
		if (!empty($add_id)) {
			$content_page .= Tree::content_add_node($add_id);
		}

		$this->response->body($content_page);
	}


	// непосредственное редактирование страницы, без учета ключевых страниц
	public function action_editcontent() {

		$id = $this->request->param('id',0);
		$content = new Model_Content($id);
		if (!$content->loaded()) {
			throw new HTTP_Exception_404('Контент-страница не найдена');
		}

		$count_images = $content->images->where('type','=',Model_Content_Image::type())->count_all();
		$count_files = $content->files->where('type','=',Model_Content_File::type())->count_all();

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->auto_render = false;
		$this->response->body(
			View::factory('admin/content/edit')
				->set('content',$content)
				->set('content_id',$content->content_id)
				->set('count_images',$count_images)
				->set('count_files',$count_files)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// сохранить страницу
	public function action_save() {

		$this->auto_render = false;

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$id = $this->request->param('id',0);
		$content_id = $this->request->param('content_id',0);
		$back = $this->request->param('back');

		// поля, доступные для привилегии администратора
		if (Admin::logged_in(Kohana::$config->load('admin.privileges.admin'))) {
			$_POST['system'] = Arr::get($_POST,'system',0);
			$_POST['visible'] = Arr::get($_POST,'visible',0);
		} else {
			if (isset($_POST['system'])) unset($_POST['system']);
			if (isset($_POST['visible'])) unset($_POST['visible']);
		}

		// поля, доступные для seo-менеджера
		if (Admin::logged_in(Kohana::$config->load('admin.privileges.seo'))) {

			// обработка алиаса
			// изменение имени имеет приоритет, если алиас не менялся
			if (Arr::get($_POST['name-changed'],false) && !Arr::get($_POST,'alias-changed',false)) {
				if (isset($_POST['alias'])) unset($_POST['alias']);
			} else {
				$alias = trim(Arr::get($_POST,'alias',''));
				//$alias = preg_replace('/[\W\s]+/','',Arr::get($_POST,'alias',''));
				if (empty($alias)) {
					if (isset($_POST['alias'])) unset($_POST['alias']);
				} else {
					$_POST['alias'] = $alias;
				}
			}

		} else {
			if (isset($_POST['title'])) unset($_POST['title']);
			if (isset($_POST['keywords'])) unset($_POST['keywords']);
			if (isset($_POST['description'])) unset($_POST['description']);
		}

		$content = new Model_Content($id);

		$fields = array_keys($content->list_columns());

		if ($content->loaded()) {

			$new = false;

			$content->values($_POST,$fields);
			$content->save();

		} else {

			$new = true;

			if (empty($content_id)) {

				// добавим в корень
				$parent_content = new Model_Content();

			} else {

				$parent_content = new Model_Content($content_id);

				if (!$parent_content->loaded()) {
					throw new HTTP_Exception('Родительская контент-страница не найдена');
				}

				// обработка установки значения ключа дочерних элементов
				if (($key = Content_Key::get($parent_content)) && !empty($key) && isset($key['child_key'])) {
					$_POST['key'] = $key['child_key'];
				}
			}

			// добавим дочерний элемент
			$content = $parent_content->add_child($content,$_POST,$fields);
		}

		if ($new) {
			// переход к редактированию нового контента
			if ($back) {
				// без отработки добавления нового элемента в дерево
				$this->response->body(
					Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$content->id,'back'=>$back)))->execute()
				);
			} else {
				$this->response->body(
					Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$content->id,'add_id'=>$content->id,'back'=>$back)))->execute()
				);
			}
		} else {
			// возврат к редактированию контента
			$this->response->body(
				Request::factory(Route::url(Admin::route(),array('action'=>'edit','id'=>$content->id,'back'=>$back)))->execute()
			);
		}
	}
}
