<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Content pages images controller
 *
 * @package    Admin
 * @category   Module
 * @module		Admin/Content/Files
 * @author     ESV Corp. since 11.2011
 */



class Controller_Admin_Content_Images extends Controller_Admin {


	public function before() {
		parent::before();

		$this->auto_render = false;
	}


	public function action_index() {

		$content_id = $this->request->param('content_id',0);

		$content = new Model_Content($content_id);

		if (!$content->loaded()) {
			throw new HTTP_Exception_404('Контент-страница для загрузки изображений не определена');
		}

		$images = $content->images->where('type','=',Model_Content_Image::type())->order_by('position')->find_all();

		// получаем блок сообщения
		list($message,$status) = parent::message();

		$this->response->body(
			View::factory('admin/content/images/images')
				->set('content',$content)
				->set('images',$images)
				->set('message',$message)
				->set('status',$status)
		);
	}


	// загрузка изображения контент-страницы
	public function action_add() {

		if ($this->request->method()!='POST') {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$name = Arr::get($_POST,'comment','');

		$content_id = $this->request->param('content_id',0);

		$content = new Model_Content($content_id);

		if (!$content->loaded()) {
			throw new Kohana_Exception('Контент-страница для загрузки изображения не определена');
		}

		$file = Arr::get($_FILES,'image',array());

		if (!($count=Model_Content_Image::load($file,$content->id,$name))) {
			Admin::message('Изображение не загружено',Admin::MSG_WARNING);
		} elseif ($count>1) {
			Admin::message("Загружено изображений: $count",Admin::MSG_WARNING);
		}

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'content_id'=>$content->id)))->execute()
		);
	}


	// перемещение изображения в новую позицию
	public function action_move() {

		if ($this->request->method()!='POST' || !$this->request->is_ajax()) {
			$this->response->status(400);
			$this->response->body('Bad request');
			return;
		}

		$drag_id = $this->request->post('drag_id'); // перемещаемый элемент
		$drop_id = $this->request->post('drop_id'); // элемент, на который переместили

		Model_Content_Image::move($drag_id,$drop_id);

		$image = new Model_Content_Image($drag_id);
		$content = $image->content;

		if (!$content->loaded()) {
			throw new HTTP_Exception_404('Контент-страница перемещаемых элементов не найдена');
		}

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'content_id'=>$content->id)))->execute()
		);
	}


	// удаление изображения
	public function action_del() {

		$id = $this->request->param('id',0);

		$image = new Model_Content_Image($id);

		if (!$image->loaded()) {
			throw new HTTP_Exception_404('Изображение для удаления не найдено');
		}

		$content = $image->content;

		if (!$content->loaded()) {
			throw new HTTP_Exception_404('Контент-страница изображения для удаления не найдена');
		}

		$image->delete();

		// выводим страницу списка изображений
		$this->response->body(
			Request::factory(Route::url(Admin::route(), array('action'=>'index', 'content_id'=>$content->id)))->execute()
		);
	}
}
?>
