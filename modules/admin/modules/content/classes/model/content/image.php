<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Content Image model
 *
 * @package    Admin/Content
 * @category   Model
 * @author     ESV Corp. since 11.2011
 */

//
// Модель: изображение контент-страницы
//

class Model_Content_Image extends Model_List_Sorted_Type_Image_ORM {

	protected static $_config = 'content.image'; // раздел конфигурации для работы с данным типом элементов

	protected $_table_name = 'content_items';

	// описание связей
	protected $_belongs_to = array(
		// контент данного элемента
		'content' => array (
			'model'			=> 'content',
			'foreign_key'	=> 'parent_id'
		)
	);
}
