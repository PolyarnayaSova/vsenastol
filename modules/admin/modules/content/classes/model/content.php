<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Модель: контент-страницы
 *
 * @package    Content
 * @category   Model
 * @module		Content
 * @author     ESV Corp. since 10.2011
 */

class Model_Content extends Model_Tree_ORM {

	// описание связей
	protected $_has_many = array (
		// подкатегории данного контента
		'contents' => array (
			'model'			=> 'content',
			'foreign_key'	=> 'parent_id'
		),
		// изображения контент-страницы
		'images' => array(
			'model'			=> 'content_image',
			'foreign_key'	=> 'parent_id'
		),
		// файлы контент-страницы
		'files' => array(
			'model'			=> 'content_file',
			'foreign_key'	=> 'parent_id'
		),
	);

	protected $_belongs_to = array (
		'content' => array (
			'model'			=> 'content',
			'foreign_key'	=> 'parent_id'
		)
	);


	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}


	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}


	// используем алиас для названий полей
	// переопределение наименований полей
	public function __get($name) {

		if ($name=='content_id') {
			return parent::__get('parent_id');
		}

		return parent::__get($name);
	}


	// переопределение наименований полей
	public function __set($name, $val) {

		if ($name=='content_id') {
			return parent::__set('parent_id', $val);
		}

		return parent::__set($name, $val);
	}


	// удаление контент-страницы
	public function delete() {

		if ($this->loaded()) {

			// удаляем все элементы, содержащие внешние данные
			// данные элементы содержат в своей модели ответственность за удаление
			// своих внешних данных

			// изображения
			$images = $this->images->where('type','=',Model_Content_Image::TYPE_IMAGE)->find_all();
			foreach ($images as $image) {
				$image->delete();
			}

			// файлы
			$files = $this->files->where('type','=',Model_Content_File::TYPE_FILE)->find_all();
			foreach ($files as $file) {
				$file->delete();
			}

			parent::delete();
		}
	}


	// найти страницу по ключу
	public static function get_by_key($key) {
		return new Model_Content(array('key'=>$key));
	}
}
?>
