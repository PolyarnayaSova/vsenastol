<?php defined('SYSPATH') or die('No direct script access.');
//
// by ESV Corp. 11.2011
//
// Работа с ключами контент-страниц
//
//

class Content_Key {

	// получить ключ контент-страницы
	// ключи определяются в записи контент-страницы, но конкретное
	// значение прописано в базе конфигурации 'key'
	public static function get(Model_Content $content) {

		if ($content->loaded() &&
			 $content->key &&
			 ($key = Kohana::$config->load('key.' . $content->key))) {

				return $key;
		}

		return false;
	}

	// парсинг элемента 'route' ключевой страницы
	public static function get_route_action($key) {

		if (empty($key) || !is_array($key) || !isset($key['route'])) {
			throw new Kohana_Exception('Неверный формат ключа');
		}

		if (preg_match('/([\w\_\d]+)#([\w\_\d]+)/',$key['route'],$match)) {
			return array($match[1],$match[2]);
		} else {
			return array($key['route'],'index');
		}
	}
}
