<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Base page for site.
 *
 * @package    Basepage
 * @category   Init file
 * @author     ESV Corp. since 07.2011
 */


	// инициализируем cookie
	Cookie::$salt = Kohana::$config->load('basepage.cookie_salt');
?>
