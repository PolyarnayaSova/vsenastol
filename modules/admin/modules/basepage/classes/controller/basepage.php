<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Base page.
 *
 * Main controller
 *
 * @package    Admin
 * @category   Controller
 * @author     ESV Corp. since 07.2011
 */
class Controller_Basepage extends Controller_Template {

	private	$css = array(),
				$scripts = array();

	protected 	$http_equiv = array(),
					$meta = array(),
					$title = '',
					$keywords = '',
					$description = '',
					$author = '',
					$base = '',
					$head = null,
					$content = null;

	public $template = 'basepage';


	public function before() {

		parent::before();

		$this->template->http_equiv = array();

		$this->template->css = array();
		$this->template->scripts = array();
		$this->template->content = null;

		// ВАЖНО!!!
		// всегда производится сброс стандартных настроек CSS
		static::reset_css();
	}


	public function after() {

		$this->template->title = isset($this->title) ? htmlspecialchars($this->title,ENT_COMPAT,'UTF-8',false) : null;
		$this->template->keywords =  isset($this->keywords) ? htmlspecialchars($this->keywords,ENT_COMPAT,'UTF-8',false) : null;
		$this->template->description = isset($this->description) ? htmlspecialchars($this->description,ENT_COMPAT,'UTF-8',false) : null;
		$this->template->author = isset($this->author) ? htmlspecialchars($this->author,ENT_COMPAT,'UTF-8',false): null;
		$this->template->http_equiv = $this->http_equiv;
		$this->template->meta = $this->meta;
		$this->template->base = $this->base;
		$this->template->css = array_merge($this->css, $this->template->css);
		$this->template->scripts = array_merge($this->scripts, $this->template->scripts);
		$this->template->head = $this->head;
		$this->template->content = $this->content . $this->template->content;

		parent::after();
	}


	protected function css($css,$prefix='') {

		if (empty($css)) return;

		if (is_array($css)) {
			foreach ($css as $c) {
				$this->css[] = $prefix . $c;
			}
		} else {
			$this->css[] = $prefix . $css;
		}
	}


	protected function scripts($scripts,$prefix='') {

		if (empty($scripts)) return;

		if (is_array($scripts)) {
			foreach ($scripts as $s) {
				$this->scripts[] = $prefix . $s;
			}
		} else {
			$this->scripts[] = $prefix . $scripts;
		}
	}


	// сброс css, вызывается в before
	protected function reset_css() {
		self::css(Kohana::$config->load('basepage.reset_css'));
	}


	// подключение JQuery
	protected function JQuery() {
		self::scripts(Kohana::$config->load('basepage.jquery'));
	}


	// подключение JQueryUI
	protected function JQueryUI() {
		self::scripts(Kohana::$config->load('basepage.jqueryui.js'));
		self::scripts(Kohana::$config->load('basepage.jqueryui.datepicker_ru'));
		self::css(Kohana::$config->load('basepage.jqueryui.css'));
	}


	// подключение ExtJS
	protected function ExtJS() {
		self::scripts(Kohana::$config->load('basepage.extjs3.jq-adapter'));
		self::scripts(Kohana::$config->load('basepage.extjs3.js'));
		self::css(Kohana::$config->load('basepage.extjs3.css-reset'));
		self::css(Kohana::$config->load('basepage.extjs3.css'));
	}


	// подключение CKEditor
	protected function CKEditor() {

		// CKEditor
		self::scripts(Kohana::$config->load('basepage.ckeditor.js'));
		self::scripts(Kohana::$config->load('basepage.ckeditor.config'));
		self::scripts(Kohana::$config->load('basepage.ckeditor.jquery'));

		// CKFinder
		self::scripts(Kohana::$config->load('basepage.ckfinder.js'));
		self::scripts(Kohana::$config->load('basepage.ckfinder.config'));
	}
}
