<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 * Basepage Config
 *
 * @package    Admin
 * @module		Admin/Basepage
 * @category   Config file
 * @author     ESV Corp. since 07.2011
 */

return array (

	'path' => array (
		'css'			=> 'include/css/',
		'script'		=> 'include/js/',
		'scripts'	=> 'include/js/',
		'image'		=> 'include/images/',
		'images'		=> 'include/images/'
	),

	// изначальный сброс css, находится в корне структуры
	'reset_css' => 'include/css/reset.css',

	// библиотека JQuery находится в корне структуры
	'jquery' => 'include/js/lib/JQuery/jquery-1.7.1.min.js',

	// библиотека JQueryUI находится в корне структуры
	'jqueryui' => array (
		'js' 					=> 'include/js/lib/JQueryUI/js/jquery-ui-1.8.16.custom.min.js',
		'datepicker_ru'	=> 'include/js/lib/JQueryUI/js/jquery.ui.datepicker-ru.js',
		'css'					=> 'include/js/lib/JQueryUI/css/redmond/jquery-ui-1.8.16.custom.css'
	),

	// библиотека ExtJS3 находится в корне структуры
	'extjs3' => array (
		'ext-adapter'	=> 'include/js/lib/ExtJS3/adapter/ext/ext-base.js',
		'jq-adapter'	=> 'include/js/lib/ExtJS3/adapter/jquery/ext-jquery-adapter.js',
		'js'				=> 'include/js/lib/ExtJS3/ext-all.js',
		'css-reset'		=> 'include/js/lib/ExtJS3/resources/css/reset-min.css',
		'css'				=> 'include/js/lib/ExtJS3/resources/css/ext-all.css',
	),

	// редактор находится в корне структуры
	'ckeditor' => array (
		'js'		=> 'include/editor/ckeditor/ckeditor.js',
		'config' => 'include/editor/ckeditor/config.js',
		'jquery' => 'include/editor/ckeditor/adapters/jquery.js' // адаптер CKEditor JQuery
	),

	'ckfinder' => array (
		'js' 		=> 'include/editor/ckfinder/ckfinder.js',
		'config' => 'include/editor/ckfinder/config.js'
	),

	'cookie_salt' => 'SaltForCookieOnSite'
);
?>
