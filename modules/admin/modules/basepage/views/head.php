<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Base page for site.
 *
 * HEAD template
 *
 * @package    Basepage
 * @category   View file
 * @author     ESV Corp. since 07.2011
 */
?>
<head>
<?php
	if ($head):
		print $head;
	else:
?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
		if (!empty($http_equiv)) {
			foreach ($http_equiv as $name => $content) {
				print "<meta content=\"$content\" http-equiv=\"$name\">\n";
			}
		}

		if (!empty($meta)) {
			foreach ($meta as $name => $content) {
				print "<meta content=\"$content\" name=\"$name\">\n";
			}
		}
?>
	<title><?php print $title; ?></title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<meta content="<?php print $keywords; ?>" name="keywords" />
	<meta content="<?php print $description; ?>" name="description" />
	<meta content="<?php print $author; ?>" name="author" />

<?php if (!empty($base)): ?>
<?php	if (is_array($base)): ?>
<?php foreach ($base as $href => $target) { print "<base href=\"$href\" target=\"$target\" />\n"; } ?>
<?php else: ?>
	<base href="<?php print $base; ?>">
<?php endif; ?>
<?php endif; ?>

<?php

	if (!empty($css) && is_array($css)) {
		foreach ($css as $item) {
			print HTML::style($item, array('media' => 'all')) . "\n";
		}
	}

	if (!empty($scripts) && is_array($scripts)) {
		foreach ($scripts as $item) {
			print HTML::script($item) . "\n";
		}
	}

	endif; // if ($head):
?>
</head>
