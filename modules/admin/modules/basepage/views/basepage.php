<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Base page for site.
 *
 * Main template
 *
 * @package    Basepage
 * @category   Template
 * @author     ESV Corp. since 07.2011
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<?php
	print View::factory('head')
		->set('http_equiv',$http_equiv)
		->set('meta',$meta)
		->set('title',$title)
		->set('keywords',$keywords)
		->set('description',$description)
		->set('author',$author)
		->set('base',$base)
		->set('css',$css)
		->set('scripts',$scripts)
		->set('head',$head);
?>
<body>
	<?php print $content; ?>
</body>
</html>
