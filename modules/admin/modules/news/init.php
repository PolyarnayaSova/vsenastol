<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Init file
 *
 * @package    Admin/News
 * @category   Init file
 * @author     ESV Corp. (C) 08.2011
 */

// при регистрации внутреннего модуля заголовок не указывается - модуль не попадает в меню
// доступ осуществляется через контент-страницы
Admin::RegisterModule('admin/news','admin_news');

Admin::RouteSet('admin/news','admin_news','admin/news(/<action>(/<id>)(/content_<content_id>)(/ts_<time>))', array('id'=>'\d+','content_id'=>'\d+','time'=>'\d+'))
	->defaults(array(
		'controller' => 'admin_news',
		'action'     => 'index',
	));
?>
