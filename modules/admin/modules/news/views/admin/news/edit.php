<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit News template
 *
 * @package    Admin/Content
 * @module		News
 * @category   Template
 * @author     ESV Corp. since 10.2011
 */

	if ($news->loaded()) {
		$caption = 'Редактировать новость';
		$submit = 'сохранить';
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$news->id));
	} else {
		$caption = 'Добавить новость';
		$submit = 'добавить';
		$form = Route::url(Admin::route(),array('action'=>'save'));
		$news->visible = 1;
	}
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST')); ?>

	<div class="form">

		<div class="caption"><?php print $caption; ?></div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Заголовок</div>
					<?php print Form::input('name',$news->name,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Кратко</div>
					<?php print Form::textarea('brief',$news->brief,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<table><tr><td>
					<div class="label">Текст</div>
					</td><td>
					<?php print Form::textarea('text',$news->text,array('cols'=>80,'rows'=>8,'id'=>'wysiwyg')); ?>
					</td></tr></table>
				</label>
			</div>

			<div class="field datepicker">
				<label>
					<div class="label">Дата</div>
					<?php print Form::input('date',$news->date,array('size'=>10,'datepicker'=>'datepicker')); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Отображать</div>
					<?php print Form::checkbox('visible', 1, $news->visible!=0); ?>
				</label>
			</div>

		</fieldset>

		<div class="group">SEO</div>

		<fieldset>

			<div class="field">
				<label>
					<div class="label">Title</div>
					<?php print Form::input('title',$news->title,array('size'=>80)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Keywords</div>
					<?php print Form::textarea('keywords',$news->keywords,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

			<div class="field">
				<label>
					<div class="label">Description</div>
					<?php print Form::textarea('description',$news->description,array('cols'=>80,'rows'=>4)); ?>
				</label>
			</div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('save',$submit); ?>
	</div>

	<?php print Form::close(); ?>

</div>

<script language="javascript">
<!--
	// функция определена в главном модуле админки
	// инициализация WYSIWYG
	if (typeof window['ckeditor_start'] == 'function') {
		ckeditor_start('wysiwyg');
	}

	// функция определена в главном модуле админки
	// инициализация datepicker
	if (typeof window['datepicker_start'] == 'function') {
		datepicker_start('INPUT[datepicker="datepicker"]');
	}
-->
</script>
