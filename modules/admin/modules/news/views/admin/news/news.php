<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * News page template
 *
 * @package    Admin/News
 * @module		News
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */
?>
<div class="listHeader">Новости</div>

<div class="list">

	<?php if (count($news)>0): ?>

	<table border="0" cellpadding="0" cellspacing="0" width="800" cols="4">
		<th>создана</th>
		<th>дата</th>
		<th>заголовок</th>
		<th>&nbsp;</th>
		<?php
			foreach($news as $n):

				if (empty($n->date)) {
					$date = '&mdash;';
				} else {
					$date = $n->date;
				}
		?>
		<tr>
			<td class="date nowrap" width="30"><?php print $n->created; ?></td>
			<td class="date nowrap" width="10"><?php print $date; ?></td>
			<td class="info"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','id'=>$n->id)),$n->name); ?></td>
			<td width="20" class="last"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'del' ,'id'=>$n->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данную новость?');")); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>

	<?php else: ?>
	<p>новостей нет</p>
	<?php endif; ?>

	<div class="submit">
		<div class="margin"></div>
		<?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','time'=>(date('is') . rand(100,999)))),'добавить'); ?>
		<div class="delimiter">|</div>
		<?php print HTML::anchor(Route::url('admin_content',array('action'=>'editcontent','id'=>$content_id,'time'=>(date('is') . rand(100,999)))),"страница 'Новости'"); ?>
	</div>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager">
	<?php print  $pager; ?>
	</div>
	<?php endif; ?>

</div>
