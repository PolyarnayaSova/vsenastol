<?php defined('SYSPATH') or die('No direct script access.');
//
// @uthor ESV Corp. (C) 2011
//
// Модель: новости
//
class Model_News extends ORM {


	// используем фильтр для генерации алиасов
	public function filters() {

		return array (
			'name' => array(
				array(array($this,'alias'),array(':field',':value'))
			)
		);
	}


	// генерация алиаса для наименований
	public function alias($field,$value) {

		$alias = array(
			'name' => 'alias'
		);

		if (isset($alias[$field])) {

			$this->$alias[$field] = Text::translit($value);
		}

		return $value;
	}

	// работа с датой в формате dd.mm.yyyy
	public function __get($name) {

		if ($name=='date') {
			$date = parent::__get($name);
			if ($date && preg_match('/(\d{4})\-(\d{2})\-(\d{2})/',$date,$match)) {
				$date = "{$match[3]}.{$match[2]}.{$match[1]}";
			}
			return $date;
		} elseif ($name=='created') {
			$created = parent::__get($name);
			if ($created && preg_match('/(\d{4})\-(\d{2})\-(\d{2}) (\d{2})\:(\d{2})\:(\d{2})/',$created,$match)) {
				$created = "{$match[3]}.{$match[2]}.{$match[1]} {$match[4]}:{$match[5]}";
			}
			return $created;
		}

		return parent::__get($name);
	}

	// работа с датой в формате dd.mm.yyyy
	public function __set($name,$val) {

		if ($name=='date' && $val) {
			if (preg_match('/(\d{2})\.(\d{2})\.(\d{4})/',$val,$match)) {
				$val = "{$match[3]}-{$match[2]}-{$match[1]}";
			}
		}

		return parent::__set($name,$val);
	}

	// дата в виде строки
	public function string_date() {

		$months = array (
			1 => 'января',
			2 => 'февраля',
			3 => 'марта',
			4 => 'апреля',
			5 => 'мая',
			6 => 'июня',
			7 => 'июля',
			8 => 'августа',
			9 => 'сентября',
			10 => 'октября',
			11 => 'ноября',
			12 => 'декабря'
		);

		$date = $this->date;

		if ($date && preg_match('/(\d{2})\.(\d{2})\.(\d{4})/',$date,$match)) {
			return $match[1] . ' ' . $months[intval($match[2])] . ' ' . $match[3];
		}

		return '';
	}
}
?>
