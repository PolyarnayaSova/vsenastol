<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 *
 * News controller
 *
 * @package    Admin/News
 * @category   Module
 * @author     ESV Corp. (C) 07.2011
 */
class Controller_Admin_News extends Controller_Admin {

	private function return_back() {
		// возврат к списку новостей
		$this->request->redirect(Route::url(Admin::route()));
	}


	public function before() {
		parent::before();

		// модуль будет использоваться в ajax-контенте, поэтому не отрисовываем шаблон
		$this->auto_render = false;
	}


	public function action_index() {

		$content_id = $this->request->param('content_id',0);

		$news = new Model_News();
		$news->order_by('date','DESC')->order_by('created','DESC');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.admin_news');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($news->reset(false)->find_all());

		$news = $news->offset($offset)->limit($limit)->find_all();

		$this->response->body(
			View::factory('admin/news/news')
				->set('news',$news)
				->set('content_id',$content_id)
				->set('pager',$pager)
		);
	}


	// редактирование или добавление новости
	public function action_edit() {

		$id = $this->request->param('id',0);

		$news = new Model_News($id);

		$this->response->body(
			View::factory('admin/news/edit')
				->set('news',$news)
		);
	}


	// сохранить
	public function action_save() {

		$id = $this->request->param('id',0);

		$_POST['visible'] = Arr::get($_POST,'visible',0);

		if (empty($_POST['date'])) {
			$_POST['date'] = date('d.m.Y');
		}

		$news = new Model_News($id);
		$fields = array_keys($news->list_columns());

		$news->values($_POST,$fields);
		$news->save();

		// возврат к списку новостей
		$this->return_back();
	}


	// удалить
	public function action_del() {

		$id = $this->request->param('id',0);

		$news = new Model_News($id);

		if ($news->loaded()) {
			$news->delete();
		}

		// возврат к списку новостей
		$this->return_back();
	}
}
