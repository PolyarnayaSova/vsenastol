<?php defined('SYSPATH') or die('No direct script access.');
//
// by ESV Corp. (C) 2011
//
// Модель: Заказы
//
class Model_Order extends ORM {

	// статусы заказа
	const NEW_ORDER = 0,
			SUCCESS = 1,
			REFUSE = 2,
			POST = 3;

	public static $statuses = array (
		self::NEW_ORDER => 'новый',
		self::SUCCESS => 'обработан',
		self::REFUSE => 'отказ',
		self::POST => 'почта'
	);

	// работа с датой в формате dd.mm.yyy
	public function __get($name) {

		if ($name=='dclosed') {
			$date = parent::__get($name);
			if ($date && preg_match('/(\d{4})\-(\d{2})\-(\d{2})/',$date,$match)) {
				$date = "{$match[3]}.{$match[2]}.{$match[1]}";
			}
			return $date;
		} elseif ($name=='created') {
			$created = parent::__get($name);
			if ($created && preg_match('/(\d{4})\-(\d{2})\-(\d{2}) (\d{2})\:(\d{2})\:(\d{2})/',$created,$match)) {
				$created = "{$match[3]}.{$match[2]}.{$match[1]} {$match[4]}:{$match[5]}";
			}
			return $created;
		}

		return parent::__get($name);
	}

	// работа с датой в формате dd.mm.yyy
	public function __set($name,$val) {

		if ($name=='dclosed' && $val) {
			if (preg_match('/(\d{2})\.(\d{2})\.(\d{4})/',$val,$match)) {
				$val = "{$match[3]}-{$match[2]}-{$match[1]}";
			}
		}

		return parent::__set($name,$val);
	}


	// дата создания
	public function get_date_created() {

		$created = $this->created;

		if (preg_match('/(\d{2}\.\d{2}\.\d{4})/',$created,$match)) {
			$created = $match[1];
		}

		return $created;
	}
}
?>
