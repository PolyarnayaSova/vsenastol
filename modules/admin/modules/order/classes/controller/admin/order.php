<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Gallery controller
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
class Controller_Admin_Order extends Controller_Admin {

	public function action_index() {

		$this->content = View::factory('admin/order');
	}


	// заказы для обработки
	public function action_orders() {

		$done = $this->request->param('done',null);
		$opened_id = $this->request->param('o_id',0);

		if ($this->request->method()=='POST') {
			$city = Arr::get($_POST,'city','');
			Cookie::set('admin_orders_city',$city);
		} else {
			$city = Cookie::get('admin_orders_city','');
		}

		$orders = new Model_Order();

		if ('success'==$done) {
			$orders->where('status','=',Model_Order::SUCCESS)->where('closed','=',1);
		} elseif ('refuse'==$done) {
			$orders->where('status','=',Model_Order::REFUSE)->where('closed','=',1);
		} elseif ('post'==$done) {
			$orders->where('status','=',Model_Order::POST)->where('closed','=',0);
		} else {
			$orders->where('status','=',Model_Order::NEW_ORDER)->where('closed','=',0);
		}

		if (!empty($city)) {
			$city = intval($city);
			$orders->where('city','=',$city);
		}

		// заменена сортировка по датам на сортировку просто по номеру
		// 04.07.2012
		//
		////if (null===$done || 'post'==$done) {
		////	$orders->order_by('created','DESC');
		////} else {
		////	$orders->order_by('dclosed','DESC')->order_by('created','DESC');
		////}
		$orders->order_by('id', 'DESC');

		// постраничная навигация
		$pager = Kohana::$config->load('pager.admin_orders');

		$page = max(1, arr::get($_GET, 'page', 1));

		$limit = $pager['items_per_page'];
		$offset = $limit * ($page - 1);

		$pager['total_items'] = count($orders->reset(false)->find_all());

		$orders = $orders->offset($offset)->limit($limit)->find_all();

		$this->content =
			View::factory('admin/order/list')
				->set('orders',$orders)
				->set('pager',$pager)
				->set('opened_id',$opened_id)
				->set('done',$done)
				->set('city',$city);
	}


	// заказ выполнен успешно
	public function action_success() {

		$id = $this->request->param('id');

		$order = new Model_Order($id);

		if ($order->loaded()) {
			$order->dclosed = date('d.m.Y');
			$order->closed = 1;
			$order->status = Model_Order::SUCCESS;
			$order->save();
		}

		$this->request->redirect(Route::url(Admin::route(),array('action'=>'orders')));
	}


	// отказ от заказа
	public function action_refuse() {

		$id = $this->request->param('id');

		$order = new Model_Order($id);

		if ($order->loaded()) {
			$order->dclosed = date('d.m.Y');
			$order->closed = 1;
			$order->status = Model_Order::REFUSE;
			$order->save();
		}

		$this->request->redirect(Route::url(Admin::route(),array('action'=>'orders')));
	}


	// комментарий и статус
	public function action_save() {

		$opened_id = null;

		if ($this->request->method()=='POST') {

			$id = $this->request->param('id');
			$done = $this->request->param('done',null);

			$order = new Model_Order($id);

			if ($order->loaded()) {

				$opened_id = $order->id;

				$order->status = Arr::get($_POST,'status',0);
				if ($order->status==Model_Order::SUCCESS || $order->status==Model_Order::REFUSE) {
					$order->closed = 1;
				} else {
					$order->closed = 0;
				}
				$order->comment = Arr::get($_POST,'comment','');

				$order->save();
			}
		}

		$url = Route::url(Admin::route(),array('action'=>'orders','o_id'=>$opened_id,'done'=>$done));

		// учитываем страницу перехода
		$page = max(1, arr::get($_GET, 'page', 1));
		if ($page>1) {
			$url .= "?page=$page";
		}

		$this->request->redirect($url);
	}


	// бланк заказа
	public function action_blank() {

		$id = $this->request->param('id');
		$double = $this->request->param('double');
		$double = ! empty($double);

		$order = new Model_Order($id);
		if ( ! $order->loaded()) {
			throw new HTTP_Exception_404('Заказ не найден');
		}

		$contacts = Model_Content::get_by_key('order_blank_contacts');
		if ( ! $contacts->loaded()) {
			throw new HTTP_Exception_404('Блок контактной информации для заказа не найден');
		}

		if ($double) {
			$delivery = Model_Content::get_by_key('delivery_pay_info');
			if ( ! $delivery->loaded()) {
				throw new HTTP_Exception_404('Блок информации о доставке и оплате заказа не найден');
			}
		} else {
			$delivery = false;
		}

		$this->auto_render = false;

		$this->response->body(
			View::factory('admin/order/blank')
				->set('contacts', $contacts->text)
				->set('order', $order)
				->set('double', $double)
				->set('delivery', $delivery)
		);
	}


	// распечатать новые заказы
	public function action_blank_new_orders() {

		$start_date = $this->request->param('start_date', null);
		$end_date = $this->request->param('end_date', null);

		$orders = new Model_Order();
		$orders = $orders->where('status','=',0)->and_where('closed','=',0)->order_by('created');

		if ( ! empty($start_date) && preg_match('/(\d{2})_(\d{2})_(\d{4})/', $start_date, $match)) {
			$start_date = $match[3] . '-' . $match[2] . '-' . $match[1];
			$orders = $orders->and_where('created','>=',$start_date);
		}

		if ( ! empty($end_date) && preg_match('/(\d{2})_(\d{2})_(\d{4})/', $end_date, $match)) {
			$end_date = $match[3] . '-' . $match[2] . '-' . ($match[1] + 1);
			$orders = $orders->and_where('created','<=',$end_date);
		}

		$orders = $orders->find_all();

		$this->auto_render = false;

		$this->response->body(
			View::factory('admin/order/blank_new_orders')
				->set('orders', $orders)
		);
	}


	// удалить заказ
	public function action_del() {

		$id = $this->request->param('id');

		$order = new Model_Order($id);

		if ($order->loaded()) {
			$order->delete();
		}

		$this->request->redirect($this->request->referrer());
	}
}
