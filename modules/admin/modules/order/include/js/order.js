/* Orders scripts */
$(document).ready(function(){

	$('DIV.order-body').hide();

	$('DIV.order-body[opened="opened"]').show();

	$('DIV.list').on('click','A.order-anchor',function(){
		var el = $(this);
		var id = el.attr('id');

			$('DIV.order-body[id!="order_'+id+'"]').slideUp('fast');

			$('DIV#order_'+id).slideToggle(500);

		return false;
	});
});
