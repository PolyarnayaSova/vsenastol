<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 * Init file
 *
 * @package    Admin/Order
 * @category   Init file
 * @author     ESV Corp. (C) 12.2011
 */

Admin::RegisterModule('admin/order','admin_order','Заказы');

Admin::RouteSet('admin/order','admin_order','admin/order(/<action>(/<id>)(/open_<o_id>)(/start_<start_date>)(/end_<end_date>))(/done_<done>)(/double_<double>)', array('id'=>'\d+','o_id'=>'\d+'))
	->defaults(array(
		'controller' => 'admin_order',
		'action'     => 'orders',
	));

// добавим пункты подменю
Admin::ActionSet('admin/order',Route::url('admin_order', array('action' => 'orders')),'новые');
Admin::ActionSet('admin/order',Route::url('admin_order', array('action' => 'orders','done'=>'success')),'обработанные');
Admin::ActionSet('admin/order',Route::url('admin_order', array('action' => 'orders','done'=>'refuse')),'отказ');
Admin::ActionSet('admin/order',Route::url('admin_order', array('action' => 'orders','done'=>'post')),'почта');

?>
