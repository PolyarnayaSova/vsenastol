<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Информационный блок заказа
 *
 * @package    Admin/Order
 * @module		Order
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */

	if ($done) {
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$order->id,'done'=>$done));
	} else {
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$order->id));
	}

	// учитываем страницу, на которой находимся, чтобы вернуться на нее же
	$page = max(1, arr::get($_GET, 'page', 1));
	if ($page>1) {
		$form .= "?page=$page";
	}

?>
<div class="order-body" id="<?php print "order_" . $order->id; ?>" <?php if ($opened_id==$order->id) print 'opened="opened"'; ?> >
	<div>
		<p><strong>Ф.И.О</strong>: <?php print $order->fio; ?></p>
		<p><strong>Телефон</strong>: <?php print $order->phone; ?></p>
		<p><strong>Адрес</strong>: <?php print $order->addr; ?></p>
		<p><strong>Город</strong>: <?php print Model_City::name($order->city); ?></p>

		<?php if ($order->email): ?>
		<p><strong>e-mail</strong>: <?php print $order->email; ?></p>
		<?php endif; ?>

		<?php if ($order->self): ?>
		<p><strong>Самовывоз</strong></p>
		<?php endif; ?>

		<?php if ($order->text): ?>
		<p><strong>Дополнительно</strong>:<br />
			<?php print nl2br($order->text); ?>
		</p>
		<?php endif; ?>

		<?php print $order->info; ?>
	</div>

	<div class="wrap_form" style="margin-top:5px;">

		<?php print Form::open($form,array('method'=>'POST')); ?>

		<div class="form">
			<div class="field">
				<label>
					<div class="label">Статус</div>
					<?php print Form::select('status',Model_Order::$statuses,$order->status); ?>
				</label>
				<label>
					<table class="comment"><tr><td>
					<div class="label">Комментарий</div>
					</td><td>
					<?php print Form::textarea('comment',$order->comment,array('cols'=>60,'rows'=>4)); ?>
					</td></tr></table>
				</label>
			</div>
		</div>

		<div class="submit">
			<?php print Form::submit('save','сохранить'); ?>
		</div>

		<?php print Form::close(); ?>

	</div>

	<?php if ($order->blank): ?>
	<div class="blanks-wrap">
		<?php
			print HTML::anchor(Route::url(Admin::route(),array('action'=>'blank','id'=>$order->id)),'бланк',array('target'=>'_blank'));
			print HTML::anchor(Route::url(Admin::route(),array('action'=>'blank','id'=>$order->id,'double'=>'2')),'двойной бланк',array('target'=>'_blank'));
		?>
	</div>
	<?php endif; ?>

</div>
