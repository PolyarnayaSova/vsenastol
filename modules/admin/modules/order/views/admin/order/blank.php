<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Бланк заказа
 *
 * @package    Admin/Order
 * @module		Order
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */
	$css = array (
		'include/css/reset.css',
		'admin/order/include/css/blank.css?v=1'
	);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php

	foreach ($css as $item) {
		print HTML::style($item, array('media' => 'all')) . "\n";
	}
?>
</head>
<body>
	<?php if ($double): $count=2; ?>
	<div class="double-blank">
	<?php else: $count=1; ?>
	<div class="single-blank">
	<?php endif; ?>
		<?php for($i=0;$i<$count;$i++): ?>
		<div class="blank-order">
			<div class="contacts">
				<?php print $contacts; ?>
			</div>
			<h2 style="margin-top: 15px;">Заказ № <?php print $order->id; ?> от <?php print $order->get_date_created(); ?></h2>
			<div class="blank">
				<?php print $order->blank; ?>
			</div>
			<div class="contacts-client">
				<p><strong>Ф.И.О.:</strong> <?php print $order->fio; ?></p>
				<p><strong>Телефон:</strong> <?php print $order->phone; ?></p>
				<p><strong>Адрес доставки:</strong> <?php print 'г.' . Model_City::name($order->city) . ', ' . $order->addr; ?></p>
				<p><strong>E-mail:</strong> <?php print $order->email; ?></p>

				<?php if ($double && $i==0 && ($comment_client = $order->text) && ! empty($comment_client)): ?>
					<p>
						<strong>Дополнительная информация:</strong><br /><?php print nl2br($comment_client); ?>
						<br /><br />
					</p>
				<?php endif; ?>

				<p><strong>Время доставки:</strong><span class="underline" style="padding-left:500px;"></span></p>
				<br /><br />
				<p>Товар получен, претензий не имею:<span class="underline" style="padding-left:200px;"></span> / <span class="underline" style="padding-left:250px;"></span></p>
				<br /><br /><br />
				<h1 style="text-align:center;">Спасибо за покупку!</h1>

				<?php if ($double && $i==0 && $delivery): ?>
					<?php print "\n\f"; ?>
					<div class="delivery">
						<?php print $delivery->text ?>
					</div>
				<?php endif; ?>

				<?php if ($double && $i==0 && ($comment_admin = $order->comment) && ! empty($comment_admin)): ?>
					<?php print "\n\f"; ?>
					<p>
						<strong>Дополнительная информация к заказу:</strong><br /><?php print nl2br($comment_admin); ?>
					</p>
				<?php endif; ?>

			</div>
		</div>
		<?php endfor; ?>
	</div>
</body>
</html>
