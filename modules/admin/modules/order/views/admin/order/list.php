<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Orders list page template
 *
 * @package    Admin/Order
 * @module		Order
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */
?>
<div class="list">

	<div class="listHeader">
		<?php if ($done=='success'): ?>
			Выполненные заказы
		<?php elseif ($done=='refuse'): ?>
			Отказные заказы
		<?php elseif ($done=='post'): ?>
			Заказы по почте
		<?php else: ?>
			Заказы
		<?php endif; ?>
	</div>

	<div class="submit" style="float: left; margin-bottom: 10px;">
		<?php
			print Form::open(Route::url(Admin::route(),array('action'=>'orders','done'=>$done)),array('method'=>'POST'));
			print Form::select('city',Model_City::get_select(true),$city);
			print Form::submit('change_city','сменить',array('style'=>'margin-left: 5px;'));
			print Form::close();
		?>
	</div>

	<?php if (count($orders)>0): ?>

	<table border="0" cellpadding="0" cellspacing="0" width="800" cols="4" class="orders-list">

		<?php if ($done && $done!='post'): ?>
			<th>поступил</th>
			<th>выполнен</th>
			<th>заказ</th>
			<th>телефон</th>
			<th>Ф.И.О.</th>
			<th>&nbsp;</th>
		<?php else: ?>
			<th>поступил</th>
			<th>заказ</th>
			<th>телефон</th>
			<th>Ф.И.О.</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		<?php endif; ?>

		<?php
			foreach($orders as $order):
		?>
		<tr>
			<td class="date nowrap" width="30"><?php print $order->created; ?></td>

			<?php if ($done && $done!='post'): ?>
				<td class="date nowrap" width="30"><?php print $order->dclosed; ?></td>
			<?php endif; ?>

			<td class="text nowrap"><?php print HTML::anchor('#',"Заказ № " . $order->id,array('class'=>'order-anchor','id'=>$order->id)); ?>
				<?php
					print View::factory('admin/order/info')
								->set('order',$order)
								->set('opened_id',$opened_id)
								->set('done',$done);
				?>
			</td>
			<td class="text" width="50" nowrap="nowrap">
				<?php print str_replace(' ','&nbsp;',$order->phone); ?>
			</td>
			<td class="text" width="50" nowrap="nowrap">
				<?php print str_replace(' ','&nbsp;',$order->fio); ?>
			</td>

			<?php if (!$done || $done=='post'): ?>
				<td width="20" class="text"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'success','id'=>$order->id)),'выполнен',array('onclick'=>"return confirm('Заказ действительно выполнен?');")); ?></td>
				<td width="20" class="text"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'refuse','id'=>$order->id)),'отказ',array('onclick'=>"return confirm('От заказа действительно отказались?');")); ?></td>
			<?php endif; ?>

			<td width="20" class="text last"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'del' ,'id'=>$order->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данный заказ?');")); ?></td>
		</tr>
		<?php endforeach; ?>

	</table>

	<div class="blank" style="margin-top: 10px;">
	<?php
		if (empty($done)) {
			$link_blank = Route::url(Admin::route(),array('action'=>'blank_new_orders'));
			print 'от&nbsp;' . Form::input('blank_start', '', array('size' => 10, 'id' => 'blank_start', 'datepicker'=>'datepicker'));
			print '&nbsp;&nbsp;до&nbsp;' . Form::input('blank_end', '', array('size' => 10, 'id' => 'blank_end', 'datepicker'=>'datepicker'));
			print '&nbsp;' . HTML::anchor('#','распечатать',array('target'=>'_blank', 'data-link' => $link_blank));
		}
	?>
	</div>

	<?php $pager = trim(Pagination::factory($pager)); if (!empty($pager)): ?>
	<div class="pager">
	<?php print  $pager; ?>
	</div>
	<?php endif; ?>

	<?php else: ?>
	<p>заказов нет</p>
	<?php endif; ?>

</div>

<script type="text/javascript" language="javascript">
<!--

	$(document).ready(function() {

		var blank_start = $('div.blank input[type="text"]#blank_start'),
			 blank_end = $('div.blank input[type="text"]#blank_end');

		// функция определена в главном модуле админки
		// инициализация datepicker
		if (typeof window['datepicker_start'] == 'function') {
			datepicker_start('INPUT[datepicker="datepicker"]');
		}

		$('div.blank').on('click', 'a', function(e) {

			var link = $(this).attr('data-link'),
				 bstart = blank_start.val(),
				 bend = blank_end.val();

				if (bstart && bstart.length > 0) {
					link += '/start_' + bstart.replace(/\./g, '_');
				}

				if (bend && bend.length > 0) {
					link += '/end_' + bend.replace(/\./g, '_');
				}

				$(this).attr('href', link);

			return true;

		});

	});
-->
</script>
