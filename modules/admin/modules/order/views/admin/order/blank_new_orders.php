<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Бланк заказа
 *
 * @package    Admin/Order
 * @module		Order
 * @category   Template
 * @author     ESV Corp. (C) 12.2011
 */
	$css = array (
		'include/css/reset.css',
		'admin/order/include/css/blank.css?v=1'
	);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php

	foreach ($css as $item) {
		print HTML::style($item, array('media' => 'all')) . "\n";
	}
?>
</head>

<body>

	<div class="blank-order" style="width: auto; font-size: 11px !important;">

	<table width="100%">

	<?php foreach($orders as $order): ?>

		<tr style="padding-bottom: 5px;">

			<td>

				<h2 style="margin-top: 15px; font-size: 13px;">Заказ № <?php print $order->id; ?> от <?php print $order->get_date_created(); ?></h2>

				<div class="contacts-client">
					<p><strong>Ф.И.О.:</strong> <?php print $order->fio; ?></p>
					<p><strong>Телефон:</strong> <?php print $order->phone; ?></p>
					<p><strong>Адрес доставки:</strong> <?php print 'г.' . Model_City::name($order->city) . ', ' . $order->addr; ?></p>
					<p><strong>E-mail:</strong> <?php print $order->email; ?></p>

					<?php if (($comment_client = $order->text) && ! empty($comment_client)): ?>
						<p>
							<strong>Дополнительно:</strong><br /><?php print nl2br($comment_client); ?>
						</p>
					<?php endif; ?>

					<?php if (($comment_admin = $order->comment) && ! empty($comment_admin)): ?>
						<?php print "\n\f"; ?>
						<p>
							<strong>Дополнительная информация к заказу:</strong><br /><?php print nl2br($comment_admin); ?>
						</p>
					<?php endif; ?>
				</div>

			</td>

			<td>
				<div class="blank">
					<?php
						$blank = $order->blank_list;
						$blank = empty($blank) ? $order->blank : $blank;
						print $blank;
					?>
				</div>
			</td>

		</tr>

		<tr><td colspan="2" style="padding:0; margin: 0; height: 3px;">&nbsp</td></tr>
		<tr><td colspan="2" style="padding:0; margin: 0; height: 1px; border-bottom: 1px solid black;">&nbsp</td></tr>
		<tr><td colspan="2" style="padding:0; margin: 0; height: 5px;">&nbsp</td></tr>

	<?php endforeach; ?>

	</table>

	</div>

</body>
</html>
