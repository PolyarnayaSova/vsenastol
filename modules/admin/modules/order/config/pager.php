<?php defined('SYSPATH') or die('No direct script access.');

	// параметры постраничной навигации
	return array (
		// для списка публикаций
		'admin_orders' => array (
			'total_items'		=> 0, // заполняется при вычислении
			'items_per_page'	=> 30,
			'current_page'		=> array(
				'source'	=> 'query_string',
				'key'		=> 'page'
			),
			'auto_hide'	=> TRUE,
			'view'		=> 'admin/pager/orders_floating'
		)
	);
?>
