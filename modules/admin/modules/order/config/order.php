<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 *
 * Orders configuration
 *
 * @package    Admin
 * @module 		Orders
 * @category   Config file
 * @author     ESV Corp. (C) 12.2011
 */

	return array (
		// конфигурация для шаблона заголовка страницы:
		// пути, файлы стилей, скрипты
		'head_page' => array (
			'author' => 'ESV Corp. &copy; 2011',
			'path' => array (
				'css'			=> 'include/css/',
				'script'		=> 'include/js/',
				'scripts'	=> 'include/js/',
				'image'		=> 'include/images/',
				'images'		=> 'include/images/'
			),
			'css' => array (
				'order.css?v=3',
			),
			'scripts' => array (
				'order.js'
			)
		),
		'logo' => 'include/icon/order.png'
	);
?>
