<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools.
 * Init file
 *
 * @package    Admin/Access
 * @category   Init file
 * @author     ESV Corp. since 08.2011
 */

// только для роли 'админ'
if (Admin::logged_in('admin')) {

	Admin::RegisterModule('admin/access','admin_access','Доступ');

	Admin::RouteSet('admin/access','admin_access','admin/access(/<action>(/<id>))')
		->defaults(array(
			'controller' => 'admin_access',
			'action'     => 'index',
		));

	// добавим пункты подменю
	Admin::ActionSet('admin/access',Route::url('admin_access', array('action' => 'users')),'пользователи');
	Admin::ActionSet('admin/access',Route::url('admin_access', array('action' => 'roles')),'привелегии');
}
?>
