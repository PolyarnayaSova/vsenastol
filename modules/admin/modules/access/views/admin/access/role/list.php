<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Users List template
 *
 * @package    Admin/access
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */

$form = Route::url(Admin::route(),array('action'=>'addrole'));

?>
<div class="listHeader">Уровни доступа</div>
<div class="list">
	<table border="0" cellpadding="0" cellspacing="0" width="600" cols="3">

		<?php foreach($roles as $role): ?>
		<tr>
			<td width="100"><?php print $role->name; ?></td>
			<td nowrap="nowrap"><?php print $role->description; ?></td>
			<td width="20" class="last"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'delrole' ,'id'=>$role->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данную привелегию?');")); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
</div>

<br />

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST')); ?>

	<div class="form">

		<div class="caption">Добавить привелегию</div>

		<fieldset>

				<div class="field">
					<label>
						<div class="label">Псевдоним</div>
						<div class="input">
						<?php print Form::input('name','',array('size'=>20)); ?>
						</div>
					</label>
				</div>


				<div class="field">
					<label>
						<div class="label">Описание</div>
						<div class="input">
						<?php print Form::input('description','',array('size'=>40)); ?>
						</div>
					</label>
				<div>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('addrole','добавить'); ?>
	</div>

	<?php print Form::close(); ?>

</div>
