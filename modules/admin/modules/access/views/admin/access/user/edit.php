<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Edit user info template
 *
 * @package    Admin/access
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */

	if ($user->loaded()) {
		$caption = 'Редактировать информацию о пользователе';
		$submit = 'сохранить';
		$form = Route::url(Admin::route(),array('action'=>'save','id'=>$user->id));
		$login = array('size'=>'20','readonly');
	} else {
		$caption = 'Добавить пользователя';
		$submit = 'добавить';
		$form = Route::url(Admin::route(),array('action'=>'save'));
		$login = array('size'=>'20');
	}
?>

<div class="wrap_form">

	<?php print Form::open($form,array('method'=>'POST')); ?>

	<div class="form">

		<div class="caption"><?php print $caption; ?></div>

		<div class="field">
			<label>
				<div class="label">Логин</div>
				<?php print Form::input('login',$user->username,$login); ?>
			</label>
		</div>

		<div class="field">
			<label>
				<div class="label">e-mail</div>
				<?php print Form::input('email',$user->email,array('size'=>20)); ?>
			</label>
		</div>

		<div class="field">
			<label>
				<div class="label">Пароль</div>
				<?php print Form::password('pass1','',array('size'=>10)); ?>
			</label>
		</div>

		<div class="field">
			<label>
				<div class="label">Подтверждение</div>
				<?php print Form::password('pass2','',array('size'=>10)); ?>
			</label>
		</div>

		<div class="group">Привелегии</div>

		<fieldset class="fieldset">

			<?php
				foreach ($roles as $r) {
					print '<div class="field">';
					print '<label>';
					print Form::checkbox('roles[]', $r->id, $user->has('roles',$r));
					print '<div class="label right">';
					print "{$r->name}: {$r->description}";
					print '</div></label></div>';
				}
			?>

		</fieldset>

	</div>

	<div class="submit">
		<?php print Form::submit('save',$submit); ?>
	</div>

	<?php print Form::close(); ?>

</div>
