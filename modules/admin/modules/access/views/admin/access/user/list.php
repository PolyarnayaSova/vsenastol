<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Administration tools for site.
 *
 * Users List template
 *
 * @package    Admin/access
 * @category   Template
 * @author     ESV Corp. since 08.2011
 */

	$add = Route::url(Admin::route(),array('action'=>'edit'));
?>

<div class="listHeader">Пользователи</div>
<div class="list">
	<table border="0" cellpadding="0" cellspacing="0" width="600" cols="2">

		<?php foreach($users as $user): ?>
		<tr>
			<td nowrap="nowrap"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'edit','id'=>$user->id)),$user->username); ?></td>
			<td width="20" class="last"><?php print HTML::anchor(Route::url(Admin::route(),array('action'=>'del' ,'id'=>$user->id)),'удалить',array('onclick'=>"return confirm('Вы действительно хотите удалить данного пользователя?');")); ?></td>
		</tr>
		<?php endforeach; ?>
	</table>
</div>

<div class="submit">
	<div class="margin"></div>
	<?php print HTML::anchor($add,'добавить'); ?>
</div>
