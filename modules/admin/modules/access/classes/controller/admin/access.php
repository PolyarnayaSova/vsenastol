<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Main controller
 *
 * @package    Admin
 * @category   Module
 * @author     ESV Corp. since 07.2011
 */
class Controller_Admin_Access extends Controller_Admin {

	private $sys_roles = array('login','admin');

	public function action_index() {

		$this->content = View::factory('admin/access/content');
	}


	// управление пользователями - список пользователей
	public function action_users() {

		$users = ORM::factory('user')->find_all();

		$this->content = View::factory('admin/access/user/list')->set('users',$users);
	}


	// управление уровнями доступа - список ролей
	public function action_roles() {

		$roles = ORM::factory('role')->find_all();

		$this->content = View::factory('admin/access/role/list')->set('roles',$roles);
	}


	// редактировать информацию о пользователе или добавить пользователя, если id
	// не указан
	public function action_edit() {

		$id = $this->request->param('id',0);

		$user = new Model_User($id);

		$roles = new Model_Role();
		$roles = $roles->find_all();

		$this->content =
			View::factory('admin/access/user/edit')
				->set('user',$user)
				->set('roles',$roles);
	}


	// сохранение информации
	public function action_save() {

		if ($this->request->method()!='POST' || !$this->request->post()) {
			Admin::message('Неверный запрос', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'users')));
			return;
		}

		$id = $this->request->param('id',null);
		$user = new Model_User($id);

		$login = strtolower(Arr::get($_POST,'login'));
		$email = Arr::get($_POST,'email');
		$pass1 = Arr::get($_POST,'pass1');
		$pass2 = Arr::get($_POST,'pass2');
		$roles = Arr::get($_POST,'roles',array());

		if (empty($login)) {
			Admin::message('Требуется имя пользователя', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
			return;
		}

		if (empty($email)) {
			Admin::message('Требуется email пользователя', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
			return;
		}

		if (!Valid::email($email)) {
			Admin::message('Требуется корректный email пользователя', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
			return;
		}

		if (!empty($pass1) && $pass1!=$pass2) {
			Admin::message('Пароли не совпадают', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
			return;
		}

		if ($user->loaded()) {

			// существующий

			if (!empty($pass1)) {
				$user->password = $pass1;
			}

			$user->email = $email;

			$message = 'Информация о пользователе успешно сохранена';

		} else {

			// добавить пользователя
			$user->clear();

			if (empty($pass1)) {
				Admin::message('Пароль не указан', Admin::MSG_ERROR);
				$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
				return;
			}

			$check_user = new Model_User(array('username'=>$login));
			if ($check_user->loaded()) {
				Admin::message('Пользователь с таким именем уже зарегистрирован в системе', Admin::MSG_ERROR);
				$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
				return;
			}

			$check_user = new Model_User(array('email'=>$email));
			if ($check_user->loaded()) {
				Admin::message('Пользователь с таким e-mail уже зарегистрирован в системе', Admin::MSG_ERROR);
				$this->request->redirect(Route::url(Admin::route(),array('action'=>'edit','id'=>$id)));
				return;
			}
			$user->username = $login;
			$user->password = $pass1;
			$user->email = $email;

			$message = 'Пользователь успешно добавлен';
		}

		$user->save();

		// определение привелегий
		$user->remove('roles');

		if (!empty($roles)) {
			$user->add('roles',$roles);
		}

		Admin::message($message, Admin::MSG_OK);

		// перенаправляем к списку пользователей
		$this->request->redirect(Route::url(Admin::route(),array('action'=>'users')));
	}


	// удалить пользователя
	public function action_del() {

		$id = $this->request->param('id',0);
		$user = new Model_User($id);

		if ($user->loaded()) {
			$user->remove('roles'); // удаляем все роли пользователя
			$user->delete();
		}
		// перенаправляем к списку пользователей
		$this->request->redirect(Route::url(Admin::route(),array('action'=>'users')));
	}


	// добавить привелегию
	public function action_addrole() {

		if ($this->request->method()!='POST') {
			Admin::message('Неверный запрос', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
			return;
		}

		$name = strtolower(Arr::get($_POST,'name'));
		$description = Arr::get($_POST,'description');

		if (empty($name)) {
			Admin::message('Требуется псевдоним привелегии', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
			return;
		}

		if (empty($description)) {
			Admin::message('Требуется описание привелегии', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
			return;
		}

		$role = new Model_Role(array('name'=>$name));

		if ($role->loaded()) {
			Admin::message('Привелегия с таким псевдонимом уже зарегистрирована в системе', Admin::MSG_ERROR);
			$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
			return;
		}

		$role->clear();

		$role->name = $name;
		$role->description = $description;
		$role->save();

		// перенаправляем к списку привелегий
		Admin::message('Привелегия успешно добавлена', Admin::MSG_OK);
		$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
	}


	// удалить привелегию
	public function action_delrole() {

		$id = $this->request->param('id',0);
		$role = new Model_Role($id);

		if ($role->loaded()) {
			if (in_array($role->name,$this->sys_roles)) {
				Admin::message('Невозможно удалить системные привелегии', Admin::MSG_ERROR);
				$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
				return;
			}
			$role->remove('users'); // удаляем все связи с пользователями
			$role->delete();
		}

		// перенаправляем к списку привелегий
		$this->request->redirect(Route::url(Admin::route(),array('action'=>'roles')));
	}
}
