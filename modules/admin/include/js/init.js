/* Работа с CKEditor */
$(document).ready(function(){

	if (window.CKFinder) {
		CKFinder.setupCKEditor( null, { basePath: '/include/editor/ckfinder/' });
	}

	// проверка браузера на поддерживаемые стандарты
	if (!($.support.ajax && $.support.boxModel && $.support.cssFloat)) {
		if (window.location.pathname!='/admin/error/browser') {
			window.location.replace('/admin/error/browser');
		}
	}
});

// подключение CKEditor к элементу редактирования
function ckeditor_start(id) {
	if (CKEDITOR) {
		var instance = CKEDITOR.instances[id];
			if (instance) {
				CKEDITOR.remove(instance);
			}

		var el = $('#'+id);
			if (el && el.length) {
				CKEDITOR.replace(id);
			}
	}
}

// инициализация datepicker JQueryUI
function datepicker_start(id) {
	var date_field = $(id);

		if (date_field.length) {
			$.datepicker.setDefaults($.datepicker.regional['ru']);
			date_field.datepicker();
		}
}
