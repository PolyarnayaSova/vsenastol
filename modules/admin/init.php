<?php defined('SYSPATH') or die('No direct script access.');
/**
 * ESV administration tools.
 * Init file
 *
 * @package    Admin
 * @category   Init file
 * @author     ESV Corp. since 07.2011
 */

Admin::RouteSet(Admin::admin_module(), Admin::admin_route(), Admin::admin_path())
	->defaults(array(
		'controller' => 'admin',
		'action'     => 'index',
	));

Admin::RouteSet(Admin::admin_module(), Admin::admin_route().'/login', Admin::admin_path().'/login')
	->defaults(array(
		'controller' => 'admin',
		'action'     => 'login',
	));

Admin::RouteSet(Admin::admin_module(), Admin::admin_route().'/init', Admin::admin_path().'/init')
	->defaults(array(
		'controller' => 'admin',
		'action'     => 'init',
	));

Admin::RouteSet(Admin::admin_module(), Admin::admin_route().'/error', Admin::admin_path().'/error(/<code>)',array('code'=>'browser|\d+'))
	->defaults(array(
		'controller' => 'admin_error',
		'action'     => 'error',
	));

// добавляем пути подключаемых файлов для админки
Admin::set_includes_module(Admin::admin_module());
?>
